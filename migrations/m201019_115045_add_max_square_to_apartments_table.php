<?php

use yii\db\Migration;

/**
 * Class m201019_115045_add_max_square_to_apartments_table
 */
class m201019_115045_add_max_square_to_apartments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('apartments', 'max_square', $this->float()->comment('Максимальная Квадратура'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('apartments', 'max_square');
    }
}
