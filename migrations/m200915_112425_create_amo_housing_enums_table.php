<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%amo_housing_enums}}`.
 */
class m200915_112425_create_amo_housing_enums_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%amo_housing_enums}}', [
            'id' => $this->primaryKey(),
            'amo_id' => $this->integer()->comment('ID в базе AmoCRM'),
            'name' => $this->string()->comment('ЖК в базе AmoCRM'),
            'housing_estate_id' => $this->integer()
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%amo_housing_enums}}');
    }
}
