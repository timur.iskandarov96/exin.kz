<?php

use yii\db\Migration;

/**
 * Class m201127_062459_add_slug_to_vacancy_translation_table
 */
class m201127_062459_add_slug_to_vacancy_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('vacancies_translation', 'slug', $this->string(256)->comment('ЧПУ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('vacancies_translation', 'slug');
    }
}
