<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m201112_093929_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->comment('Изображение'),
            'publication_date' => $this->date()->comment('Дата публикации'),
            'is_active' => $this->boolean()->comment('Активность'),
            'position' => $this->integer()->comment('Порядок очередности')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
