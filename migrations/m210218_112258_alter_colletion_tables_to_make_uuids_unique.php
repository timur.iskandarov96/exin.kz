<?php

use yii\db\Migration;

/**
 * Class m210218_112258_alter_colletion_tables_to_make_uuids_unique
 */
class m210218_112258_alter_colletion_tables_to_make_uuids_unique extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('location_collection', 'uuid', $this->string(256)->unique());
        $this->alterColumn('location_status_collection', 'uuid', $this->string(256)->unique());
        $this->alterColumn('location_type_collection', 'uuid', $this->string(256)->unique());
        $this->alterColumn('projects_collection', 'uuid', $this->string(256)->unique());
        $this->alterColumn('cities_collection', 'uuid', $this->string(256)->unique());
        $this->alterColumn('districts_collection', 'uuid', $this->string(256)->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210218_112258_alter_colletion_tables_to_make_uuids_unique cannot be reverted.\n";

        return false;
    }
}
