<?php

use yii\db\Migration;

/**
 * Class m201210_120745_alter_description_column_in_vacancies_translation
 */
class m201210_120745_alter_description_column_in_vacancies_translation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('vacancies_translation', 'description', $this->text()->comment('Описание'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
