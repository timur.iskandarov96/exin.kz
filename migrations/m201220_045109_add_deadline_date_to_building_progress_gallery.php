<?php

use yii\db\Migration;

/**
 * Class m201220_045109_add_deadline_date_to_building_progress_gallery
 */
class m201220_045109_add_deadline_date_to_building_progress_gallery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('building_progress_gallery', 'deadline_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('building_progress_gallery', 'deadline_date');
    }
}
