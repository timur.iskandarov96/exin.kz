<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_progress_translation}}`.
 */
class m201106_073917_create_building_progress_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_progress_translation}}', [
            'id' => $this->primaryKey(),
            'building_progress_id' => $this->integer(),
            'description' => $this->text()->comment('Описание'),
        ]);

        $this->createIndex(
            'idx-building_progress_translation-building_progress_id',
            'building_progress_translation',
            'building_progress_id'
        );

        $this->addForeignKey(
            'fk-building_progress_translation-building_progress_id',
            'building_progress_translation',
            'building_progress_id',
            'building_progress',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-building_progress_translation-building_progress_id',
            'building_progress_translation'
        );

        $this->dropIndex(
            'idx-building_progress_translation-building_progress_id',
            'building_progress_translation'
        );

        $this->dropTable('{{%building_progress_translation}}');
    }
}
