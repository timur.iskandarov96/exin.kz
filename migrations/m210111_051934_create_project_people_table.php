<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project_people}}`.
 */
class m210111_051934_create_project_people_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%project_people}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull()->comment('Проект'),
            'full_name' => $this->string(512)->notNull()->comment('ФИО'),
            'job_position' => $this->string(255)->notNull()->comment('Должность'),
            'lang' => $this->string()->notNull()->comment('Язык перевода')
        ]);

        $this->createIndex(
            'idx-project_people-project_id',
            'project_people',
            'project_id'
        );

        $this->addForeignKey(
            'fk-project_people-project_id',
            'project_people',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-project_people-project_id',
            'project_people'
        );

        $this->dropIndex(
            'idx-project_people-project_id',
            'project_people'
        );

        $this->dropTable('{{%project_people}}');
    }
}
