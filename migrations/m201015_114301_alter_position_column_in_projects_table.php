<?php

use yii\db\Migration;

/**
 * Class m201015_114301_alter_position_column_in_projects_table
 */
class m201015_114301_alter_position_column_in_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('projects', 'position', $this->integer(11)->unsigned());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201015_114301_alter_position_column_in_projects_table cannot be reverted.\n";

        return false;
    }
}
