<?php

use yii\db\Migration;

/**
 * Class m210120_112954_add_address_image_to_projects_table
 */
class m210120_112954_add_address_image_to_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'address_image', $this->string()->comment('Схема расположения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'address_image');
    }
}
