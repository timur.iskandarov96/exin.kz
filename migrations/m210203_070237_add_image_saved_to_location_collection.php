<?php

use yii\db\Migration;

/**
 * Class m210203_070237_add_image_saved_to_location_collection
 */
class m210203_070237_add_image_saved_to_location_collection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('location_collection', 'image_saved', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'image_saved');
    }
}
