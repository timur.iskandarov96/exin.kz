<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%location_collection}}`.
 */
class m201223_115645_create_location_collection_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%location_collection}}', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(256)->comment('UUID'),
            'project' => $this->string(256)->comment('ЖК'),
            'floor' => $this->integer()->comment('Этаж'),
            'area' => $this->float()->comment('Площадь'),
            'locationtype' => $this->string(256)->comment('Тип помещения'),
            'room_number' => $this->integer()->comment('Количество комнат'),
            'locationstatus' => $this->string(256)->comment('Статус помещения'),
            'plan_ru' => $this->string(256)->comment('Схема на русском'),
            'plan_kk' => $this->string(256)->comment('Схема на казахском')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%location_collection}}');
    }
}
