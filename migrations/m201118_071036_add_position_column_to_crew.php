<?php

use yii\db\Migration;

/**
 * Class m201118_071036_add_position_column_to_crew
 */
class m201118_071036_add_position_column_to_crew extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crew', 'position', $this->integer()->comment('Порядок очередности'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crew', 'position');
    }
}
