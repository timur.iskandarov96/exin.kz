<?php

use yii\db\Migration;

/**
 * Class m201223_120832_create_other_collection_tables
 */
class m201223_120832_create_other_collection_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%location_type_collection}}', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(256)->comment('UUID'),
            'name' => $this->string(256)->comment('Название')
        ]);

        $this->createTable('{{%location_status_collection}}', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(256)->comment('UUID'),
            'name' => $this->string(256)->comment('Название')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201223_120832_create_other_collection_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_120832_create_other_collection_tables cannot be reverted.\n";

        return false;
    }
    */
}
