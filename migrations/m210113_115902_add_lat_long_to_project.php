<?php

use yii\db\Migration;

/**
 * Class m210113_115902_add_lat_long_to_project
 */
class m210113_115902_add_lat_long_to_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'lat', $this->string());
        $this->addColumn('projects', 'lon', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'lat');
        $this->dropColumn('projects', 'lon');
    }
}
