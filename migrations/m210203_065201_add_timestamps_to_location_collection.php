<?php

use yii\db\Migration;

/**
 * Class m210203_065201_add_timestamps_to_location_collection
 */
class m210203_065201_add_timestamps_to_location_collection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            'location_collection',
            'created_at',
            $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания')
        );

        $this->addColumn(
            'location_collection',
            'updated_at',
            $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'created_at');
        $this->dropColumn('location_collection', 'updated_at');
    }
}
