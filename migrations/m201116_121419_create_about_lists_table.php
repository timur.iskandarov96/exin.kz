<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_lists}}`.
 */
class m201116_121419_create_about_lists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about_lists}}', [
            'id' => $this->primaryKey(),
            'about_block_id' => $this->integer()->comment('Блок страницы'),
            'lang' => $this->string(),
            'text' => $this->text()
        ]);

        $this->createIndex(
            'idx-about_lists-about_block_id',
            'about_lists',
            'about_block_id'
        );

        $this->addForeignKey(
            'fk-about_lists-about_block_id',
            'about_lists',
            'about_block_id',
            'about_company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-about_lists-about_block_id',
            'about_lists'
        );

        $this->dropIndex(
            'idx-about_lists-about_block_id',
            'about_lists'
        );

        $this->dropTable('{{%about_lists}}');
    }
}
