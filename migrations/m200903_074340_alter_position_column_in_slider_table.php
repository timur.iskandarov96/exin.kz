<?php

use yii\db\Migration;

/**
 * Class m200903_074340_alter_position_column_in_slider_table
 */
class m200903_074340_alter_position_column_in_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('slider', 'position', $this->integer(11)->unsigned());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
