<?php

use yii\db\Migration;

/**
 * Class m201112_093030_add_slug_to_exclusive_social_translation_table
 */
class m201112_093030_add_slug_to_exclusive_social_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('exclusive_social_translation', 'slug', $this->string(256));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('exclusive_social_translation', 'slug');
    }
}
