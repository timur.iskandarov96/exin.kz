<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%plans_collection}}`.
 */
class m201228_091247_create_plans_collection_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%plans_collection}}', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(256)->comment('UUID'),
            'text' => $this->string(512)->comment('Текс'),
            'ru_image' => $this->string(256)->comment('Файл изображенияна на русском языке'),
            'kk_image' => $this->string(256)->comment('Файл изображения на казахском языке')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%plans_collection}}');
    }
}
