<?php

use yii\db\Migration;

/**
 * Class m210106_120257_add_created_at_updated_at_to_vacancies
 */
class m210106_120257_add_created_at_updated_at_to_vacancies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('vacancies', 'created_at', $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'));
        $this->addColumn('vacancies', 'updated_at', $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('vacancies', 'created_at');
        $this->dropColumn('vacancies', 'updated_at');
    }
}
