<?php

use yii\db\Migration;

/**
 * Class m201103_120909_add_lang_to_crew_translation_table
 */
class m201103_120909_add_lang_to_crew_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crew_translation', 'lang', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crew_translation', 'lang');
    }
}
