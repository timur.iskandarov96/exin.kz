<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project_statuses}}`.
 */
class m201013_111258_create_project_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%project_statuses}}', [
            'id' => $this->primaryKey(),
            'status' => $this->string(256)->comment('Статус')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%project_statuses}}');
    }
}
