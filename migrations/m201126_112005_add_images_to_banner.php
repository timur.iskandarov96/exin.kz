<?php

use yii\db\Migration;

/**
 * Class m201126_112005_add_images_to_banner
 */
class m201126_112005_add_images_to_banner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('promo_banners', 'img_xs_ru', $this->string());
        $this->addColumn('promo_banners', 'img_md_ru', $this->string());
        $this->addColumn('promo_banners', 'img_vertical_ru', $this->string());

        $this->addColumn('promo_banners', 'image_kz', $this->string());
        $this->addColumn('promo_banners', 'img_xs_kz', $this->string());
        $this->addColumn('promo_banners', 'img_md_kz', $this->string());
        $this->addColumn('promo_banners', 'img_vertical_kz', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('promo_banners', 'img_xs_ru');
        $this->dropColumn('promo_banners', 'img_md_ru');
        $this->dropColumn('promo_banners', 'img_vertical_ru');

        $this->dropColumn('promo_banners', 'image_kz');
        $this->dropColumn('promo_banners', 'img_xs_kz');
        $this->dropColumn('promo_banners', 'img_md_kz');
        $this->dropColumn('promo_banners', 'img_vertical_kz');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201126_112005_add_images_to_banner cannot be reverted.\n";

        return false;
    }
    */
}
