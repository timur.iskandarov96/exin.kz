<?php

use yii\db\Migration;

/**
 * Class m210120_095820_remove_obsolete_columns_in_location_collection_table
 */
class m210120_095820_remove_obsolete_columns_in_location_collection_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('location_collection', 'floor');
        $this->dropColumn('location_collection', 'plan');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210120_095820_remove_obsolete_columns_in_location_collection_table cannot be reverted.\n";

        return false;
    }
}
