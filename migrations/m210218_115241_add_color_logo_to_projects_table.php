<?php

use yii\db\Migration;

/**
 * Class m210218_115241_add_color_logo_to_projects_table
 */
class m210218_115241_add_color_logo_to_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'color_logo', $this->string()->comment('Цветное лого'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'color_logo');
    }
}
