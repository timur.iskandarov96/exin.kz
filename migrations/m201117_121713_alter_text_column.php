<?php

use yii\db\Migration;

/**
 * Class m201117_121713_alter_text_column
 */
class m201117_121713_alter_text_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('about_company_lists_translation', 'text', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
