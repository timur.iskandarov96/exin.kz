<?php

use yii\db\Migration;

/**
 * Class m201228_093624_alter_locations_columns_table
 */
class m201228_093624_alter_locations_columns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('location_collection', 'plan_ru');
        $this->dropColumn('location_collection', 'plan_kk');
        $this->addColumn('location_collection', 'plan', $this->string(256)->comment('UUID плана'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'plan');
    }
}
