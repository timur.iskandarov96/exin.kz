<?php

use yii\db\Migration;

/**
 * Class m210212_104230_add_image_saving_error_to_location_collection
 */
class m210212_104230_add_image_saving_error_to_location_collection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('location_collection', 'image_saving_error', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'image_saving_error');
    }
}
