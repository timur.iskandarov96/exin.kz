<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_progress}}`.
 */
class m201105_112031_create_building_progress_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_progress}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull()->comment('ЖК'),
            'deadline_date' => $this->dateTime()->notNull()->comment('Срок сдачи'),
            'service_date' => $this->dateTime()->comment('Срок эксплуатации'),
            'position' => $this->integer(11)->unsigned()->comment('Позиция')
        ]);

        $this->createIndex(
            'idx-building_progress-project_id',
            'building_progress',
            'project_id'
        );

        $this->addForeignKey(
            'fk-building_progress-project_id',
            'building_progress',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-building_progress-project_id',
            'building_progress'
        );

        $this->dropIndex(
            'idx-building_progress-project_id',
            'building_progress'
        );

        $this->dropTable('{{%building_progress}}');
    }
}
