<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%promo_banners_translation}}`.
 */
class m201112_074231_create_promo_banners_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%promo_banners_translation}}', [
            'id' => $this->primaryKey(),
            'promo_banner_id' => $this->integer()->comment('Промо баннер'),
            'lang' => $this->string()->comment('Язык перевода'),
            'title' => $this->string()->comment('Заголовок'),
            'description' => $this->text()->comment('Описание')
        ]);

        $this->createIndex(
            'idx-promo_banners_translation-promo_banner_id',
            'promo_banners_translation',
            'promo_banner_id'
        );

        $this->addForeignKey(
            'fk-promo_banners_translation-promo_banner_id',
            'promo_banners_translation',
            'promo_banner_id',
            'promo_banners',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-promo_banners_translation-promo_banner_id',
            'promo_banners_translation'
        );

        $this->dropIndex(
            'idx-promo_banners_translation-promo_banner_id',
            'promo_banners_translation'
        );

        $this->dropTable('{{%promo_banners_translation}}');
    }
}
