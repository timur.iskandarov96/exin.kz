<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_translation}}`.
 */
class m201112_094231_create_news_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news_translation}}', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->comment('Новость'),
            'lang' => $this->string()->comment('Язык перевода'),
            'title' => $this->string(512)->comment('Заголовок'),
            'description' => $this->text()->comment('Текст'),
            'slug' => $this->string()->comment('Slug')
        ]);

        $this->createIndex(
            'idx-news_translation-news_id',
            'news_translation',
            'news_id'
        );

        $this->addForeignKey(
            'fk-news_translation-promo_news_id',
            'news_translation',
            'news_id',
            'news',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-news_translation-promo_news_id',
            'news_translation'
        );

        $this->dropIndex(
            'idx-news_translation-news_id',
            'news_translation'
        );

        $this->dropTable('{{%news_translation}}');
    }
}
