<?php

use yii\db\Migration;

/**
 * Class m201229_092046_alter_request_tasks_table
 */
class m201229_092046_alter_request_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('request_tasks', 'is_sent_to_amo', $this->boolean()->defaultValue(0));
        $this->addColumn('request_tasks', 'is_sent_to_email', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('request_tasks', 'is_sent_to_amo');
        $this->dropColumn('request_tasks', 'is_sent_to_email');
    }
}
