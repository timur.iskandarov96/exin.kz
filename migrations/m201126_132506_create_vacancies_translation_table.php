<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vacancies_translation}}`.
 */
class m201126_132506_create_vacancies_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vacancies_translation}}', [
            'id' => $this->primaryKey(),
            'vacancy_id' => $this->integer()->comment('Вакансия'),
            'lang' => $this->string()->comment('Язык перевода'),
            'title' => $this->string(256)->comment('Название вакансии'),
            'description' => $this->string(512)->comment('Описание'),
            'category' => $this->string(256)->comment('Категория'),
            'responsible_person_full_name' => $this->string(256)->comment('ФИО'),
            'responsible_person_job' => $this->string(256)->comment('Должность'),
        ]);

        $this->createIndex(
            'idx-vacancies_translation-vacancy_id',
            'vacancies_translation',
            'vacancy_id'
        );

        $this->addForeignKey(
            'fk-vacancies_translation-vacancy_id',
            'vacancies_translation',
            'vacancy_id',
            'vacancies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-vacancies_translation-vacancy_id',
            'vacancies_translation'
        );

        $this->dropIndex(
            'idx-vacancies_translation-vacancy_id',
            'vacancies_translation'
        );

        $this->dropTable('{{%vacancies_translation}}');
    }
}
