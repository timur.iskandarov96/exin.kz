<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_progress_gallery}}`.
 */
class m201109_070107_create_building_progress_gallery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_progress_gallery}}', [
            'id' => $this->primaryKey(),
            'building_progress_id' => $this->integer(),
            'path' => $this->string(),
            'position' => $this->integer()->unsigned()
        ]);

        $this->createIndex(
            'idx-building_progress_gallery-building_progress_id',
            'building_progress_gallery',
            'building_progress_id'
        );

        $this->addForeignKey(
            'fk-building_progress_gallery-building_progress_id',
            'building_progress_gallery',
            'building_progress_id',
            'building_progress',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-building_progress_gallery-building_progress_id',
            'building_progress_gallery'
        );

        $this->dropIndex(
            'idx-building_progress_gallery-building_progress_id',
            'building_progress_gallery'
        );

        $this->dropTable('{{%building_progress_gallery}}');
    }
}
