<?php

use yii\db\Migration;

/**
 * Class m201222_080305_alter_building_progress_table
 */
class m201222_080305_alter_building_progress_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('building_progress', 'deadline_date');
        $this->alterColumn('building_progress', 'service_date', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('building_progress', 'deadline_date', $this->date());
        $this->alterColumn('building_progress', 'service_date', $this->date());
    }
}
