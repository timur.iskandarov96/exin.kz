<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%requests}}`.
 */
class m200902_120455_create_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%requests}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Имя'),
            'phone' => $this->string()->notNull()->comment('Номер телефона'),
            'housing_estate_id' => $this->integer()->comment('Выбранный ЖК'),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->createIndex(
            'idx-requests-housing_estate_id',
            'requests',
            'housing_estate_id'
        );

        $this->addForeignKey(
            'fk-requests-housing_estate_id',
            'requests',
            'housing_estate_id',
            'housing_estates',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-requests-housing_estate_id',
            'requests'
        );

        $this->dropIndex(
            'idx-requests-housing_estate_id',
            'requests'
        );

        $this->dropTable('{{%requests}}');
    }
}
