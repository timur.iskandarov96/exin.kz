<?php

use yii\db\Migration;

/**
 * Class m201204_123327_alter_image_in_slider
 */
class m201204_123327_alter_image_in_slider extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('slider', 'image', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
