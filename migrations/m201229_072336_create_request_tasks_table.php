<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request_tasks}}`.
 */
class m201229_072336_create_request_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%request_tasks}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(512),
            'phone' => $this->string(255),
            'housing_estate_id' => $this->integer(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
            'is_complete' => $this->boolean()->defaultValue(false),
            'completed_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-request_tasks-housing_estate_id',
            'request_tasks',
            'housing_estate_id'
        );

        $this->addForeignKey(
            'fk-request_tasks-housing_estate_id',
            'request_tasks',
            'housing_estate_id',
            'housing_estates',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-request_tasks-housing_estate_id',
            'request_tasks'
        );

        $this->dropIndex(
            'idx-request_tasks-housing_estate_id',
            'request_tasks'
        );

        $this->dropTable('{{%request_tasks}}');
    }
}
