<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_company_statistics}}`.
 */
class m201103_091047_create_about_company_statistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about_company_statistics}}', [
            'id' => $this->primaryKey(),
            'is_active' => $this->integer()->comment('Активность')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%about_company_statistics}}');
    }
}
