<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%statistics_main}}`.
 */
class m201207_100137_create_statistics_main_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%statistics_main}}', [
            'id' => $this->primaryKey(),
            'icon' => $this->string(255)->comment('Иконка'),
            'is_active' => $this->boolean()->comment('Активность')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%statistics_main}}');
    }
}
