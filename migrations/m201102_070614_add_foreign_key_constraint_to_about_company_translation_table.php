<?php

use yii\db\Migration;

/**
 * Class m201102_070614_add_foreign_key_constraint_to_about_company_translation_table
 */
class m201102_070614_add_foreign_key_constraint_to_about_company_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-about_company_translation-about_company_id',
            'about_company_translation',
            'about_company_id'
        );

        $this->addForeignKey(
            'fk-about_company_translation-about_company_id',
            'about_company_translation',
            'about_company_id',
            'about_company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-about_company_translation-about_company_id',
            'about_company_translation'
        );

        $this->dropIndex(
            'idx-about_company_translation-about_company_id',
            'about_company_translation'
        );
    }
}
