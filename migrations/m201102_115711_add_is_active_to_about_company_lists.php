<?php

use yii\db\Migration;

/**
 * Class m201102_115711_add_is_active_to_about_company_lists
 */
class m201102_115711_add_is_active_to_about_company_lists extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('about_company_lists', 'is_active', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('about_company_lists', 'is_active');
    }

}
