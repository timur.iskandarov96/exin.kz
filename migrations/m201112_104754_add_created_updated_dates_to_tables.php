<?php

use yii\db\Migration;

/**
 * Class m201112_104754_add_created_updated_dates_to_tables
 */
class m201112_104754_add_created_updated_dates_to_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            'news',
            'created_at',
            $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания')
        );

        $this->addColumn(
            'news',
            'updated_at',
            $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления')
        );

        $this->addColumn(
            'building_progress',
            'created_at',
            $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания')
        );

        $this->addColumn(
            'building_progress',
            'updated_at',
            $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления')
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('news', 'created_at');
        $this->dropColumn('news', 'updated_at');
        $this->dropColumn('building_progress', 'created_at');
        $this->dropColumn('building_progress', 'updated_at');

    }
}
