<?php

use yii\db\Migration;

/**
 * Class m201119_113254_add_image_description_to_news_translation
 */
class m201119_113254_add_image_description_to_news_translation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('news_translation', 'image_description', $this->string(256));
        $this->addColumn('exclusive_social_translation', 'image_description', $this->string(256));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('news_translation', 'image_description');
        $this->dropColumn('exclusive_social_translation', 'image_description');
    }
}
