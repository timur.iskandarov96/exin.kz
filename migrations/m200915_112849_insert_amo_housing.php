<?php

use yii\db\Migration;

/**
 * Class m200915_112849_insert_amo_housing
 */
class m200915_112849_insert_amo_housing extends Migration
{
    // Taken from AmoCRM\Collections\CustomFields\CustomFieldsCollection (type Contacts)
    const LIST = [
        775753 => 'Алатау Сити',
        775755 => 'Аспан Сити',
        775757 => 'Панорама',
        775759 => 'Exclusive Life',
        775761 => 'Exclusive Residence',
        809005 => 'Юбилейный',
        809007 => 'Exclusive Star',
        858637 => 'Exclusive Time'
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach (self::LIST as $amoId => $house) {
            $inDb = \app\modules\request\models\HousingEstate::find()->where(['like', 'name', $house])->one();
            $innerId = 0;

            if ($inDb) {
                $innerId = $inDb->id;
            }

            $this->insert('amo_housing_enums', [
                'amo_id' => $amoId,
                'name' => $house,
                'housing_estate_id' => $innerId
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200915_112849_insert_amo_housing cannot be reverted.\n";

        return false;
    }
}
