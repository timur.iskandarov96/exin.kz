<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_company_statistics_translation}}`.
 */
class m201103_091215_create_about_company_statistics_translation_table extends Migration
{
    /**
     * lang
     * title
     * subtitle
     * text
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about_company_statistics_translation}}', [
            'id' => $this->primaryKey(),
            'about_company_statistics_id' => $this->integer()->comment('Статистика'),
            'lang' => $this->string()->comment('Язык перевода'),
            'title' => $this->string()->comment('Заголовок'),
            'subtitle' => $this->string()->comment('Подзаголовок'),
            'text' => $this->string()->comment('Текст')
        ]);

        $this->createIndex(
            'idx-statistics_translation-about_company_statistics_id',
            'about_company_statistics_translation',
            'about_company_statistics_id'
        );

        $this->addForeignKey(
            'fk-statistics_translation-about_company_statistics_id',
            'about_company_statistics_translation',
            'about_company_statistics_id',
            'about_company_statistics',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-statistics_translation-about_company_statistics_id',
            'about_company_statistics_translation'
        );

        $this->dropIndex(
            'idx-statistics_translation-about_company_statistics_id',
            'about_company_statistics_translation'
        );

        $this->dropTable('{{%about_company_statistics_translation}}');
    }
}
