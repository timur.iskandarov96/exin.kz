<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%exclusive_social_translation}}`.
 */
class m201110_114431_create_exclusive_social_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%exclusive_social_translation}}', [
            'id' => $this->primaryKey(),
            'lang' => $this->string()->comment('Язык перевода'),
            'exclusive_social_id' => $this->integer(),
            'preview_title' => $this->string()->comment('Заголовок превью'),
            'main_title' => $this->string()->comment('Заголовок'),
            'description' => $this->text()->comment('Текст'),
            'preview_description' => $this->text()->comment('Текст превью')
        ]);

        $this->createIndex(
            'idx-exclusive_social_translation-exclusive_social_id',
            'exclusive_social_translation',
            'exclusive_social_id'
        );

        $this->addForeignKey(
            'fk-exclusive_social_translation-exclusive_social_id',
            'exclusive_social_translation',
            'exclusive_social_id',
            'exclusive_social',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-exclusive_social_translation-exclusive_social_id',
            'exclusive_social_translation'
        );

        $this->dropIndex(
            'idx-exclusive_social_translation-exclusive_social_id',
            'exclusive_social_translation'
        );

        $this->dropTable('{{%exclusive_social_translation}}');
    }
}
