<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_company_lists_translation}}`.
 */
class m201102_071106_create_about_company_lists_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about_company_lists_translation}}', [
            'id' => $this->primaryKey(),
            'lang' => $this->string()->notNull()->comment('Язык перевода'),
            'about_company_lists_id' => $this->integer()->notNull()->comment('Список'),
            'text' => $this->string()->notNull()->comment('Текст')
        ]);

        $this->createIndex(
            'idx-about_company_lists_translation-about_company_lists_id',
            'about_company_lists_translation',
            'about_company_lists_id'
        );

        $this->addForeignKey(
            'fk-about_company_lists_translation-about_company_lists_id',
            'about_company_lists_translation',
            'about_company_lists_id',
            'about_company_lists',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-about_company_lists_translation-about_company_lists_id',
            'about_company_lists_translation'
        );

        $this->dropIndex(
            'idx-about_company_lists_translation-about_company_lists_id',
            'about_company_lists_translation'
        );

        $this->dropTable('{{%about_company_lists_translation}}');
    }
}
