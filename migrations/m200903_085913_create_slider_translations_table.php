<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%slider_translations}}`.
 */
class m200903_085913_create_slider_translations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%slider_translations}}', [
            'id' => $this->primaryKey(),
            'lang' => $this->string()->notNull()->comment('Язык перевода'),
            'slider_id' => $this->integer(),
            'text' => $this->text()->notNull()->comment('Текстовое поле')
        ]);

        $this->createIndex(
            'idx-slider_translations-slider_id',
            'slider_translations',
            'slider_id'
        );

        $this->addForeignKey(
            'fk-slider_translations-slider_id',
            'slider_translations',
            'slider_id',
            'slider',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-slider_translations-slider_id',
            'slider_translations'
        );

        $this->dropIndex(
            'idx-slider_translations-slider_id',
            'slider_translations'
        );

        $this->dropTable('{{%slider_translations}}');
    }
}
