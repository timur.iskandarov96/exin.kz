<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%apartments}}`.
 */
class m201013_113728_create_apartments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apartments}}', [
            'id' => $this->primaryKey(),
            'number_of_rooms' => $this->integer()->unsigned()->comment('Комнатность'),
            'square' => $this->float()->comment('Квадратура'),
            'price' => $this->integer()->unsigned()->comment('Цена'),
            'project_id' => $this->integer()
        ]);

        $this->createIndex(
            'idx-apartments-project_id',
            'apartments',
            'project_id'
        );

        $this->addForeignKey(
            'fk-apartments-project_id',
            'apartments',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-apartments-project_id',
            'apartments'
        );

        $this->dropIndex(
            'idx-apartments-project_id',
            'apartments'
        );

        $this->dropTable('{{%apartments}}');
    }
}
