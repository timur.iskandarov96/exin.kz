<?php

use yii\db\Migration;

/**
 * Class m201224_062048_add_price_to_location_collection
 */
class m201224_062048_add_price_to_location_collection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('location_collection', 'price', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'price');
    }
}
