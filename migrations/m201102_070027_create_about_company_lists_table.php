<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_company_lists}}`.
 */
class m201102_070027_create_about_company_lists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about_company_lists}}', [
            'id' => $this->primaryKey(),
            'about_company_id' => $this->integer()->notNull()->comment('Блок о компании'),
            'icon' => $this->string()->comment('Иконка')
        ]);

        $this->createIndex(
            'idx-about_company_lists-about_company_id',
            'about_company_lists',
            'about_company_id'
        );

        $this->addForeignKey(
            'fk-about_company_lists-about_company_id',
            'about_company_lists',
            'about_company_id',
            'about_company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-about_company_lists-about_company_id',
            'about_company_lists'
        );

        $this->dropIndex(
            'idx-about_company_lists-about_company_id',
            'about_company_lists'
        );

        $this->dropTable('{{%about_company_lists}}');
    }
}
