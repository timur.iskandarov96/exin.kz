<?php

use yii\db\Migration;

/**
 * Class m201229_070201_add_blocked_column_to_collections
 */
class m201229_070201_add_blocked_column_to_collections extends Migration
{
    /**
     *
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('location_collection', 'blocked', $this->boolean());
        $this->addColumn('location_status_collection', 'blocked', $this->boolean());
        $this->addColumn('location_type_collection', 'blocked', $this->boolean());
        $this->addColumn('plans_collection', 'blocked', $this->boolean());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'blocked');
        $this->dropColumn('location_status_collection', 'blocked');
        $this->dropColumn('location_type_collection', 'blocked');
        $this->dropColumn('plans_collection', 'blocked');
    }
}
