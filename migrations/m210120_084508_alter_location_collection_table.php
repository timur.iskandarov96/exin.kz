<?php

use yii\db\Migration;

/**
 * Class m210120_084508_alter_location_collection_table
 */
class m210120_084508_alter_location_collection_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('location_collection', 'priceformetre', $this->integer()->comment('Цена за кв м'));
        $this->addColumn('location_collection', 'finish', $this->string()->comment('Отделка'));
        $this->addColumn('location_collection', 'heightarea', $this->integer()->comment('Высота потолков'));
        $this->addColumn('location_collection', 'file', $this->string()->comment('Файл планировки'));
        $this->addColumn('location_collection', 'arealife', $this->float()->comment('Площадь жилая'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'priceformetre');
        $this->dropColumn('location_collection', 'finish');
        $this->dropColumn('location_collection', 'heightarea');
        $this->dropColumn('location_collection', 'file');
        $this->dropColumn('location_collection', 'arealife');
    }
}
