<?php

use yii\db\Migration;

/**
 * Class m210120_104714_add_columns_to_projects
 */
class m210120_104714_add_columns_to_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'background_image', $this->string(255)->comment('Изображение фона'));
        $this->addColumn('projects', 'scheme_image', $this->string(255)->comment('Изображение фона'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'background_image');
        $this->dropColumn('projects', 'scheme_image');
    }
}
