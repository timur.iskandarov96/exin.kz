<?php

use yii\db\Migration;

/**
 * Class m201016_092716_alter_columns_in_projects_table
 */
class m201016_092716_alter_columns_in_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'name', $this->string());

        $this->dropForeignKey(
            'fk-projects-housing_estate_id',
            'projects'
        );

        $this->dropIndex(
            'idx-projects-housing_estate_id',
            'projects'
        );

        $this->dropColumn('projects', 'housing_estate_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'name');
    }
}
