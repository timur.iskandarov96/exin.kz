<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects_translation}}`.
 */
class m201110_104929_create_projects_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%projects_translation}}', [
            'id' => $this->primaryKey(),
            'lang' => $this->string()->comment('Язык перевода'),
            'project_id' => $this->integer()->comment('Проект'),
            'address' => $this->string(512)->comment('Адрес')
        ]);

        $this->createIndex(
            'idx-projects_translation-project_id',
            'projects_translation',
            'project_id'
        );

        $this->addForeignKey(
            'fk-projects_translation-project_id',
            'projects_translation',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-projects_translation-project_id',
            'projects_translation'
        );

        $this->dropIndex(
            'idx-projects_translation-project_id',
            'projects_translation'
        );

        $this->dropTable('{{%projects_translation}}');
    }
}
