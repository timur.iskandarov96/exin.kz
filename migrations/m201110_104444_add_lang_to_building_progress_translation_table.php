<?php

use yii\db\Migration;

/**
 * Class m201110_104444_add_lang_to_building_progress_translation_table
 */
class m201110_104444_add_lang_to_building_progress_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('building_progress_translation', 'lang', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('building_progress_translation', 'lang');
    }
}
