<?php

use yii\db\Migration;

/**
 * Class m210120_094157_alter_location_collection_table
 */
class m210120_094157_alter_location_collection_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('location_collection', 'heightarea',$this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
