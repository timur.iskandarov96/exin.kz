<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%promo_banners}}`.
 */
class m201112_073420_create_promo_banners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%promo_banners}}', [
            'id' => $this->primaryKey(),
            'is_active' => $this->boolean()->comment('Активность'),
            'position' => $this->integer()->comment('Порядок очередности'),
            'image' => $this->string()->comment('Изображение'),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%promo_banners}}');
    }
}
