<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%idx_and_fk_in_about_copmany_list}}`.
 */
class m201102_115433_drop_idx_and_fk_in_about_copmany_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-about_company_lists-about_company_id',
            'about_company_lists'
        );

        $this->dropIndex(
            'idx-about_company_lists-about_company_id',
            'about_company_lists'
        );

        $this->dropColumn('about_company_lists', 'about_company_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('about_company_lists', 'about_company_id', $this->integer());

        $this->createIndex(
            'idx-about_company_lists-about_company_id',
            'about_company_lists',
            'about_company_id'
        );

        $this->addForeignKey(
            'fk-about_company_lists-about_company_id',
            'about_company_lists',
            'about_company_id',
            'about_company',
            'id',
            'CASCADE'
        );
    }
}
