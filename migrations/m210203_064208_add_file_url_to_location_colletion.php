<?php

use yii\db\Migration;

/**
 * Class m210203_064208_add_file_url_to_location_colletion
 */
class m210203_064208_add_file_url_to_location_colletion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('location_collection', 'file_path', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('location_collection', 'file_path');
    }
}
