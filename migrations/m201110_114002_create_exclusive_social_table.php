<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%exclusive_social}}`.
 */
class m201110_114002_create_exclusive_social_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%exclusive_social}}', [
            'id' => $this->primaryKey(),
            'publication_date' => $this->dateTime()->notNull()->comment('Дата публикации'),
            'position' => $this->integer()->comment('Порядок вывода'),
            'is_active' => $this->boolean()->comment('Активность'),
            'image' => $this->string()->comment('Изображение'),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%exclusive_social}}');
    }
}
