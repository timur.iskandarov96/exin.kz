<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%statistics_main_translation}}`.
 */
class m201207_105605_create_statistics_main_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%statistics_main_translation}}', [
            'id' => $this->primaryKey(),
            'stat_id' => $this->integer()->comment('Блок статистики'),
            'title' => $this->string(255)->comment('Заголовок'),
            'text' => $this->string(255)->comment('Текст')
        ]);

        $this->createIndex(
            'idx-statistics_main_translation-stat_id',
            'statistics_main_translation',
            'stat_id'
        );

        $this->addForeignKey(
            'fk-statistics_main_translation-stat_id',
            'statistics_main_translation',
            'stat_id',
            'statistics_main',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-statistics_main_translation-stat_id',
            'statistics_main_translation'
        );

        $this->dropIndex(
            'idx-statistics_main_translation-stat_id',
            'statistics_main_translation'
        );

        $this->dropTable('{{%statistics_main_translation}}');
    }
}
