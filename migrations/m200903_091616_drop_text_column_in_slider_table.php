<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%text_column_in_slider}}`.
 */
class m200903_091616_drop_text_column_in_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('slider', 'text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('slider', 'text', $this->text());
    }
}
