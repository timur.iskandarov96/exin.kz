<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vacancies}}`.
 */
class m201126_131734_create_vacancies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vacancies}}', [
            'id' => $this->primaryKey(),
            'is_active' => $this->boolean()->comment('Активность'),
            'salary' => $this->integer()->unsigned()->comment('Заработная плата'),
            'publication_date' => $this->date()->comment('Дата публикации'),
            'responsible_person_image' => $this->string()->comment('Фото ответственного лица'),
            'responsible_person_phone' => $this->string()->comment('Телефон ответственного лица'),
            'responsible_person_email' => $this->string()->comment('E-mail ответственного лица'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vacancies}}');
    }
}
