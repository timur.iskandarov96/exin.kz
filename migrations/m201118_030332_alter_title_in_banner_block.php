<?php

use yii\db\Migration;

/**
 * Class m201118_030332_alter_title_in_banner_block
 */
class m201118_030332_alter_title_in_banner_block extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('promo_banners_translation', 'title', $this->text());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
