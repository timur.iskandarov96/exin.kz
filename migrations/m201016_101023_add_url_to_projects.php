<?php

use yii\db\Migration;

/**
 * Class m201016_101023_add_url_to_projects
 */
class m201016_101023_add_url_to_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'url', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'url');
    }
}
