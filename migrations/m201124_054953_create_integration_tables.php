<?php

use yii\db\Migration;

/**
 * Class m201124_054953_create_integration_tables
 */
class m201124_054953_create_integration_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cities_collection', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(256)->comment('UUID'),
            'name' => $this->string(256)->comment('Название'),
            'blocked' => $this->boolean()->comment('Заблочен')
        ]);

        $this->createTable('districts_collection', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(256)->comment('UUID'),
            'name' => $this->string(256)->comment('Название'),
            'city' => $this->string(256)->comment('Город'),
            'blocked' => $this->boolean()->comment('Заблочен')
        ]);

        $this->createTable('projects_collection', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(256)->comment('UUID'),
            'name' => $this->string(256)->comment('Название'),
            'city' => $this->string(256)->comment('Город'),
            'district' => $this->string(256)->comment('Район'),
            'blocked' => $this->boolean()->comment('Заблочен')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cities_collection');
        $this->dropTable('districts_collection');
        $this->dropTable('projects_collection');
    }
}
