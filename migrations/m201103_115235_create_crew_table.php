<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crew}}`.
 */
class m201103_115235_create_crew_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crew}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->comment('Фото'),
            'is_active' => $this->boolean()->comment('Активность'),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crew}}');
    }
}
