<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crew_translation}}`.
 */
class m201103_115422_create_crew_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crew_translation}}', [
            'id' => $this->primaryKey(),
            'crew_id' => $this->integer(),
            'full_name' => $this->string()->notNull()->comment('ФИО'),
            'job_position' => $this->string()->notNull()->comment('Должность')
        ]);

        $this->createIndex(
            'idx-crew_translation-crew_id',
            'crew_translation',
            'crew_id'
        );

        $this->addForeignKey(
            'fk-crew_translation-crew_id',
            'crew_translation',
            'crew_id',
            'crew',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-crew_translation-crew_id',
            'crew_translation'
        );

        $this->dropIndex(
            'idx-crew_translation-crew_id',
            'crew_translation'
        );

        $this->dropTable('{{%crew_translation}}');
    }
}
