<?php

use yii\db\Migration;

/**
 * Class m201207_111430_add_lang_to_statistics_main_table
 */
class m201207_111430_add_lang_to_statistics_main_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('statistics_main_translation', 'lang', $this->string()->comment('Язык перевода'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('statistics_main_translation', 'lang');
    }
}
