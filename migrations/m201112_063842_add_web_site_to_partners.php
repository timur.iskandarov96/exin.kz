<?php

use yii\db\Migration;

/**
 * Class m201112_063842_add_web_site_to_partners
 */
class m201112_063842_add_web_site_to_partners extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('partners', 'web_site', $this->string(256));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('partners', 'web_site');
    }
}
