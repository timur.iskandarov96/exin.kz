<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_company_translation}}`.
 */
class m201102_053111_create_about_company_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about_company_translation}}', [
            'id' => $this->primaryKey(),
            'lang' => $this->string()->notNull()->comment('Язык перевода'),
            'about_company_id' => $this->integer()->notNull()->comment('Блок страницы'),
            'title' => $this->string()->notNull()->comment('Заголовок'),
            'subtitle' => $this->string()->comment('Подзаголовок'),
            'description' => $this->text()->comment('Описание')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%about_company_translation}}');
    }
}
