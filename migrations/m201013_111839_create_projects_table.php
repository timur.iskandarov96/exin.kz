<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m201013_111839_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'housing_estate_id' => $this->integer()->comment('Проект'),
            'image' => $this->string(256)->comment('Картинка'),
            'logo' => $this->string(256)->comment('Логотип'),
            'address' => $this->string(512)->comment('Адрес'),
            'status_id' => $this->integer()->comment('Статус'),
            'is_active' => $this->boolean()->comment('Активность'),
            'position' => $this->integer()->notNull()->comment('Порядок вывода'),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления')
        ]);

        $this->createIndex(
            'idx-projects-status_id',
            'projects',
            'status_id'
        );

        $this->addForeignKey(
            'fk-projects-status_id',
            'projects',
            'status_id',
            'project_statuses',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-projects-housing_estate_id',
            'projects',
            'housing_estate_id'
        );

        $this->addForeignKey(
            'fk-projects-housing_estate_id',
            'projects',
            'housing_estate_id',
            'housing_estates',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-projects-status_id',
            'projects'
        );

        $this->dropIndex(
            'idx-projects-status_id',
            'projects'
        );

        $this->dropForeignKey(
            'fk-projects-housing_estate_id',
            'projects'
        );

        $this->dropIndex(
            'idx-projects-housing_estate_id',
            'projects'
        );

        $this->dropTable('{{%projects}}');
    }
}
