<?php

use yii\db\Migration;

/**
 * Class m201013_114629_insert_into_project_statuses_table
 */
class m201013_114629_insert_into_project_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('project_statuses', [
            'status' => 'сдан'
        ]);

        $this->insert('project_statuses', [
            'status' => 'строится'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201013_114629_insert_into_project_statuses_table cannot be reverted.\n";

        return false;
    }
}
