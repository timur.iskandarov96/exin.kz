<?php

use yii\db\Migration;

/**
 * Class m210203_073909_alter_location_collection_price_column
 */
class m210203_073909_alter_location_collection_price_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('location_collection', 'price', $this->integer()->unsigned());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210203_073909_alter_location_collection_price_column cannot be reverted.\n";

        return false;
    }
}
