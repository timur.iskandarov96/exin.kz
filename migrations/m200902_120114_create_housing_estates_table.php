<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%housing_estates}}`.
 */
class m200902_120114_create_housing_estates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%housing_estates}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%housing_estates}}');
    }
}
