<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%projects}}`.
 */
class m210319_061128_add_has_locations_column_to_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            'projects',
            'no_locations',
            $this->boolean()
                ->defaultValue(false)
                ->comment('Все квартиры проданы')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
