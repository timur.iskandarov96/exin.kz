<?php


namespace app\widgets;


use app\modules\partner\models\Partner;
use yii\base\Widget;

class Partners extends Widget
{
    public function run()
    {
        return $this->render('_partners', [
            'partners' => Partner::find()->active()->orderBy('position')->all()
        ]);
    }
}