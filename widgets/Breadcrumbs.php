<?php


namespace app\widgets;


use yii\base\Widget;

class Breadcrumbs extends Widget
{
    public $elements;
    
    public function init()
    {
        parent::init();
        if ($this->elements === null) {
            $this->elements = [];
        }
    }

    public function run()
    {
        return $this->render('_breadcrumbs', [
            'elements' => $this->elements
        ]);
    }
}