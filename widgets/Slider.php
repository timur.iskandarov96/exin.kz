<?php


namespace app\widgets;


use yii\base\Widget;

class Slider extends Widget
{
    public $districtsCollection;
    public $slides;

    public function init()
    {
        parent::init();
        if ($this->districtsCollection === null) {
            $this->districtsCollection = [];
        }

        if ($this->slides === null) {
            $this->slides = [];
        }
    }

    public function run()
    {
        return $this->render('_slider', [
            'districtsCollection' => $this->districtsCollection,
            'slides' => $this->slides
        ]);
    }
}