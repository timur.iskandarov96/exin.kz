<?php

use yii\helpers\Html;

/* @var $partners \app\modules\partner\models\Partner[] */

?>

<section class="partners pr-pages">
    <div class="container">
        <div class="partners__wrap">
            <div class="partners__title-block">
                <h3><?= \Yii::t('front', 'Партнёры') ?></h3>
                <div class="partners__controls">
                    <div class="partners__btn partners__btn--prev"></div>
                    <div class="partners__btn partners__btn--next"></div>
                </div>
            </div>
            <div class="swiper-container partners__slider">
                <div class="swiper-wrapper">
                    <?php foreach ($partners as $key => $partner): ?>
                        <div class="swiper-slide partners__slide">
                            <?= Html::a(
                                Html::img(
                                    $partner->getThumbUploadUrl('image'), [
                                        'alt' => 'Партнер № '.($key + 1),
                                        'width' => 286,
                                        'height' => 205
                                    ]),
                                $partner->web_site,
                                [
                                    'target' => '_blank'
                                ]
                            )
                            ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
