<?php

use yii\helpers\Url;

/* @var $pageUrl string */
/* @var $paginator app\components\front\Paginator */

?>

<div class="pagination">
    <span class="pagination__title"><?= \Yii::t('front', 'Страницы').':' ?></span>
    <ul class="pagination__list">
        <?php foreach ($paginator->getRange() as $page): ?>
            <?php $active = $page === $paginator->getPage() ? ' active' : '' ?>
            <li class="<?= 'pagination__item'.$active ?>">
                <a href="<?= Url::to("/$pageUrl/page/$page") ?>"><?= $page ?></a>
            </li>
        <?php endforeach; ?>
        <?php if ($paginator->hasTail()): ?>
            <li class="pagination__item">
                <p>...</p>
            </li>
            <li class="pagination__item">
                <a href="<?= Url::to("/$pageUrl/page/$page".$paginator->getTail()) ?>"><?= $paginator->getTail() ?></a>
            </li>
        <?php endif; ?>
    </ul>
</div>
