<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $contactForm \app\models\ContactForm */
/* @var $housingEstates array */

$this->registerCss(
    <<<CSS
    #he-error {
        display: none;
    }
    
    .help-block {
        color: #FF4C4C
    }
CSS

);

?>

<h3><?= \Yii::t('front', 'Есть вопросы?') ?></h3>
<p><?= \Yii::t('front', 'Мы перезвоним вам в ближайшее время, чтобы ответить на все интересующие вас вопросы.') ?></p>
<?php $form = ActiveForm::begin([
    'id' => 'contact-form-modal',
    'method' => 'POST',
    'action' => ['/site/save-request']
]) ?>
    <?php $svgPath = Url::to('@web/images/symbol/svg/sprite.symbol.svg#name'); ?>
    <?= $form->field($contactForm, 'name', [
        'template' => '{label}{input}{error}',
        'options' => [
            'class' => 'input-block',
        ]
    ])->textInput([
        'required' => true,
        'placeholder' => \Yii::t('front', 'Имя'),
        'type' => 'text',
        'class' => 'input--item input--item__faq'
    ])->label("<svg class='svg-icon svg-icon-name'>
                        <use xlink:href='$svgPath'></use>
                    </svg>") ?>
    <?php $svgPath = Url::to('@web/images/symbol/svg/sprite.symbol.svg#tel'); ?>
    <?= $form->field($contactForm, 'phone', [
        'template' => '{label}{input}{error}',
        'options' => [
            'class' => 'input-block',
        ]
    ])->textInput([
        'required' => true,
        'placeholder' => \Yii::t('front', 'Номер телефона'),
        'inputmode' => 'tel',
        'type' => 'tel',
    ])->label("<svg class='svg-icon svg-icon-tel'>
                          <use xlink:href='$svgPath'></use>
                        </svg>") ?>

    <div class="select-form custom-select" id="select1">
        <div class="select-form__head">
            <label><svg class="svg-icon svg-icon-home">
                    <use xlink:href="<?=Url::to('@web/images/symbol/svg/sprite.symbol.svg#home');?>"></use>
                </svg></label>
            <span
                    class="select-form__title"
                    data-placeholder="<?= \Yii::t('front', 'ЖК') ?>"
            >
                <?= \Yii::t('front', 'ЖК') ?>
            </span>
        </div>

        <?= Html::activeHiddenInput(
            $contactForm,
            'housingEstateId',
            ['data-select-input' => '', 'value' => null, 'id' => 'modal-contact-housing_estate'] ) ?>

        <ul class="select-form__options">
            <?php foreach ($housingEstates as $id => $name): ?>
                <li data-select-value="<?= $id ?>"><?= $name ?></li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="input-block question-btn">
        <?= Html::submitInput(\Yii::t('front', 'Заказать звонок'), [
            'value' => \Yii::t('front', 'Заказать звонок'),
            'id' => 'modal-btn',
            'class' => 'btn'
        ] ) ?>
    </div>

    <?= Html::hiddenInput('form_token_param', $contactForm::getFormToken()) ?>

<?php ActiveForm::end() ?>

<?php
$js = <<<JS

var form2 = $('#contact-form-modal');

form2.on('beforeSubmit', function(e) {
    const submitBtn = form2.find('#modal-btn');
    
    submitBtn.prop('disabled', true);
    setTimeout(function() {submitBtn.prop('disabled', false); }, 3000);
    
    const data = form2.serialize();
    
    $.ajax({
        url: form2.attr('action'),
        type: 'POST',
        data: data,
        success: function (data) {
            form2[0].reset();
            $('.select-form__title').html('ЖК');
            modalManager('#success').open();
            modalManager('#modal-question').close();            
        },
        error: function(jqXHR, errMsg) {
            modalManager('#error').open();    
        }
     });
     return false; // prevent default submit
});

JS;

$this->registerJs($js);

?>
