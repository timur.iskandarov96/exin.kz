<?php
use Bridge\Core\Models\Settings;
use yii\helpers\Url;
?>

<section class="contacts pr-pages">
    <div class="container">
        <h2 class="contacts__title"><?= \Yii::t('front', 'Контакты') ?></h2>
        <div class="footer-flex">
            <div class="block contacts__address">
                <svg class="svg-icon svg-icon-adress">
                    <use xlink:href="<?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#adress') ?>"></use>
                </svg>
                <span><?= \Yii::t('front', 'Наш адрес') ?></span>
                <p><?= Settings::group('index_page', [
                        'title' => 'Главная'
                    ])->getOrCreate('contacts-address', [
                        'title' => 'Адрес компании',
                        'type' => Settings::TYPE_STRING
                    ]);
                    ?></p>
                <a id="policy-link" href="<?= \yii\helpers\Url::to('/site/policy') ?>" target="_blank">
                    <?= \Yii::t('front', 'Политика конфиденциальности') ?>
                </a>
            </div>
            <div class="block contacts__phone-block">
                <svg class="svg-icon svg-icon-pho">
                    <use xlink:href="<?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#pho') ?>"></use>
                </svg>
                <span><?= \Yii::t('front', 'Телефоны') ?></span>
                <?php
                    $phoneMain = Settings::group('index_page', [
                        'title' => 'Главная'
                    ])->getOrCreate('contacts-main-phone', [
                        'title' => 'Телефон компании',
                        'type' => Settings::TYPE_STRING
                    ]);

                    $phoneExtra = Settings::group('index_page', [
                        'title' => 'Главная'
                    ])->getOrCreate('contacts-extra-phone', [
                        'title' => 'Дополнительный телефон компании',
                        'type' => Settings::TYPE_STRING
                ]);
                ?>
                <a class="contacts__phone" href="tel:<?= $phoneMain ?>">
                    <b><?= \app\components\front\Helper::getPrettyPhone($phoneMain) ?></b>
                </a>
                <a class="contacts__phone" href="tel:<?= $phoneExtra ?>">
                    <b><?= \app\components\front\Helper::getPrettyPhone($phoneExtra) ?></b>
                </a>
            </div>
            <div class="block">
                <svg class="svg-icon svg-icon-text">
                    <use xlink:href="<?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#text') ?>"></use>
                </svg>
                <span><?= \Yii::t('front', 'Социальные сети') ?></span>
                <div class="flex">
                    <a href="https://www.instagram.com/exclusive_qurylys/" target="_blank">
                        <svg width="51" height="51" class="svg-icon-insta" viewBox="0 0 51 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M25.3225 0C11.3597 0 0 11.3597 0 25.3225C0 39.2843 11.3597 50.645 25.3225 50.645C39.2843 50.645 50.645 39.2843 50.645 25.3225C50.645 11.3597 39.2863 0 25.3225 0Z" fill="#7EB621" />
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M39.7239 34.1587V22.3675V16.4883C39.7239 13.4181 37.2269 10.9221 34.1567 10.9221H16.4873C13.4171 10.9221 10.9211 13.4181 10.9211 16.4883V22.3675V34.1587C10.9211 37.23 13.4171 39.7259 16.4873 39.7259H34.1578C37.2269 39.7259 39.7239 37.23 39.7239 34.1587ZM36.3916 14.2393V19.121L31.5251 19.1373L31.5088 14.2546L36.3916 14.2393ZM25.3215 30.392C28.1153 30.392 30.3929 28.1184 30.3929 25.3226C30.3929 24.2189 30.0319 23.2 29.4341 22.3676C28.5131 21.0906 27.0157 20.2532 25.3246 20.2532C23.6324 20.2532 22.136 21.0896 21.2129 22.3666C20.6132 23.1989 20.2552 24.2179 20.2541 25.3216C20.2511 28.1174 22.5267 30.392 25.3215 30.392ZM33.2 25.3226C33.2 29.6648 29.6667 33.2001 25.3225 33.2001C20.9783 33.2001 17.4461 29.6648 17.4461 25.3226C17.4461 24.2781 17.6542 23.2796 18.0254 22.3677H13.7261V34.1589C13.7261 35.6828 14.9634 36.917 16.4863 36.917H34.1557C35.6765 36.917 36.9158 35.6828 36.9158 34.1589V22.3677H32.6145C32.9888 23.2796 33.2 24.2781 33.2 25.3226Z" fill="#F8F8F8" />
                        </svg>
                    </a>
                    <a href="https://www.youtube.com/channel/UC0wvPcli_1Bh0xTzBvXYI8w" target="_blank">
                        <svg width="51" height="51" class="svg-icon-you" viewBox="0 0 51 51" xmlns="http://www.w3.org/2000/svg">
                            <path d="M25.3225 0C11.3597 0 0 11.3597 0 25.3225C0 39.2843 11.3597 50.645 25.3225 50.645C39.2843 50.645 50.645 39.2843 50.645 25.3225C50.645 11.3597 39.2863 0 25.3225 0Z" fill="#7EB621" />
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M37.5204 15.6423C38.891 15.9883 39.9718 17.0011 40.3407 18.286C41.0262 20.6328 40.9999 25.5247 40.9999 25.5247C40.9999 25.5247 40.9999 30.3917 40.3409 32.7387C39.9718 34.0234 38.8912 35.0364 37.5204 35.3822C35.0163 36 24.9999 36 24.9999 36C24.9999 36 15.0097 36 12.4795 35.3577C11.1086 35.0117 10.0281 33.9987 9.65893 32.714C9 30.3917 9 25.5 9 25.5C9 25.5 9 20.6328 9.65893 18.286C10.0278 17.0013 11.135 15.9636 12.4792 15.6178C14.9834 15 24.9997 15 24.9997 15C24.9997 15 35.0163 15 37.5204 15.6423ZM31 25.5L21 31V20L31 25.5Z" fill="#F8F8F8" />
                        </svg>
                    </a>
                    <a href="https://vm.tiktok.com/ZSPmKr9P/" target="_blank">
                        <svg width="51" height="51" class="svg-icon-tick" viewBox="0 0 51 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M25.4349 0C11.3881 0 0 11.3876 0 25.4349C0 39.4823 11.3881 50.8699 25.4349 50.8699C39.4818 50.8699 50.8699 39.4823 50.8699 25.4349C50.8699 11.3876 39.4818 0 25.4349 0Z" fill="#7EB621" />
                            <path d="M36.9871 19.0108C35.4257 19.0108 33.9851 18.4806 32.8282 17.5862C31.5014 16.5609 30.5481 15.0568 30.2115 13.3233C30.1281 12.895 30.0832 12.453 30.079 12H25.6187V24.4924L25.6133 31.3351C25.6133 33.1645 24.4511 34.7156 22.84 35.2611C22.3725 35.4194 21.8675 35.4945 21.3417 35.4649C20.6705 35.4271 20.0416 35.2195 19.495 34.8843C18.3317 34.1712 17.543 32.8774 17.5216 31.3975C17.4879 29.0845 19.3122 27.1987 21.5672 27.1987C22.0123 27.1987 22.4398 27.2732 22.84 27.4085V23.994V22.7665C22.4179 22.7025 21.9883 22.669 21.5538 22.669C19.0856 22.669 16.7772 23.7207 15.1271 25.6152C13.8799 27.047 13.1318 28.8736 13.0164 30.789C12.8652 33.3052 13.7635 35.6971 15.5055 37.4619C15.7614 37.7209 16.0302 37.9614 16.3113 38.1832C17.8048 39.3614 19.6307 40 21.5538 40C21.9883 40 22.4179 39.9671 22.84 39.9031C24.6365 39.6303 26.2941 38.7873 27.6022 37.4619C29.2096 35.8335 30.0977 33.6716 30.1073 31.3707L30.0843 21.1524C30.8511 21.7587 31.6895 22.2604 32.5894 22.6499C33.9888 23.2551 35.4727 23.5618 36.9999 23.5613V20.2415V19.0097C37.001 19.0108 36.9882 19.0108 36.9871 19.0108Z" fill="#F8F8F8" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
