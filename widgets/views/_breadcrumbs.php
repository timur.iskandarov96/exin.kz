<?php

use yii\helpers\Url;

/* @var $elements array */

?>

<div class="breadcrumbs__block">
    <div class="container-main">
        <div class="link__back-btn">
            <svg class="svg-icon svg-icon-arrow-back">
                <use xlink:href="<?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#arrow-back') ?>"></use>
            </svg>
            <a href="javascript:history.back()"><?= \Yii::t('front', 'Назад') ?></a>
        </div>
        <ul class="breadcrumbs breadcrumbs--main">
            <?php foreach ($elements as $elem): ?>
                <?php if ($elem['url']): ?>
                    <li class="breadcrumbs__item breadcrumbs__item--active">
                        <a href="<?= $elem['url'] ?>"><?= $elem['title'] ?></a>
                    </li>
                <?php else: ?>
                    <li class="breadcrumbs__item">
                        <p><?= $elem['title'] ?></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

