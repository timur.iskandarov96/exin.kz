<?php

use app\modules\slider\models\Slider;
use yii\helpers\Html;

/* @var $districtsCollection array */
/* @var $slides Slider[] */

?>

<section class="hero" id="first">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php foreach ($slides as $key => $item): ?>
                <?php if ($key === 0): ?>
                    <?php $classList = 'hero__slide swiper-slide is-active' ?>
                <?php else: ?>
                    <?php $classList = 'hero__slide swiper-slide' ?>
                <?php endif; ?>

                <figure class="<?= $classList ?>">
                    <?= Html::img(
                         $item->get1920Image(),
                        [
                            'class' => 'swiper-lazy',
                            'data-src' => $item->get1920Image(),
                            'width' => 1920,
                            'height' => 1080,
                            'alt' => 'Изображение слайдера № '.($key+1)
                        ]
                    ) ?>
                    <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                    <figcaption class="swiper-lazy"><?= $item->text ?></figcaption>
                </figure>

            <?php endforeach; ?>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="hero__search pr-pages">
        <div class="container">
            <?= Html::beginForm(['/locations'], 'get', [
                    'enctype' => 'multipart/form-data',
                'class' => 'hero__search-wrap'
            ]) ?>
            <div class="hero__input-block">
                <div class="select-form hero__select" id="select-area">
                    <div class="select-form__head">
                        <span class="select-form__title"><?= \Yii::t('front', 'Район') ?></span>
                    </div>
                    <input data-select-input type="hidden" value="" name="district">
                    <ul class="select-form__options">
                        <?php foreach ($districtsCollection as $uuid => $name): ?>
                            <li data-select-value="<?= $uuid ?>"><?= \Yii::t('front', $name) ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="hero__input-block hero__input-inline">
                <div class="select-form hero__select" id="select-rooms">
                    <div class="select-form__head">
                        <span class="select-form__title"><?= \Yii::t('front', 'Комнат') ?></span>
                    </div>
                    <input data-select-input type="hidden" value="" name="room_number">
                    <ul class="select-form__options">
                        <li data-select-value="1">1</li>
                        <li data-select-value="1-2">1-2</li>
                        <li data-select-value="2">2</li>
                        <li data-select-value="2-3">2-3</li>
                        <li data-select-value="3">3</li>
                        <li data-select-value="3-4">3-4</li>
                        <li data-select-value="4">4</li>
                    </ul>
                </div>
                <input autocomplete="off" class="hero__search-cost" name="price_max" placeholder="<?= \Yii::t('front', 'Цена до') ?>"/>
                <input type="hidden" value="<?= \app\modules\collection\models\LocationType::getLocationUUUID() ?>" name="location_type">
                <input type="hidden" value="<?= \app\modules\collection\models\query\LocationQuery::getMinPrice() ?>" name="price_min">
            </div>
            <?= Html::submitButton(\Yii::t('front', 'Подобрать'), ['class' => 'hero__search-btn']) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
</section>