<?php

use app\modules\project\models\ProjectsCollection;
use yii\helpers\Url;

$projectStatuses = [
    1 => 'completed',
    2 => 'buildining'
];

?>
<section class="project-block pr-pages" id="project">
    <img src="<?= Url::to('@web/images/bg.png') ?>" alt="background" class="bg" width="1920" height="720">
    <div class="container wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.3s">

        <div class="flex">
            <h2><?= \Yii::t('front', 'Проекты') ?></h2>
            <div class="flex tabs-wrap">
                <span class="open-tab active" data-status="all"><?= \Yii::t('front', 'Все') ?></span>
                <span class="open-tab" data-status="buildining"><?= \Yii::t('front', 'Строятся') ?> (<?= count(ProjectsCollection::getUnderConstruction())?>)</span>
                <span class="open-tab" data-status="completed"><?= \Yii::t('front', 'Сданы') ?> (<?= count(ProjectsCollection::getReady())?>)</span>
            </div>
        </div>

        <div class="project-block__content active block1">

            <?php foreach (ProjectsCollection::getAll() as $project): ?>
                <div class="block active" data-status="<?= $projectStatuses[$project->status->id] ?>">
                    <div class="images">
                        <img src="<?= $project->getThumbUploadUrl('image', 'thumb') ?? $project->getUploadUrl('image') ?> " alt="house" class="fon" width="372" height="280" >
                        <a href="<?= $project->url ?>" target="_blank" class="zoom">
                            <?php $logoUrl = "background-image: url('".$project->getLogoUrl()."')" ?>
                            <div class="project-block__icon" style="<?= $logoUrl ?>"></div>
                        </a>
                    </div>
                    <div class="block--content">
                        <span class="state"><?= \Yii::t('front', $project->status->status) ?></span>
                        <h3>
                            <a href="<?= $project->url ?>" target="_blank"><?= $project->name ?></a>
                        </h3>
                        <span class="place"><svg class="svg-icon svg-icon-place">
                            <use xlink:href="images/symbol/svg/sprite.symbol.svg#place"></use>
                        </svg><?= $project->translation->address ?></span>
                        <div class="price-list">
                            <?php if ($project->no_locations): ?>
                                <hr>
                                <ul><li><span><?= \Yii::t('front', 'Все квартиры от застройщика проданы') ?></span></li></ul>
                            <?php else: ?>
                                <?php if (count($project->apartments) > 0): ?>
                                    <hr>
                                    <ul>
                                        <?php foreach ($project->apartments as $apartment): ?>
                                            <li>
                                                <span>
                                                    <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                                        <?= $apartment->number_of_rooms ?>-комн. от
                                                    <?php else: ?>
                                                        <?= $apartment->number_of_rooms ?> бөлмелі
                                                    <?php endif ?>
                                                    <?php if ($apartment->max_square): ?>
                                                        <?= number_format($apartment->square, 1, ',', ''); ?> -
                                                        <?= number_format($apartment->max_square, 1, ',', ''); ?> м²
                                                    <?php else: ?>
                                                        <?= number_format($apartment->square, 1, ',', ''); ?> м²
                                                    <?php endif; ?>
                                                    <?php if (\Yii::$app->language === 'kk-KZ'): ?>
                                                        <?= " бастап" ?>
                                                    <?php endif; ?>
                                                </span>
                                                <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                                    <b>от <?= number_format($apartment->price, 0, '', ' '); ?> тг</b>
                                                <?php else: ?>
                                                    <b><?= number_format($apartment->price, 0, '', ' '); ?> тг бастап</b>
                                                <?php endif ?>

                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>

    </div>

</section>