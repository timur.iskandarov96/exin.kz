<?php


namespace app\widgets;

use yii\base\Widget;

class Contacts extends  Widget
{
    public function run()
    {
        return $this->render('_contacts');
    }
}