<?php


namespace app\widgets;


use yii\base\Widget;

class Projects extends Widget
{
    public function run()
    {
        return $this->render('_projects');
    }
}