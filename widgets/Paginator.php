<?php


namespace app\widgets;


use yii\base\Widget;

class Paginator extends Widget
{
    public $paginator;

    public $pageUrl;

    public function init()
    {
        parent::init();
        if ($this->paginator === null) {
            $this->paginator = '';
        }
    }

    public function run()
    {
        return $this->render('_paginator', [
            'paginator' => $this->paginator,
            'pageUrl' => $this->pageUrl
        ]);
    }
}