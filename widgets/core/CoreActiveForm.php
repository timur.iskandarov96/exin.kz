<?php


namespace app\widgets\core;


use Bridge\Core\Widgets\MetaTagsFormWidget;
use Bridge\Core\Widgets\TranslationFormWidget;

/**
 * Almost the same as \Bridge\Widgets\ActiveForm, but allows to attach behaviours for
 * imageUpload and fileUpload, so that multiple files and images could be attached to different behaviours
 *
 * Class CoreActiveForm
 * @package app\widgets\core
 */
class CoreActiveForm extends \yii\widgets\ActiveForm
{
    /**
     * @inheritdoc
     */
    public $fieldClass = 'app\widgets\core\CoreActiveField';

    /**
     * @inheritdoc
     * @return CoreActiveField
     */
    public function field($model, $attribute, $options = [])
    {
        return parent::field($model, $attribute, $options);
    }

    public function translate($model, $viewName, $languages = null)
    {
        return TranslationFormWidget::widget([
            'form' => $this,
            'model' => $model,
            'languages' => $languages,
            'viewName' => $viewName
        ]);
    }

    public function metaTags($model, $viewName = '@app/vendor/yii2-bridge/core/src/views/meta-page/_form-meta-tags', $languages = null)
    {
        return MetaTagsFormWidget::widget([
            'form' => $this,
            'model' => $model,
            'languages' => $languages,
            'viewName' => $viewName
        ]);
    }
}