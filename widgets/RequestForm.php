<?php


namespace app\widgets;


use app\models\ContactForm;
use yii\base\Widget;


class RequestForm extends Widget
{
    public $contactForm;

    public $housingEstates;

    public function init()
    {
        parent::init();
        if ($this->contactForm === null) {
            $this->contactForm = new ContactForm();
        }

        if ($this->housingEstates === null) {
            $this->housingEstates = [];
        }
    }

    public function run()
    {
        return $this->render('_request_form', [
            'contactForm' => $this->contactForm,
            'housingEstates' => $this->housingEstates
        ]);
    }
}