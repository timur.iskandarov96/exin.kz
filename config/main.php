<?php

use yii\web\UrlNormalizer;

$params = require __DIR__ . '/params.php';

$db = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2bridge',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8mb4',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];

return [
    'yiiDebug' => false,
    'yiiEnv' => 'prod',
    'yiiPath' => dirname(__DIR__) . '/vendor/yiisoft/yii2/Yii.php',
    'web' => [
        'id' => 'exin.abpx.kz',
        'language' => 'ru-RU',
        'name' => 'exin.abpx.kz',
        'basePath' => dirname(__DIR__),
        'timeZone'   => 'Asia/Almaty',
        'bootstrap' => [
            'log', 'admin',
            '\app\events\Bootstrap'
        ],
        'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm' => '@vendor/npm-asset',
        ],
        'components' => [
            'request' => [
                // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                'cookieValidationKey' => '1AD948uWLSSg14DNbb-_x257YmzAydFi',
                'parsers' => [
                    'application/json' => 'yii\web\JsonParser',
                ]
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'user' => [
                'identityClass' => \app\models\User::class,
                'enableAutoLogin' => true,
            ],
            'i18n' => [
                'translations' => [
                    'admin' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'sourceLanguage' => 'en-US',
                        'basePath' => '@app/messages',
                    ]
                ]
            ],
            'authManager' => [
                'class' => \Da\User\Component\AuthDbManagerComponent::class,
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'useFileTransport' => false,
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'mail.exin.kz',
                    'encryption' => 'tls',
                    'port' => '587',
                    'username' => getenv('MAIL_USER'),
                    'password' => getenv('MAIL_PASSWORD')
                ]
            ],
            'log' => [
                'traceLevel' => 3,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'db' => $db,
            'urlManager' => [
                'class' => \codemix\localeurls\UrlManager::class,
                'languages' => [
                    'ru' => 'ru-RU',
                    'kk' => 'kk-KZ',
                ],
                'normalizer' =>[
                    'class' => 'yii\web\UrlNormalizer',
                    // use temporary redirection instead of permanent for debugging
                    'action' => UrlNormalizer::ACTION_REDIRECT_PERMANENT,
                ],
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                //'enableStrictParsing' => true,
                //'enableLanguageDetection' => false,
                'rules' => [
                    '/' => 'site/index',
                    'about' => 'aboutCompany/front/index',
                    'our-team' => 'crew/front/index',
                    'building-progress' => 'buildingProgress/front/index',
                    'exclusive-social' => 'exclusiveSocial/front/index',
                    'exclusive-social/page/<page>' => 'exclusiveSocial/front/index',
                    'exclusive-social/sort-limit/<sort-limit>' => 'exclusiveSocial/front/index',
                    'exclusive-social/<slug>' => 'exclusiveSocial/front/view',
                    'news' => 'news/front/index',
                        'news/page/<page>' => 'news/front/index',
                    'news/sort-limit/<sort-limit>' => 'news/front/index',
                    'news/<slug>' => 'news/front/view',
                    'contacts' => 'site/contact',
                    'favorites' => 'collection/location/favorites',
                    'vacancy/<slug>' => 'vacancy/front/view',
                    'locations' => 'collection/location',
                    'locations-new' => 'atomicCollection/location',
                    'locations/<id>' => 'collection/location/view',
                    'toggle-favorites' => 'collection/location/toggle-favorites',
                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'api/city',
                        'tokens' => [
                            '{id}' => '<id:\\w+>'
                        ],
                        'extraPatterns' => [
                            'GET <id>' => 'view',
                            'POST batch-insert' => 'batch-insert',
                            'POST batch-insert-and-clear' => 'batch-insert-and-clear',
                            'POST batch-update' => 'batch-update'
                        ]
                    ],
                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'api/district',
                        'tokens' => [
                            '{id}' => '<id:\\w+>',
                            '{lang}' => '<lang:\\w+>'
                        ],
                        'extraPatterns' => [
                            'GET get-translation/<lang>' => 'get-translation',
                            'GET <id>' => 'view',
                            'POST batch-insert' => 'batch-insert',
                            'POST batch-insert-and-clear' => 'batch-insert-and-clear',
                            'POST batch-update' => 'batch-update'
                        ]
                    ],
                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'api/project',
                        'tokens' => [
                            '{id}' => '<id:\\w+>'
                        ],
                        'extraPatterns' => [
                            'GET <id>' => 'view',
                            'POST batch-insert' => 'batch-insert',
                            'POST batch-insert-and-clear' => 'batch-insert-and-clear',
                            'POST batch-update' => 'batch-update'
                        ]
                    ],
                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'api/location',
                        'tokens' => [
                            '{id}' => '<id:\\w+>'
                        ],
                        'extraPatterns' => [
                            'GET filter-data' => 'filter-data',
                            'GET <id>' => 'view',
                            'POST batch-insert' => 'batch-insert',
                            'POST batch-insert-and-clear' => 'batch-insert-and-clear',
                            'POST batch-update' => 'batch-update',
                        ]
                    ],
                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'api/locationtype',
                        'tokens' => [
                            '{id}' => '<id:\\w+>',
                            '{lang}' => '<lang:\\w+>'
                        ],
                        'extraPatterns' => [
                            'GET get-translation/<lang>' => 'get-translation',
                            'GET <id>' => 'view',
                            'POST batch-insert' => 'batch-insert',
                            'POST batch-insert-and-clear' => 'batch-insert-and-clear',
                            'POST batch-update' => 'batch-update'
                        ]
                    ],
//                    [
//                        'class' => 'yii\rest\UrlRule',
//                        'controller' => 'api/locationstatus',
//                        'tokens' => [
//                            '{id}' => '<id:\\w+>'
//                        ],
//                        'extraPatterns' => [
//                            'GET <id>' => 'view',
//                            'POST batch-insert' => 'batch-insert',
//                            'POST batch-update' => 'batch-update'
//                        ]
//                    ],
//                    [
//                        'class' => 'yii\rest\UrlRule',
//                        'controller' => 'api/plan',
//                        'tokens' => [
//                            '{id}' => '<id:\\w+>'
//                        ],
//                        'extraPatterns' => [
//                            'GET <id>' => 'view',
//                            'POST batch-insert' => 'batch-insert',
//                            'POST batch-update' => 'batch-update'
//                        ]
//                    ],
                    'sitemap.xml' => 'sitemap/index'
                ],
                'ignoreLanguageUrlPatterns' => [
                    '#^api/#' => '#^api/#',
                    '#^admin/buildingProgress/building-progress/get-building-progress#' => '#^admin/buildingProgress/building-progress/get-building-progress#',
                    '#^sitemap.xml/#' => '#^sitemap.xml/#',
                    '#^toggle-favorites#' => '#^toggle-favorites#',
                ],
            ],
            'assetManager' => [
                'appendTimestamp' => true,
                'bundles' => [
                    'yii\web\JqueryAsset' => [
                        'js' => [
                            'jquery.min.js'
                        ]
                    ],
                ],
            ],
        ],
        'modules' => [
            'admin' => [
                'class' => \Bridge\Core\BridgeModule::class,
                'userClass' => \app\models\User::class,
                'userSettings' => [
                    'class' => \Da\User\Module::class,
                    'administratorPermissionName' => 'admin'
                ],
                'dashboardAction' => 'app\controllers\actions\DashboardAction',
                'languages' => [
                    'ru-RU' => 'RU',
                    'kk-KZ' => 'KZ',
                ],
                'composeMenu' => function ($user, $roles, $authManager) {
                    /**
                     * @var \yii\web\User $user
                     * @var \Da\User\Model\Role[] $roles
                     * @var \Da\User\Component\AuthDbManagerComponent $authManager
                     */
                    return require __DIR__ . '/menu/admin.php';
                },
                'modules' => [
                    'gii' => [
                        'class' => 'yii\gii\Module',
                    ],
                    'request' => [
                        'class' => 'app\modules\request\Module',
                    ],
                    'slider' => [
                        'class' => 'app\modules\slider\Module',
                    ],
                    'project' => [
                        'class' => 'app\modules\project\Module',
                    ],
                    'atomicCollection' => [
                        'class' => 'app\modules\atomicCollection\Module',
                    ],
                    'collection' => [
                        'class' => 'app\modules\collection\Module',
                    ],
                    'aboutCompany' => [
                        'class' => 'app\modules\aboutCompany\Module',
                    ],
                    'crew' => [
                        'class' => 'app\modules\crew\Module',
                    ],
                    'buildingProgress' => [
                        'class' => 'app\modules\buildingProgress\Module',
                    ],
                    'exclusiveSocial' => [
                        'class' => 'app\modules\exclusiveSocial\Module',
                    ],
                    'partner' => [
                        'class' => 'app\modules\partner\Module',
                    ],
                    'promoBanner' => [
                        'class' => 'app\modules\promoBanner\Module',
                    ],
                    'news' => [
                        'class' => 'app\modules\news\Module',
                    ],
                    'vacancy' => [
                        'class' => 'app\modules\vacancy\Module',
                    ],
                    'stat' => [
                        'class' => 'app\modules\stat\Module',
                    ],
                ]
            ],
            'aboutCompany' => [
                'class' => 'app\modules\aboutCompany\Module',
            ],
            'crew' => [
                'class' => 'app\modules\crew\Module',
            ],
            'buildingProgress' => [
                'class' => 'app\modules\buildingProgress\Module',
            ],
            'exclusiveSocial' => [
                'class' => 'app\modules\exclusiveSocial\Module',
            ],
            'news' => [
                'class' => 'app\modules\news\Module',
            ],
            'vacancy' => [
                'class' => 'app\modules\vacancy\Module',
            ],
            'collection' => [
                'class' => 'app\modules\collection\Module',
            ],
            'atomicCollection' => [
                'class' => 'app\modules\atomicCollection\Module',
            ]
        ],
        'params' => $params,
    ],
    'console' => [
        'id' => 'basic-console',
        'basePath' => dirname(__DIR__),
        'bootstrap' => [
            'log', 'admin',
            '\app\events\Bootstrap'
        ],
        'controllerNamespace' => 'app\commands',
        'components' => [
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'db' => $db,
            'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'useFileTransport' => false,
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'mail.exin.kz',
                    'encryption' => 'tls',
                    'port' => '587',
                    'username' => getenv('MAIL_USER'),
                    'password' => getenv('MAIL_PASSWORD')
                ]
            ],
        ],
        'modules' => [
            'admin' => [
                'class' => \Bridge\Core\BridgeModule::class
            ]
        ],
        'params' => $params,
        'aliases' => [
            '@webroot' => dirname(dirname(__FILE__)) . '/web',
        ],
        /*
        'controllerMap' => [
            'fixture' => [ // Fixture generation command line.
                'class' => 'yii\faker\FixtureController',
            ],
        ],
        */
    ]
];
