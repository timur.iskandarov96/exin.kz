<?php

return [
    \Yii::t('bridge-admin', 'Сайт'),
    [
        'title' => 'Заявки',
        'icon' => 'envelope',
        'url' => ['/admin/request/request/index'],
        'active' => ['module' => 'request', 'controller' => 'request']
    ],
    [
        'title' => 'ЖК',
        'icon' => 'building',
        'url' => ['/admin/request/housing-estate/index'],
        'active' => ['module' => 'request', 'controller' => 'housing-estate']
    ],
    [
        'title' => 'Слайдер',
        'icon' => 'image',
        'url' => ['/admin/slider/slider/index'],
        'active' => ['module' => 'slider']
    ],
    [
        'title' => 'Статистика на главной',
        'url' => ['/admin/stat/statistics-main/index'],
        'active' => ['module' => 'stat', 'controller' => 'statistics-main'],
        'icon' => 'table'
    ],
    [
        'title' => 'Проекты',
        'icon' => 'home',
        'items' => [
            [
                'title' => 'Проекты',
                'url' => ['/admin/project/project/index'],
                'active' => ['module' => 'project', 'controller' => 'project'],
                'icon' => 'home'
            ],
            [
                'title' => 'Квартиры',
                'url' => ['/admin/project/apartment/index'],
                'active' => ['module' => 'project', 'controller' => 'apartment'],
                'icon' => 'home',
            ],
        ]
    ],
    [
        'title' => 'Наша Команда',
        'icon' => 'users',
        'url' => ['/admin/crew/crew/index'],
        'active' => ['module' => 'crew']
    ],
    [
        'title' => 'О компании',
        'icon' => 'align-justify',
        'items' => [
            [
                'title' => 'Блоки страницы',
                'url' => ['/admin/aboutCompany/about-company/index'],
                'active' => ['module' => 'aboutCompany', 'controller' => 'about-company'],
                'icon' => 'file'
            ],
            [
                'title' => 'Список с иконками',
                'url' => ['/admin/aboutCompany/about-company-list/index'],
                'active' => ['module' => 'aboutCompany', 'controller' => 'about-company-list'],
                'icon' => 'list'
            ],
            [
                'title' => 'Статистика',
                'url' => ['/admin/aboutCompany/about-company-statistics/index'],
                'active' => ['module' => 'aboutCompany', 'controller' => 'about-company-statistics'],
                'icon' => 'table'
            ],
        ]
    ],
    [
        'title' => 'Ход строительства',
        'icon' => 'spinner',
        'url' => ['/admin/buildingProgress/building-progress/index'],
        'active' => ['module' => 'buildingProgress']
    ],
    [
        'title' => 'Exclusive Social',
        'icon' => 'sticky-note',
        'url' => ['/admin/exclusiveSocial/exclusive-social/index'],
        'active' => ['module' => 'exclusiveSocial']
    ],
    [
        'title' => 'Партнеры',
        'icon' => 'address-book',
        'url' => ['/admin/partner/partner/index'],
        'active' => ['module' => 'partner']
    ],
    [
        'title' => 'Акционные баннеры',
        'icon' => 'clone',
        'url' => ['/admin/promoBanner/promo-banner/index'],
        'active' => ['module' => 'promoBanner']
    ],
    [
        'title' => 'Новости',
        'icon' => 'rss',
        'url' => ['/admin/news/news/index'],
        'active' => ['module' => 'news']
    ],
    [
        'title' => 'Вакансии',
        'icon' => 'briefcase',
        'url' => ['/admin/vacancy/vacancy/index'],
        'active' => ['module' => 'vacancy']
    ],
    \Yii::t('bridge', 'Настройка сайта'),
    [
        'title' => \Yii::t('bridge', 'Settings'),
        'url' => ['/admin/settings/index'],
        'active' => ['module' => 'admin', 'controller' => 'settings'],
        'icon' => 'gear'
    ],
    [
        'title' => \Yii::t('bridge', 'Translations'),
        'url' => ['/admin/i18n/default'],
        'active' => ['module' => 'i18n', 'controller' => 'default'],
        'icon' => 'globe'
    ],
    [
        'title' => \Yii::t('bridge', 'Users'),
        'url' => ['/user/admin/index'],
        'active' => ['module' => 'user'],
        'icon' => 'users'
    ],
    [
        'title' => \Yii::t('bridge', 'File manager'),
        'url' => ['/admin/default/elfinder'],
        'active' => ['module' => 'admin', 'controller' => 'default', 'action' => 'elfinder'],
        'icon' => 'file',
        'isVisible' => ['admin']
    ],
    [
        'title' => \Yii::t('bridge', 'Meta-tags'),
        'url' => ['/admin/meta-page/index'],
        'active' => ['module' => 'admin', 'controller' => 'meta-page'],
        'icon' => 'tags',
        'isVisible' => ['admin']
    ],
];