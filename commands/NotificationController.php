<?php


namespace app\commands;


use app\components\amocrm\AmoCrmService;
use app\components\sendpulse\EmailSender;
use app\modules\request\models\HousingEstate;
use yii\console\Controller;
use yii\db\Query;


/**
 * Class NotificationController
 * @package app\commands
 */
class NotificationController extends Controller
{
    /**
     * Creates lead in Amo CRM
     * @throws \yii\db\Exception
     *
     * @deprecated since 31.03.2021
     *
     */
    public function actionNotifyAmo()
    {
        $rows = (new Query())->select('*')
            ->from('request_tasks')
            ->where(['is_sent_to_amo' => 0])
            ->limit(10)
            ->all();

        foreach ($rows as $row) {
            $amoCrmEnumId = null;
            $amoCrmEnum = (new \yii\db\Query())
                ->select('amo_id')
                ->from('amo_housing_enums')
                ->where(['housing_estate_id' => $row['housing_estate_id']])
                ->one();

            if ($amoCrmEnum) {
                $amoCrmEnumId = (int) $amoCrmEnum['amo_id'];
            }

            if ((new AmoCrmService())->createUnsorted($row['name'], $row['phone'], $amoCrmEnumId)) {
                try {
                    \Yii::$app->db->createCommand()
                        ->update('request_tasks', ['is_sent_to_amo' => 1], ['id' => $row['id']])
                        ->execute();
                } catch (\Exception $e) {
                    \Yii::error($e->getMessage());
                }
            }
        }
    }

    /**
     * Notify admin about new request via email
     */
    public function actionSendEmail()
    {
        $rows = (new Query())->select('*')
            ->from('request_tasks')
            ->where(['is_sent_to_email' => 0])
            ->limit(10)
            ->all();

        foreach ($rows as $row) {
            $housingEstate = HousingEstate::findOne($row['housing_estate_id']);

            $name = $row['name'];
            $phone = $row['phone'];
            $housingEstateName = 'Не найдено';
            if ($housingEstate) {
                $housingEstateName = $housingEstate->name;
            }
            $utm_source = $row['utm_source'];
            $utm_medium = $row['utm_medium'];
            $utm_campaign = $row['utm_campaign'];
            $utm_term = $row['utm_term'];
            $utm_content = $row['utm_content'];
            $user_agent = $row['user_agent'];
            $ga = $row['ga'];

            try {
               \ Yii::$app->mailer->compose(
                   ['html' => 'notify-html', 'text' => 'notify-text'],
                   [
                       'name' => $name,
                       'phone' => $phone,
                       'housingEstate' => $housingEstateName,
                       'utm_source' => $utm_source,
                       'utm_medium' => $utm_medium,
                       'utm_campaign' => $utm_campaign,
                       'utm_term' => $utm_term,
                       'utm_content' => $utm_content,
                       'user_agent' => $user_agent,
                       'ga' => $ga
                   ]
               )
                    ->setFrom([getenv('MAIL_USER') => 'Exin.kz'])
                    ->setTo(getenv('MAIL_ADDRESS'))
                    ->setSubject('Новая заявка с сайта exin.kz')
                    ->send();
               \Yii::$app->db->createCommand()
                    ->update('request_tasks', ['is_sent_to_email' => 1], ['id' => $row['id']])
                    ->execute();
            } catch (\Exception $e) {
                \Yii::error('EMAIL SEND ERROR');
                \Yii::error($e->getMessage());
            }
        }
    }
}