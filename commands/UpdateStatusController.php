<?php


namespace app\commands;


use app\modules\collection\models\Location;
use yii\console\Controller;

class UpdateStatusController extends Controller
{
    public function actionStatus()
    {

        $ch = curl_init('https://cms.abpx.kz/api/collections/get/exin_plans_test?token=account-98a82c65cb0b513d2be55d1b56b599');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $responce = curl_exec($ch);

        curl_close($ch);

        $responce = json_decode($responce, true);

        if (count($responce) > 0) {
            foreach ($responce['entries'] as $entry) {

                $location = Location::find()->where([
                    'uuid' => $entry['uuid']
                ])->one();

                if (!empty($location)) {
                    if ($entry['block'] == 1) {
                        $location->blocked = '1';
                        $location->update();
                    } else {
                        $location->blocked = '0';
                        $location->update();
                    }
                } else {
                    $params = Array(
                        'data' => array(
                            '_id' => $entry['_id'],
                            'del' => true
                        ),
                    );

                    $ch1 = curl_init('https://cms.abpx.kz/api/collections/save/exin_plans_test?token=account-98a82c65cb0b513d2be55d1b56b599');
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch1, CURLOPT_HEADER, false);
                    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($params));
                    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

                    $responce = curl_exec($ch1);

                    curl_close($ch1);
                    
                }

            }
        }

    }
}