<?php


namespace app\commands;


use app\modules\collection\models\Location;
use yii\console\Controller;
use yii\helpers\FileHelper;

class LocationImageController extends Controller
{
    public function actionSave()
    {
        $locations = Location::find()->where([
            'image_saved' => false,
            'image_saving_error' => null
        ])->limit(10)->all();

        if (count($locations) === 0) {
            $locations = Location::find()->where([
                'image_saved' => false,
                'image_saving_error' => true
            ])->limit(10)->all();
        }

        foreach ($locations as $l) {
            try {
                $path = \Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR .
                    'media' . DIRECTORY_SEPARATOR . Location::tableName() . DIRECTORY_SEPARATOR . $l->uuid;

                FileHelper::createDirectory($path);

                $fileName = md5(microtime()).'.'.pathinfo($l->file)['extension'];
                $fullPath = $path.DIRECTORY_SEPARATOR.$fileName;
                $fp = fopen($fullPath, 'w+');

                $ch = curl_init($l->file);
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
                curl_exec($ch);

                curl_close($ch);
                fclose($fp);

                $l->file_path = $fileName;
                $l->image_saved = true;
                $l->blocked = false;
                $l->update(false);
            } catch (\Exception $e) {
                \Yii::error('IMAGE SAVING COMMAND ERROR');
                \Yii::error($l->uuid);
                \Yii::error($e->getMessage());

                $l->blocked = true;
                $l->image_saving_error = true;
                $l->update(false);
            }

        }
    }
}