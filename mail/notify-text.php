Имя: <?= $name ?>
Телефон: <?= $phone ?>
Жк: <?= $housingEstate ?>

<?php if ($utm_source != 'no') { ?>
    utm_source: <?= $utm_source ?>
<?php } ?>

<?php if ($utm_medium != 'no') { ?>
    utm_medium: <?= $utm_medium ?>
<?php } ?>

<?php if ($utm_campaign != 'no') { ?>
    utm_campaign: <?= $utm_campaign ?>
<?php } ?>

<?php if ($utm_term != 'no') { ?>
    utm_term: <?= $utm_term ?>
<?php } ?>

<?php if ($utm_content != 'no') { ?>
    utm_content: <?= $utm_content ?>
<?php } ?>

<?php if ($user_agent != 'no') { ?>
    user_agent: <?= $user_agent ?>
<?php } ?>

<?php if ($ga != 'no') { ?>
    ga: <?= $ga ?>
<?php } ?>