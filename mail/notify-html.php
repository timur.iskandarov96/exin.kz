<p>Имя: <?= $name ?></p>
<p>Телефон: <?= $phone ?></p>
<p>Жк: <?= $housingEstate ?></p>

<?php if ($utm_source != 'no') { ?>
    <p>utm_source: <?= $utm_source ?></p>
<?php } ?>

<?php if ($utm_medium != 'no') { ?>
    <p>utm_medium: <?= $utm_medium ?></p>
<?php } ?>

<?php if ($utm_campaign != 'no') { ?>
    <p>utm_campaign: <?= $utm_campaign ?></p>
<?php } ?>

<?php if ($utm_term != 'no') { ?>
    <p>utm_term: <?= $utm_term ?></p>
<?php } ?>

<?php if ($utm_content != 'no') { ?>
    <p>utm_content: <?= $utm_content ?></p>
<?php } ?>

<?php if ($user_agent != 'no') { ?>
    <p>user_agent: <?= $user_agent ?></p>
<?php } ?>

<?php if ($ga != 'no') { ?>
    <p>ga: <?= $ga ?></p>
<?php } ?>