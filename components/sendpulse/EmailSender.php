<?php

namespace app\components\sendpulse;


use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

/**
 * Class EmailSender
 * @package app\components\sendpulse
 */
class EmailSender
{
    private $spProxy;

    const RECIPIENT = 'crm.exclusive@exin.kz';

    /**
     * EmailSender constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $id = $_ENV['SENDPULSE_ID'];
        $secret = $_ENV['SENDPULSE_SECRET'];

        $this->spProxy = new ApiClient($id, $secret, new FileStorage());
    }

    /**
     * @param string $htmlMsg
     * @param string $textMsg
     */
    public function sendEmail(string $htmlMsg, string $textMsg)
    {
        $email = [
            'html' => $htmlMsg,
            'text' => $textMsg,
            'subject' => 'Заявка с сайта exin.kz',
            'from' => [
                'name' => 'Exin.kz',
                'email' => 'e.kurmangalieva@exin.kz',
            ],
            'to' => [
                [
                    'email' => self::RECIPIENT,
                ],
            ]
        ];

        $r = $this->spProxy->smtpSendMail($email);
    }
}
