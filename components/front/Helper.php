<?php


namespace app\components\front;


/**
 * Class Helper
 * @package app\components\front
 */
final class Helper
{
    const KZ_MONTHS = [
        1 => 'қаңтар',
        'ақпан',
        'наурыз',
        'сәуір',
        'мамыр',
        'маусым',
        'шілде',
        'тамыз',
        'қыркүйек',
        'қазан',
        'қараша',
        'желтоқсан'
    ];

    const RU_MONTHS = [
        1 => 'январь',
        'февраль',
        'март',
        'апрель',
        'май',
        'июнь',
        'июль',
        'август',
        'сентябрь',
        'октябрь',
        'ноябрь',
        'декабрь'
    ];

    const RU_MONTHS_FORMATTED = [
          1 => 'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
    ];

    /**
     * @param string $phone
     * @return string
     */
    public static function getPrettyPhone(string $phone) : string
    {
        $result = substr_replace($phone, ' ', 2, 0);
        $result = substr_replace($result, ' ', 6, 0);
        $result = substr_replace($result, ' ', 10, 0);
        $result = substr_replace($result, ' ', 13, 0);

        return $result;
    }

    /**
     * @param string $lang
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getMonths(string $lang) : array
    {
        $formatter = \Yii::$app->formatter;
        $currentMonth = date('n');
        $currentYear = date('Y');

        $result = [];

        foreach (range($currentMonth, $currentMonth + 4) as $month) {
            $year = $month > 12 ? $currentYear + 1 : $currentYear;
            $month = $month > 12 ? $month - 11 : $month;
            $date = (new \DateTimeImmutable('1.'.$month.'.'.$year))->format('Y-m-d');
            $dateFormatted = mb_convert_case(self::KZ_MONTHS[$month], MB_CASE_TITLE, "UTF-8").' '.$year;

            if ($lang === 'ru-RU') {
                $dateFormatted = mb_convert_case(self::RU_MONTHS[$month], MB_CASE_TITLE, "UTF-8").' '.$year;
            }

            $result[$date] = $dateFormatted;
        }

        return $result;
    }

    /**
     * @param string $date
     * @param string $lang
     * @return string
     * @throws \Exception
     */
    public static function getPrettyDate(string $date, string $lang) : string
    {
        $date = new \DateTimeImmutable($date);
        $year = $date->format('Y');
        $day = $date->format('d');
        $month = $date->format('n');

        if ($lang === 'ru-RU') {
            return $day.' '.self::RU_MONTHS_FORMATTED[$month].' '.$year;
        }

        return $day.' '.self::KZ_MONTHS[$month].' '.$year;
    }

    public static function formatDates(array $dates, string $lang) : array
    {
        $result = [];
        foreach ($dates as $date) {
            $dateObj = new \DateTimeImmutable($date);
            $month = $dateObj->format('n');
            $year = $dateObj->format('Y');
            $dateFormatted = mb_convert_case(self::KZ_MONTHS[$month], MB_CASE_TITLE, "UTF-8").' '.$year;

            if ($lang === 'ru-RU') {
                $dateFormatted = mb_convert_case(self::RU_MONTHS[$month], MB_CASE_TITLE, "UTF-8").' '.$year;
            }

            $result[$date] = $dateFormatted;
        }

        return $result;
    }

    /**
     * @param int $count
     * @return string
     */
    public static function spellVacancyCount(int $count) : string
    {
        if (($count < 10 || $count > 20) && (int)substr($count, -1) === 1) {
            return 'вакансия';
        }

        if (($count < 10 || $count > 20) && in_array((int)substr($count, -1), [2, 3, 4])) {
            return 'вакансии';
        }

        return 'вакансий';
    }
}