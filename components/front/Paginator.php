<?php

namespace app\components\front;

use app\modules\exclusiveSocial\controllers\FrontController;

/**
 * Class Paginator
 */
final class Paginator
{
    const LIMIT_PAGES_COUNT = 5;

    /**
     * @var int
     */
    private $countElements;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var
     */
    private $page;

    /**
     * @var int
     */
    private $countShowed;

    /**
     * @var float|int
     */
    private $maxPage;

    /**
     * Paginator constructor.
     * @param int $countElements
     * @param int $limit
     * @param int $page
     * @param int $countShowed
     */
    public function __construct(int $countElements, int $limit, int $page, int $countShowed)
    {
        $this->countElements = $countElements;
        $this->countShowed = $countShowed;
        $this->limit = $limit;
        $this->page = $page;
        $division = (int) ($this->countElements / $this->countShowed);
        $this->maxPage = $this->countElements % $this->countShowed === 0 ? $division : $division + 1;
    }

    /**
     * @param int $page
     * @return array
     */
    public function getRange() : array
    {
        if ($this->limit < self::LIMIT_PAGES_COUNT) {
            return range(1, $this->limit);
        }

        if ($this->page === 1 || $this->page === 2) {
            return range(1, self::LIMIT_PAGES_COUNT);
        }

        if ($this->limit - $this->page < self::LIMIT_PAGES_COUNT) {
            return range($this->limit - self::LIMIT_PAGES_COUNT, $this->limit);
        }

        return range($this->page - 1, $this->page + self::LIMIT_PAGES_COUNT - 2);
    }

    /**
     * @return bool
     */
    public function hasTail() : bool
    {
        if ($this->limit - $this->page < self::LIMIT_PAGES_COUNT) {
            return false;
        }
        return $this->limit > self::LIMIT_PAGES_COUNT;
    }

    /**
     * @return float|int
     */
    public function getTail() : int
    {
        return $this->maxPage;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page) : void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPage() : int
    {
        return $this->page;
    }
}
