<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 10/23/14
 * Time: 11:27 AM
 */

namespace app\components\admin;


use dosamigos\grid\columns\ToggleColumn;


/**
 * Class RFAToggleColumn
 * @package app\components\admin
 */
class RFAToggleColumn extends ToggleColumn
{
    /**
     * @var int
     */
    public $onValue = 1;

    /**
     * @var string
     */
    public $onLabel = 'Да';

    /**
     * @var string
     */
    public $offLabel = 'Нет';

    /**
     * @var string[]
     */
    public $contentOptions = ['class' => 'col-md-1 text-center'];

    /**
     * @var string[]
     */
    public $headerOptions = ['class' => 'col-md-1'];

    /**
     * @var string
     */
    public $onIcon = 'glyphicon glyphicon-ok-circle btn btn-success';

    /**
     * @var string
     */
    public $offIcon = 'glyphicon glyphicon-remove-circle btn btn-default';

    /**
     * @var string[]
     */
    public $filter = [
        1 => 'Активный',
        0 => 'Не активный'
    ];
}
