<?php


namespace app\components\amocrm;


use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Collections\ContactsCollection;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Collections\Leads\Unsorted\FormsUnsortedCollection;
use AmoCRM\Collections\TagsCollection;
use AmoCRM\Filters\TagsFilter;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\AccountModel;
use AmoCRM\Models\CustomFieldsValues\MultiselectCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultiselectCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultiselectCustomFieldValueModel;
use AmoCRM\Models\TagModel;
use AmoCRM\Models\Unsorted\FormsMetadata;
use AmoCRM\Models\Unsorted\FormUnsortedModel;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel;


class AmoCrmService
{
    private $apiClient;

    public function __construct()
    {
        $clientId = $_ENV['AMO_CLIENT_ID'];
        $clientSecret = $_ENV['AMO_CLIENT_SECRET'];
        $redirectUri = $_ENV['AMO_REDIRECT_URI'];

        $this->apiClient = new AmoCRMApiClient($clientId, $clientSecret, $redirectUri);

        $accessToken = self::getCode();

        $this->apiClient->setAccessToken($accessToken)
            ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
            ->onAccessTokenRefresh(
                function (AccessTokenInterface $accessToken, string $baseDomain) {
                    self::saveCode(
                        [
                            'accessToken' => $accessToken->getToken(),
                            'refreshToken' => $accessToken->getRefreshToken(),
                            'expires' => $accessToken->getExpires(),
                            'baseDomain' => $baseDomain,
                        ]
                    );
                }
            );
    }
    
    public static function saveCode(array $accessToken)
    {
        $tokenFile = \Yii::getAlias('@webroot') .
            DIRECTORY_SEPARATOR . 'tmp' .
            DIRECTORY_SEPARATOR . 'token_info.json';

        if (
            isset($accessToken)
            && isset($accessToken['accessToken'])
            && isset($accessToken['refreshToken'])
            && isset($accessToken['expires'])
            && isset($accessToken['baseDomain'])
        ) {
            $data = [
                'accessToken' => $accessToken['accessToken'],
                'expires' => $accessToken['expires'],
                'refreshToken' => $accessToken['refreshToken'],
                'baseDomain' => $accessToken['baseDomain'],
            ];

            file_put_contents($tokenFile, json_encode($data));
        } else {
            exit('Invalid access token ' . var_export($accessToken, true));
        }
    }

    public static function getCode()
    {
        $tokenFile = \Yii::getAlias('@webroot') .
            DIRECTORY_SEPARATOR . 'tmp' .
            DIRECTORY_SEPARATOR . 'token_info.json';

        if (!file_exists($tokenFile)) {
            exit('Access token file not found');
        }

        $accessToken = json_decode(file_get_contents($tokenFile), true);

        if (
            isset($accessToken)
            && isset($accessToken['accessToken'])
            && isset($accessToken['refreshToken'])
            && isset($accessToken['expires'])
            && isset($accessToken['baseDomain'])
        ) {
            return new AccessToken([
                'access_token' => $accessToken['accessToken'],
                'refresh_token' => $accessToken['refreshToken'],
                'expires' => $accessToken['expires'],
                'baseDomain' => $accessToken['baseDomain'],
            ]);
        } else {
            exit('Invalid access token ' . var_export($accessToken, true));
        }
    }

    private static function printError($e)
    {
        $errorTitle = $e->getTitle();
        $code = $e->getCode();
        $debugInfo = var_export($e->getLastRequestInfo(), true);

        $error = <<<EOF
            Error: $errorTitle
            Code: $code
            Debug: $debugInfo
EOF;
        \Yii::error($error);

        echo '<pre>' . $error . '</pre>';
    }

    public function createUnsorted(string $name, string $phone, int $housingEnumId)
    {
        $ip = getHostByName(getHostName());

        // Create form for Unsorted
        $formsUnsortedCollection = new FormsUnsortedCollection();
        $formUnsorted = new FormUnsortedModel();
        $formMetadata = new FormsMetadata();
        $formMetadata
            ->setFormId('exin.kz')
            ->setFormName('Обратная связь')
            ->setFormPage('https://exin.kz')
            ->setFormSentAt(mktime(date('h'), date('i'), date('s'), 10, 04, 2019))
            ->setReferer('https://exin.kz')
            ->setIp($ip);

        // Find tag
        $tagsFilter = new TagsFilter();
        $tagsFilter->setQuery('exin.kz');
        try {
            $tagsCollection = $this->apiClient->tags(EntityTypesInterface::LEADS)->get($tagsFilter);
        } catch (AmoCRMApiException $e) {
            self::printError($e);
            die;
        }

        // Create tag if not found
        if (empty($tagsCollection)) {
            // Create if not found
            $tagsCollection = new TagsCollection();
            $tag = new TagModel();
            $tag->setName('exin.kz');
            $tagsCollection->add($tag);
            $tagsService = $this->apiClient->tags(EntityTypesInterface::LEADS);

            try {
                $tagsService->add($tagsCollection);
            } catch (AmoCRMApiException $e) {
                self::printError($e);
                die;
            }
        }

        // Create lead and attach tags to it
        $unsortedLead = new LeadModel();
        $unsortedLead->setName($name);
        $unsortedLead->setTags($tagsCollection);

        $unsortedContactsCollection = new ContactsCollection();

        $unsortedContact = new ContactModel();
        $unsortedContact->setName($name);

        try {
            $contactModel = $this->apiClient->contacts()->addOne($unsortedContact);
        } catch (AmoCRMApiException $e) {
            self::printError($e);
            die;
        }

        // Add phone info
        $phoneField = (new MultitextCustomFieldValuesModel())->setFieldCode('PHONE');
        $phoneField->setValues(
            (new MultitextCustomFieldValueCollection())
                ->add(
                    (new MultitextCustomFieldValueModel())
                        ->setEnum('WORKDD')
                        ->setValue($phone)
                )
        );

        $collection = new CustomFieldsValuesCollection();
        $collection->add($phoneField);

        // Add housing estate info if present
        if ($housingEnumId) {
            $housingField = (new MultiselectCustomFieldValuesModel())->setFieldId(536279);
            $housingField->setValues(
                (new MultiselectCustomFieldValueCollection())
                    ->add(
                        (new MultiselectCustomFieldValueModel())
                            ->setEnumId($housingEnumId)
                    )
            );
            $collection->add($housingField);
        }

        $contactModel->setCustomFieldsValues($collection);

        // Save contact
        try {
            $this->apiClient->contacts()->updateOne($contactModel);
        } catch (AmoCRMApiException $e) {
            self::printError($e);
            die;
        }

        $unsortedContactsCollection->add($contactModel);

        $formUnsorted
            ->setSourceName('exin.kz')
            ->setSourceUid('rocket_uid')
            ->setCreatedAt(time())
            ->setMetadata($formMetadata)
            ->setLead($unsortedLead)
            ->setPipelineId(2003278)
            ->setContacts($unsortedContactsCollection);

        $formsUnsortedCollection->add($formUnsorted);

        $unsortedService = $this->apiClient->unsorted();

        try {
            $formsUnsortedCollection = $unsortedService->add($formsUnsortedCollection);
        } catch (AmoCRMApiException $e) {
            self::printError($e);
            die;
        }

        return true;
    }
}
