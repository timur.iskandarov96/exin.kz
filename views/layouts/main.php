<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\modules\request\models\HousingEstate;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
            {'gtm.start': new Date().getTime(),event:'gtm.js'}

        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WLPNDGV');
    </script>
    <!-- End Google Tag Manager -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Exclusive-Qurylys</title>
    <meta name="description" content="Квартиры в ЖК 🏠 Алматы. Цены на квартиры в лучших новостройках города. Чтобы купить квартиру в жилом комплексе мечты — оставьте заявку на сайте.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= Url::to('@web/images/assets/apple-touch-icon.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= Url::to('@web/images/assets/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Url::to('@web/images/assets/favicon-16x16.png') ?>">
    <link rel="manifest" href="<?= Url::to('@web/images/assets/site.webmanifest') ?>">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/Montserrat-ExtraBold.woff2') ?>" type="font/woff2" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/Montserrat-Bold.woff2') ?>" type="font/woff2" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/Montserrat-Regular.woff2') ?>" type="font/woff2" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/Montserrat-SemiBold.woff2') ?>" type="font/woff2" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/Montserrat-Medium.woff2') ?>" type="font/woff2" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/Montserrat-Bold.ttf') ?>" type="font/ttf" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/Montserrat-Regular.ttf') ?>" type="font/ttf" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/OpenSans-SemiBold.woff') ?>" type="font/woff" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/OpenSans-Bold.woff') ?>" type="font/woff" crossorigin="crossorigin">
    <link rel="preload" as="font" href="<?= Url::to('@web/fonts/OpenSans-Regular.woff') ?>" type="font/woff" crossorigin="crossorigin">
    <style type="text/css">
        #policy-link {
            margin-top: 50px;
            text-decoration: underline;
            transition: all 0.3s ease;
        }

        #policy-link:hover {
            color: #7eb621;
        }
        .results__img a {
            width: 100%;
        }
        .results__item.item-sold > * {
            filter: grayscale(1) brightness(0.924);
        }
        .apart__bg:before {
            background: black;
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            opacity: 0.1;
        }
        span.sold-marker {
            float: right;
            font-size: .9rem;
            background: #db3333;
            padding: 5px 10px;
            border-radius: 4px;
            color: #ffff;
            font-weight: 600;
            margin: 2px 0;
        }
    </style>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function()
        {(m[i].a=m[i].a||[]).push(arguments)}

        ; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(75417226, "init",
            { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }

        );
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/75417226" style="position:absolute; left:9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script async src="<?= Url::to('@web/scripts/vendor/modernizr.min.js') ?>"></script>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WLPNDGV"
                  height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if lt IE 10]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<?= $this->render('//layouts/partials/_header'); ?>
<?= $content ?>
<footer class="footer pr-pages" id="contact">
    <?php $currentYear = (new \DateTimeImmutable())->format('Y') ?>
    <div class="copyraith">
        <div class="container">
            <div class="flex">
                <p>© <?= $currentYear ?> Exclusive Qurylys</p>
                <a href="http://rocketfirm.com" class="rocket-link" target="_blank">
                    <svg class="rocket-logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 105 14.47">
                        <title>Rocket Firm logo</title>
                        <path class="logo-letter" d="M5,11.67L3.28,8.51H1.89v3.16H0V2.78H4.17A3.44,3.44,0,0,1,5.41,3,2.6,2.6,0,0,1,6.95,4.51a3.08,3.08,0,0,1,.21,1.14,3,3,0,0,1-.17,1,2.51,2.51,0,0,1-.45.78A2.54,2.54,0,0,1,5.91,8a2.39,2.39,0,0,1-.71.29l2,3.38H5ZM3.88,6.85a1.43,1.43,0,0,0,1-.32,1.09,1.09,0,0,0,.38-0.88,1.09,1.09,0,0,0-.38-0.88,1.43,1.43,0,0,0-1-.32h-2v2.4h2Z" />
                        <path class="logo-letter" d="M13.11,11.83a5,5,0,0,1-1.85-.34A4.29,4.29,0,0,1,8.82,9.08a4.82,4.82,0,0,1-.35-1.85,4.82,4.82,0,0,1,.35-1.85A4.29,4.29,0,0,1,11.26,3,5.25,5.25,0,0,1,15,3a4.29,4.29,0,0,1,2.44,2.41,4.82,4.82,0,0,1,.35,1.85,4.82,4.82,0,0,1-.35,1.85A4.29,4.29,0,0,1,15,11.49,5,5,0,0,1,13.11,11.83Zm0-1.68a2.67,2.67,0,0,0,1.11-.23,2.6,2.6,0,0,0,.85-0.61,2.73,2.73,0,0,0,.55-0.92,3.58,3.58,0,0,0,0-2.32,2.8,2.8,0,0,0-.55-0.93,2.46,2.46,0,0,0-.85-0.61,2.93,2.93,0,0,0-2.23,0,2.39,2.39,0,0,0-.85.61,2.85,2.85,0,0,0-.54.93,3.57,3.57,0,0,0,0,2.32,2.77,2.77,0,0,0,.54.92,2.52,2.52,0,0,0,.85.61A2.67,2.67,0,0,0,13.11,10.15Z" />
                        <path class="logo-letter" d="M23.67,11.83a5.15,5.15,0,0,1-1.87-.33,4.35,4.35,0,0,1-2.5-2.39,4.66,4.66,0,0,1-.37-1.87,4.66,4.66,0,0,1,.37-1.87A4.35,4.35,0,0,1,21.8,3a5.15,5.15,0,0,1,1.87-.33A4.34,4.34,0,0,1,25,2.82a4.14,4.14,0,0,1,1,.49,3.68,3.68,0,0,1,.79.69,5.6,5.6,0,0,1,.57.81l-1.63.8a2.43,2.43,0,0,0-.84-0.92,2.21,2.21,0,0,0-1.25-.37,2.8,2.8,0,0,0-1.11.22,2.63,2.63,0,0,0-.89.61,2.79,2.79,0,0,0-.58.93,3.19,3.19,0,0,0-.21,1.16,3.23,3.23,0,0,0,.21,1.17,2.71,2.71,0,0,0,.58.93,2.68,2.68,0,0,0,.89.61,2.8,2.8,0,0,0,1.11.22,2.24,2.24,0,0,0,1.25-.37,2.36,2.36,0,0,0,.84-0.93l1.63,0.79a5.54,5.54,0,0,1-.57.81,3.87,3.87,0,0,1-.79.7,4,4,0,0,1-1,.49A4.34,4.34,0,0,1,23.67,11.83Z" />
                        <path class="logo-letter" d="M34.53,11.67L31.84,8.11l-0.69.83v2.73H29.25V2.78h1.89v4l3.15-4h2.33L33.09,7l3.77,4.72H34.53Z" />
                        <path class="logo-letter" d="M38.53,11.67V2.78h6.29V4.45h-4.4V6.33h4.3V8h-4.3v2h4.4v1.67H38.53Z" />
                        <path class="logo-letter" d="M49.21,11.67V4.45h-2.6V2.78h7.08V4.45H51.11v7.22H49.21Z" />
                        <path class="logo-letter" d="M73.56,11.67V2.78h6.29V4.45h-4.4V6.33h4.3V8h-4.3v3.68H73.56Z" />
                        <path class="logo-letter" d="M82,11.67V2.78h1.89v8.89H82Z" />
                        <path class="logo-letter" d="M91.18,11.67L89.43,8.51H88v3.16H86.15V2.78h4.17A3.44,3.44,0,0,1,91.57,3,2.6,2.6,0,0,1,93.1,4.51a3.08,3.08,0,0,1,.21,1.14,3,3,0,0,1-.17,1,2.51,2.51,0,0,1-.45.78,2.54,2.54,0,0,1-.63.53,2.39,2.39,0,0,1-.71.29l2,3.38H91.18ZM90,6.85a1.43,1.43,0,0,0,1-.32,1.09,1.09,0,0,0,.38-0.88A1.09,1.09,0,0,0,91,4.77a1.43,1.43,0,0,0-1-.32H88v2.4h2Z" />
                        <path class="logo-letter" d="M102.92,11.67V5.45l-2.51,6.22H99.59L97.08,5.45v6.22H95.19V2.78h2.65L100,8.16l2.16-5.37h2.66v8.89h-1.89Z" />
                        <path class="logo-icon" d="M70.57,7.23a7.24,7.24,0,1,0-7.24,7.24A7.23,7.23,0,0,0,70.57,7.23ZM61.75,12c-0.36.07-.32-0.1-0.21-0.25l0.15-.17c0.77-.87,1.37-1.19.37-1.19s-1.32,0-1.59-.28h0a2.62,2.62,0,0,1-.28-1.59c0-1-.32-0.4-1.2.37L58.81,9c-0.15.11-.32,0.15-0.25-0.21A5.23,5.23,0,0,1,60,6.14a12,12,0,0,1,2.38-.89c2.29-1.65,4.48-2.31,4.95-2s-0.32,2.65-2,4.95a12,12,0,0,1-.89,2.38A5.23,5.23,0,0,1,61.75,12Z" />
                        <path class="logo-icon" d="M65.08,6.78A1.24,1.24,0,0,1,63.79,8,1.25,1.25,0,1,1,65.08,6.78Z" />
                    </svg>
                    <span class="logo-year"><?= $currentYear ?></span>
                </a>
            </div>
        </div>
    </div>
</footer>
<div id="modal-question" class="modal__wrapper modal-question">
    <div class="modal">
        <article class="modal__content">
            <?= Html::button('', [ 'class' => 'modal__close', 'onclick' => "(function () { const modal = document.querySelector('#modal-question'); modal.classList.remove('active'); body.style.overflow = 'visible' })();" ]); ?>
            <?= \app\widgets\RequestForm::widget([
                'contactForm' => new \app\models\ContactForm(),
                'housingEstates' => HousingEstate::getAll()
            ]) ?>
        </article>
    </div>
</div>

<div id="success" class="modal__wrapper">
    <div class="modal">
        <button class="modal__close"></button>
        <article class="modal__content">
            <svg class="svg-icon svg-icon-success">
                <use xlink:href="images/symbol/svg/sprite.symbol.svg#success"></use>
            </svg>
            <h2><?= \Yii::t('front', 'Заявка успешно отправлена!') ?></h2>
            <span><?= \Yii::t('front', 'В ближайшее время с Вами свяжется наш специалист') ?></span>
        </article>
    </div>
</div>

<div id="error" class="modal__wrapper">
    <div class="modal">
        <button class="modal__close"></button>
        <article class="modal__content">
            <svg class="svg-icon svg-icon-error">
                <use xlink:href="<?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#error') ?>"></use>
            </svg>
            <h2><?= \Yii::t('front', 'Заявка не отправлена!') ?></h2>
            <span><?= \Yii::t('front', 'Попробуйте еще раз') ?></span>
        </article>
    </div>
</div>
<div class="up-btn" id="upBtn"></div>
<?php $this->endBody() ?>
</body>
	<script>
document.addEventListener('DOMContentLoaded', function () {
function collect_user_data(){

    const url = new URL(document.location.href)
    let user_data = new Object()

    //UTM DATA
    if (url.searchParams.get('utm_source')) {
        let name = 'utm_source'
        let data = url.searchParams.get('utm_source')
        user_data['utm_source'] = {name, data}
    }

    if (url.searchParams.get('utm_medium')) {
        let name = 'utm_medium'
        let data = url.searchParams.get('utm_medium')
        user_data['utm_medium'] = {name, data}
    }

    if (url.searchParams.get('utm_campaign')) {
        let name = 'utm_campaign'
        let data = url.searchParams.get('utm_campaign')
        user_data['utm_campaign'] = {name, data}
    }

    if (url.searchParams.get('utm_term')) {
        let name = 'utm_term'
        let data = url.searchParams.get('utm_term')
        user_data['utm_term'] = {name, data}
    }

    if (url.searchParams.get('utm_content')) {
        let name = 'utm_content'
        let data = url.searchParams.get('utm_content')
        user_data['utm_content'] = {name, data}
    }

    //UserAgent
    if (window.navigator.userAgent) {
        let name = 'userAgent'
        let data = window.navigator.userAgent
        user_data['userAgent'] = {name, data}
    }

    //Cookie
    if (get_cookie('_ga')) {
        let name = '_ga'
        let data = get_cookie('_ga').split('.')
        data = data[data.length - 2] + '.' + data[data.length - 1]
        user_data['_ga'] = {name, data}
    }

    //GetCookie Function
    function get_cookie ( cookie_name )
    {
        var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

        if ( results )
            return ( unescape ( results[2] ) );
        else
            return null;
    }

    return user_data

}

let meta = collect_user_data()
let forms = document.querySelectorAll('#contact-form-modal')

forms.forEach((form)=>{
    if (meta.utm_source) {
        let utm_source = document.createElement('input')
        utm_source.setAttribute('hidden', 'hidden')
        utm_source.setAttribute('name', 'ContactForm[utm_source]')
        utm_source.value = meta.utm_source.data
        form.appendChild(utm_source)
    }
    if (meta.utm_medium) {
        let utm_medium = document.createElement('input')
        utm_medium.setAttribute('hidden', 'hidden')
        utm_medium.setAttribute('name', 'ContactForm[utm_medium]')
        utm_medium.value = meta.utm_medium.data
        form.appendChild(utm_medium)
    }
    if (meta.utm_campaign) {
        let utm_campaign = document.createElement('input')
        utm_campaign.setAttribute('hidden', 'hidden')
        utm_campaign.setAttribute('name', 'ContactForm[utm_campaign]')
        utm_campaign.value = meta.utm_campaign.data
        form.appendChild(utm_campaign)
    }
    if (meta.utm_term) {
        let utm_term = document.createElement('input')
        utm_term.setAttribute('hidden', 'hidden')
        utm_term.setAttribute('name', 'ContactForm[utm_term]')
        utm_term.value = meta.utm_term.data
        form.appendChild(utm_term)
    }
    if (meta.utm_content) {
        let utm_content = document.createElement('input')
        utm_content.setAttribute('hidden', 'hidden')
        utm_content.setAttribute('name', 'ContactForm[utm_content]')
        utm_content.value = meta.utm_content.data
        form.appendChild(utm_content)
    }
    if (meta.userAgent) {
        let userAgent = document.createElement('input')
        userAgent.setAttribute('hidden', 'hidden')
        userAgent.setAttribute('name', 'ContactForm[user_agent]')
        userAgent.value = meta.userAgent.data
        form.appendChild(userAgent)
    }
    if (meta._ga) {
        let _ga = document.createElement('input')
        _ga.setAttribute('hidden', 'hidden')
        _ga.setAttribute('name', 'ContactForm[ga]')
        _ga.value = meta._ga.data
        form.appendChild(_ga)
    }
})
})

	</script>
<?= \yii\helpers\Html::script(isset($this->params['schema'])
    ? $this->params['schema']
    : \yii\helpers\Json::encode([
        '@context' => 'https://schema.org',
        '@type' => 'WebSite',
        'name' => 'Exclusive Qurylys',
        'image' => Url::to('@web/images/assets/favicon-16x16.png'),
        'url' => Yii::$app->homeUrl,
        'descriptions' => 'Квартиры в ЖК 🏠 Алматы. Цены на квартиры в лучших новостройках города.',
        'author' => [
            '@type' => 'Organization',
            'name' => 'Exclusive Qurylys',
            'url' => 'https://exin.kz',
            'telephone' => '+77007778878',
        ]
    ]), [
    'type' => 'application/ld+json',
]) ?>
</html>
<?php $this->endPage() ?>
