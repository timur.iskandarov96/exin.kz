<?php
/**
 * @var $content string
 * @var $title string
 */

$this->registerCss(
	<<<CSS
    .widget-panel {   
        background-color: #f5f5f5;
        padding: 10px;
        border: 1px solid #ddd
    }
    
     .widget-panel .row{
        padding:10px;
     }
CSS

)

?>
<div class="panel widget-panel">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab">
			<h4 class="panel-title">
				<?= $title; ?>
			</h4>
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-12">
					<?= $content; ?>
				</div>
			</div>
		</div>
	</div>
</div>