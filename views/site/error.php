<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Url;

$this->title = $name;
?>
<main class="js-header-size">
    <div class="not-found page">
        <div class="not-found__info">
            <div class="not-found__title">
                <p>404</p>
            </div>
            <div class="not-found__desc">
                <p class="not-found__desc-txt"><?= \Yii::t('front', 'К сожалению, страница не найдена') ?></p>
                <a class="not-found__link" href="<?= Url::to('/') ?>"><?= \Yii::t('front', 'Перейти на главную') ?></a>
            </div>
        </div>
    </div>
</main>