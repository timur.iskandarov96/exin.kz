<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin([
    'id' => 'contact-form',
    'action' => Url::to(['/contact']),
    'options' => [
        'class' => 'question--flex'
    ]
]); ?>

<?= $form->field($model, 'name', [
    'template' => '{input}',
    'options' => [
        'class' => 'input-block',
    ]
])->textInput([
    'placeholder' => 'Имя',
    'type' => 'text',
    'class' => 'input--item input--item__faq'
]) ?>

<form class="question--flex">
    <div class="input-block">
        <label for="name"><svg class="svg-icon svg-icon-name">
                <use xlink:href="images/symbol/svg/sprite.symbol.svg#name"></use>
            </svg></label>
        <input type="text" placeholder="Имя" />
    </div>
    <div class="input-block">
        <label for="tel"><svg class="svg-icon svg-icon-tel">
                <use xlink:href="images/symbol/svg/sprite.symbol.svg#tel"></use>
            </svg></label>
        <input id="phone" type="tel" required pattern="^\+\d[-]\(\d{3}\)[-]\d{3}[-]\d{2}[-]\d{2}$" placeholder="Номер телефона" />
    </div>
    <div class="select-form custom-select" id="select1">
        <div class="select-form__head">
            <label><svg class="svg-icon svg-icon-home">
                    <use xlink:href="images/symbol/svg/sprite.symbol.svg#home"></use>
                </svg></label>
            <span class="select-form__title">ЖК</span>
        </div>
        <input data-select-input type="hidden" value="0">
        <ul class="select-form__options">
            <li data-select-value="ЖК">ЖК</li>
            <li data-select-value="Алатау Сити">Алатау Сити</li>
            <li data-select-value="Exclusive Юбилейный">Exclusive Юбилейный</li>
            <li data-select-value="Exclusive Star">Exclusive Star</li>
            <li data-select-value="Exclusive Life">Exclusive Life</li>
            <li data-select-value="Azur Marmara">Azur Marmara</li>
            <li data-select-value="Панорама">Панорама</li>
            <li data-select-value="Exclusive Residence">Exclusive Residence</li>
            <li data-select-value="Project на Абая">Project на Абая</li>
            <li data-select-value="Project на Панфилова">Project на Панфилова</li>
            <li data-select-value="Exclusive Time">Exclusive Time</li>
            <li data-select-value="Altyn City">Altyn City</li>
            <li data-select-value="Tamarix City">Tamarix City</li>
        </ul>
    </div>
    <div class="input-block">
        <label class="btn" for="btn"><svg class="svg-icon svg-icon-tel2">
                <use xlink:href="images/symbol/svg/sprite.symbol.svg#tel2"></use>
            </svg></label>
        <input id="btn" class="btn" type="submit" value="Заказать звонок"></div>
</form>