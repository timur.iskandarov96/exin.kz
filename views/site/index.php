<?php

/* @var $this yii\web\View */
/* @var $contactForm app\models\ContactForm */
/* @var $housingEstates array */
/* @var $stats array */
/* @var $banner \app\modules\promoBanner\models\PromoBanner */
/* @var $news \app\modules\news\models\News */
/* @var $districtsCollection array */
/* @var $slides \app\modules\slider\models\Slider[] */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

\app\assets\MainAsset::register($this);

$this->registerCss(
<<<CSS
    #he-error {
        display: none;
    }
    
    .new-item__img img {
        position: relative;
        width: 100%;
    }
    
    .new-item__info {
        color: #333;
        padding: 2rem;
    }
    
    .main-news__container .swiper-slide a {
        background: white;
        display: block;
        position: relative;
        border-radius: 6px;
        overflow: hidden;
        border: 1px solid rgb(239 239 239 / 75%);
        box-shadow: 0 0 2rem rgb(0 0 0 / 3%) inset;
    }
    
    section.main-news {
        margin-bottom: 7rem;
    }
    
    .new-item__info .news__time {
        margin-bottom: 1rem;
        font-size: 1.2rem;
        color: #868686;
    }
    
    .new-item__info .news__info-title {
        font-size: 1.4rem;
        line-height: 1.3;
        cursor: pointer
    }
    
    .new-item__info .news__info-title:hover {
        color: #7eb621;
    }
    
    .new-item__img {
        position: relative;
        overflow: hidden;
    }
    
    .news__title-block {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-bottom: 3rem;
    }
    
    .news__controls {
        display: inline-table;
    }
    
    .news__title-block h3 {
        margin: 0;
        font-family: MontserratBold,Arial,sans-serif;
        font-size: 50px;
    }
    
    .news__btn.swiper-button-disabled {
        filter: url(data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0 0 0 1 0" /></filter></svg>#filter);
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
        opacity: .2;
    }
    
    .news__btn {
        display: inline-block;
        width: 14.85px;
        height: 26.87px;
        cursor: pointer;
        background-position: 50%;
        background-repeat: no-repeat;
    }
    
    .news__btn--prev {
        background-image: url(../images/icons/arrow-left.svg);
    }
    
    
    .news__btn--next {
        background-image: url(../images/icons/arrow-right.svg);
        margin-left: 40px;
    }

    @media (max-width: 525px) {
        .news__title-block {
            margin-left: 25px;
            margin-right: 25px;
        }
    
        .main-news__container {
            margin-left: 25px;
            margin-right: 25px;
        }
    }
CSS

);

?>

<main class="is-main-page">
    <?= \app\widgets\Slider::widget([
        'districtsCollection' => $districtsCollection,
        'slides' => $slides
    ])?>
    <section class="second-block pr-pages wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.3s">
        <div class="container">
            <?php if ($banner): ?>
                <?php if (\Yii::$app->language === 'ru-RU'): ?>
				<a href="#">
                    <picture class="second-block--top">
                        <source media="(max-width: 560px)" srcset="<?= $banner->getImagePath('img_vertical_ru') ?>">
                        <source media="(max-width: 1000px)" srcset="<?= $banner->getImagePath('img_md_ru') ?>">
                        <img src="<?= $banner->getImagePath('image') ?>" alt="<?= \Yii::t('front', 'Акционный баннер') ?>" />
                    </picture>
				</a>
                <?php else: ?>
				<a href="#">
                    <picture class="second-block--top">
                        <source media="(max-width: 560px)" srcset="<?= $banner->getImagePath('img_vertical_kz') ?>">
                        <source media="(max-width: 1000px)" srcset="<?= $banner->getImagePath('img_md_kz') ?>">
                        <img src="<?= $banner->getImagePath('image_kz') ?>" alt="<?= \Yii::t('front', 'Акционный баннер') ?>" />
                    </picture>
				</a>
                <?php endif; ?>
            <?php endif; ?>
            <div class="second-block--center">
                <h2><?= \Yii::t('front', 'Девелопер с уникальной стратегией') ?></h2>
                <span><?= \Yii::t('front', 'Работаем с экслюзивными объектами, в том числе знаковыми для города'.'.') ?></span>
            </div>
            <div class="second-block--footer">

                <?php foreach ($stats as $s): ?>

                    <div class="content">
                        <?php $iconPath = "background-image: url('".$s->getLogoPath()."')" ?>
                        <div class="content-icon" style="<?= $iconPath ?>"></div>
                        <div>
                            <h3><?= $s->translation->title ?></h3>
                            <p><?= $s->translation->text ?></p>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section class="main-news wow fadeInUp pr-pages" data-wow-duration="1.3s" data-wow-delay="0.3s">
        <div class="container">
            <div class="news__title-block">
                <h3>Новости</h3>
                <div class="news__controls">
                    <div class="news__btn news__btn--prev"></div>
                    <div class="news__btn news__btn--next"></div>
                </div>
            </div>
            <div class="swiper-container main-news__container">
                <div class="swiper-wrapper">
                    <?php if ($news): ?>
                        <?php foreach ($news as $new): ?>
                            <div class="swiper-slide new-item col-4">
                                <a href="<?= Url::to('/news/'.$new->translation->slug) ?>">
                                    <?php if($new->getUploadUrl('image', 'thumb_main')): ?>
                                        <?php $imageUrl = $new->getUploadUrl('image') ?>
                                    <?php else: ?>
                                        <?php $imageUrl = Url::to('@web/images/building_thumb.jpg') ?>
                                    <?php endif; ?>

                                    <div class="new-item__img">
                                        <img src="<?= $imageUrl ?>"
                                             alt="<?= $article->translation->image_description ?? $new->translation->title ?>"
                                        />
                                    </div>

                                    <div class="new-item__info">
                                        <p class="news__time"><?= $new->getPrettyDate() ?></p>
                                        <h4 class="news__info-title"><?= $new->translation->title ?></h4>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <?= \app\widgets\Projects::widget()?>

    <section class="question pr-pages" id="question">
        <div class="container wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s">
            <h4><?= \Yii::t('front', 'Есть вопросы?') ?></h4>
            <p><?= \Yii::t('front', 'Мы перезвоним вам в ближайшее время, чтобы ответить на все интересующие вас вопросы.') ?></p>
            <?php $form = ActiveForm::begin([
                'id' => 'contact-form',
                'options' => ['class' => 'question--flex'],
                'method' => 'POST',
                'action' => ['site/save-request']
            ]) ?>
                <?php $svgPath = Url::to('@web/images/symbol/svg/sprite.symbol.svg#name'); ?>
                <?= $form->field($contactForm, 'name', [
                    'template' => '{label}{input}{error}',
                    'options' => [
                        'class' => 'input-block',
                    ]
                ])->textInput([
                        'required' => true,
                        'placeholder' => \Yii::t('front', 'Имя'),
                        'type' => 'text',
                        'class' => 'input--item input--item__faq'
                ])->label("<svg class='svg-icon svg-icon-name'>
                    <use xlink:href='$svgPath'></use>
                </svg>") ?>
                <?php $svgPath = Url::to('@web/images/symbol/svg/sprite.symbol.svg#tel'); ?>
                <?= $form->field($contactForm, 'phone', [
                    'template' => '{label}{input}{error}',
                    'options' => [
                        'class' => 'input-block',
                    ]
                ])->textInput([
                    'required' => true,
                    'placeholder' => \Yii::t('front', 'Номер телефона'),
                    'id' => 'phone',
                    'inputmode' => 'tel',
                    'type' => 'tel',
                    'class' => 'input--item input--item__faq'
                ])->label("<svg class='svg-icon svg-icon-tel'>
                      <use xlink:href='$svgPath'></use>
                    </svg>") ?>

                <div class="select-form custom-select" id="select1">
                    <div class="select-form__head">
                        <label><svg class="svg-icon svg-icon-home">
                                <use xlink:href="<?=Url::to('@web/images/symbol/svg/sprite.symbol.svg#home');?>"></use>
                            </svg></label>
                        <span
                                class="select-form__title"
                                data-placeholder="<?= \Yii::t('front', 'ЖК') ?>"
                        >
                            <?= \Yii::t('front', 'ЖК') ?>
                        </span>
                    </div>

                    <?= Html::activeHiddenInput(
                        $contactForm,
                        'housingEstateId',
                        ['data-select-input' => '', 'value' => null] ) ?>

                    <ul class="select-form__options">
                        <?php foreach ($housingEstates as $id => $name): ?>
                            <li data-select-value="<?= $id ?>"><?= $name ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="input-block question-btn">
                    <label class="label-btn" for="btn"><svg class="svg-icon svg-icon-tel2">
                            <use xlink:href="<?=Url::to('@web/images/symbol/svg/sprite.symbol.svg#tel2');?>"></use>
                        </svg></label>
                    <?= Html::submitInput(\Yii::t('front', 'Заказать звонок'), [
                        'value' => \Yii::t('front', 'Заказать звонок'),
                        'id' => 'btn',
                        'class' => 'btn'
                    ] ) ?>
                </div>

            <?= \app\models\ContactForm::getFormToken() ?>

            <?php ActiveForm::end() ?>
        </div>
    </section>

    <?= \app\widgets\Partners::widget() ?>
    <?= \app\widgets\Contacts::widget() ?>
</main>

<?php
$js = <<<JS

var form = $('#contact-form');

form.on('beforeSubmit', function(e) {
    var submitBtn = form.find('#btn');
    submitBtn.prop('disabled', true);
    setTimeout(function() {submitBtn.prop('disabled', false); }, 3000);
    
    var data = form.serialize();
    
    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: data,
        success: function (data) {
            form[0].reset();
            $('.select-form__title').html('ЖК');
            modalManager('#success').open();
        },
        error: function(jqXHR, errMsg) {
            modalManager('#error').open();    
        }
     });
     return false; // prevent default submit
});   

let newsBlock = document.querySelector('.main-news')

if (newsBlock != null) {
    let newsImgBlocks = document.querySelectorAll('.main-news .new-item__img')
    newsImgBlocks.forEach((block)=>{
        block.style.height = block.offsetWidth + 'px'
    })
}

JS;

$this->registerJs($js);

$this->registerCss('.help-block {color: #FF4C4C}')
?>




