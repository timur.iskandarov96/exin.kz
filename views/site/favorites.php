<?php

use yii\helpers\Url; ?>

<main class="js-header-size">
    <div class="favorites page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => 'Главная', 'url' => Url::home()],
            ['title' => 'Избранное', 'url' => false]
        ]]) ?>
        <div class="container-main">
            <h1><?= \Yii::t('front', 'Избранное') ?></h1>
            <div class="results__controls">
                <div class="results__filter">
                    <p class="results__count">Всего: <span>23</span></p>
                </div>
                <div class="pagination">
                    <span class="pagination__title">Страницы:</span>
                    <ul class="pagination__list">
                        <li class="pagination__item active">
                            <a href="#">1</a>
                        </li>
                        <li class="pagination__item">
                            <a href="#">2</a>
                        </li>
                        <li class="pagination__item">
                            <a href="#">3</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="results__list">
                <div class="results__item">
                    <p class="results__item-title">Аспан Сити</p>
                    <div class="results__img">
                        <img src="images/apartament.png" alt="" />
                    </div>
                    <div class="results__info">
                        <div class="results__info-col">
                            <div class="results__subinfo">
                                <div class="results__info-block">
                                    <p class="results__info-title">Этаж</p>
                                    <p class="results__info-txt">2</p>
                                </div>
                                <div class="results__info-block">
                                    <p class="results__info-title">Комнат</p>
                                    <p class="results__info-txt">1</p>
                                </div>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Отделка</p>
                                <p class="results__info-txt">без отделки</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Цена за м<span class="sup">2</span>, тг</p>
                                <p class="results__info-txt">123 000</p>
                            </div>
                        </div>
                        <div class="results__info-col">
                            <div class="results__info-block">
                                <p class="results__info-title">Площадь, м<span class="sup">2</span></p>
                                <p class="results__info-txt">24,5</p>
                            </div class="results__info-block">
                            <div class="results__info-block">
                                <p class="results__info-title">Статус строит-ва</p>
                                <p class="results__info-txt">сдан</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Общая стоимость, тг</p>
                                <p class="results__info-txt">12 345 678</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="results__item">
                    <p class="results__item-title">Аспан Сити</p>
                    <div class="results__img">
                        <img src="images/apartament.png" alt="" />
                    </div>
                    <div class="results__info">
                        <div class="results__info-col">
                            <div class="results__subinfo">
                                <div class="results__info-block">
                                    <p class="results__info-title">Этаж</p>
                                    <p class="results__info-txt">2</p>
                                </div>
                                <div class="results__info-block">
                                    <p class="results__info-title">Комнат</p>
                                    <p class="results__info-txt">1</p>
                                </div>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Отделка</p>
                                <p class="results__info-txt">без отделки</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Цена за м<span class="sup">2</span>, тг</p>
                                <p class="results__info-txt">123 000</p>
                            </div>
                        </div>
                        <div class="results__info-col">
                            <div class="results__info-block">
                                <p class="results__info-title">Площадь, м<span class="sup">2</span></p>
                                <p class="results__info-txt">24,5</p>
                            </div class="results__info-block">
                            <div class="results__info-block">
                                <p class="results__info-title">Статус строит-ва</p>
                                <p class="results__info-txt">сдан</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Общая стоимость, тг</p>
                                <p class="results__info-txt">12 345 678</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="results__item">
                    <p class="results__item-title">Аспан Сити</p>
                    <div class="results__img">
                        <img src="images/apartament.png" alt="" />
                    </div>
                    <div class="results__info">
                        <div class="results__info-col">
                            <div class="results__subinfo">
                                <div class="results__info-block">
                                    <p class="results__info-title">Этаж</p>
                                    <p class="results__info-txt">2</p>
                                </div>
                                <div class="results__info-block">
                                    <p class="results__info-title">Комнат</p>
                                    <p class="results__info-txt">1</p>
                                </div>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Отделка</p>
                                <p class="results__info-txt">без отделки</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Цена за м<span class="sup">2</span>, тг</p>
                                <p class="results__info-txt">123 000</p>
                            </div>
                        </div>
                        <div class="results__info-col">
                            <div class="results__info-block">
                                <p class="results__info-title">Площадь, м<span class="sup">2</span></p>
                                <p class="results__info-txt">24,5</p>
                            </div class="results__info-block">
                            <div class="results__info-block">
                                <p class="results__info-title">Статус строит-ва</p>
                                <p class="results__info-txt">сдан</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Общая стоимость, тг</p>
                                <p class="results__info-txt">12 345 678</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="results__item">
                    <p class="results__item-title">Аспан Сити</p>
                    <div class="results__img">
                        <img src="images/apartament.png" alt="" />
                    </div>
                    <div class="results__info">
                        <div class="results__info-col">
                            <div class="results__subinfo">
                                <div class="results__info-block">
                                    <p class="results__info-title">Этаж</p>
                                    <p class="results__info-txt">2</p>
                                </div>
                                <div class="results__info-block">
                                    <p class="results__info-title">Комнат</p>
                                    <p class="results__info-txt">1</p>
                                </div>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Отделка</p>
                                <p class="results__info-txt">без отделки</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Цена за м<span class="sup">2</span>, тг</p>
                                <p class="results__info-txt">123 000</p>
                            </div>
                        </div>
                        <div class="results__info-col">
                            <div class="results__info-block">
                                <p class="results__info-title">Площадь, м<span class="sup">2</span></p>
                                <p class="results__info-txt">24,5</p>
                            </div class="results__info-block">
                            <div class="results__info-block">
                                <p class="results__info-title">Статус строит-ва</p>
                                <p class="results__info-txt">сдан</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Общая стоимость, тг</p>
                                <p class="results__info-txt">12 345 678</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="results__item">
                    <p class="results__item-title">Аспан Сити</p>
                    <div class="results__img">
                        <img src="images/apartament.png" alt="" />
                    </div>
                    <div class="results__info">
                        <div class="results__info-col">
                            <div class="results__subinfo">
                                <div class="results__info-block">
                                    <p class="results__info-title">Этаж</p>
                                    <p class="results__info-txt">2</p>
                                </div>
                                <div class="results__info-block">
                                    <p class="results__info-title">Комнат</p>
                                    <p class="results__info-txt">1</p>
                                </div>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Отделка</p>
                                <p class="results__info-txt">без отделки</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Цена за м<span class="sup">2</span>, тг</p>
                                <p class="results__info-txt">123 000</p>
                            </div>
                        </div>
                        <div class="results__info-col">
                            <div class="results__info-block">
                                <p class="results__info-title">Площадь, м<span class="sup">2</span></p>
                                <p class="results__info-txt">24,5</p>
                            </div class="results__info-block">
                            <div class="results__info-block">
                                <p class="results__info-title">Статус строит-ва</p>
                                <p class="results__info-txt">сдан</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Общая стоимость, тг</p>
                                <p class="results__info-txt">12 345 678</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="results__item">
                    <p class="results__item-title">Аспан Сити</p>
                    <div class="results__img">
                        <img src="images/apartament.png" alt="" />
                    </div>
                    <div class="results__info">
                        <div class="results__info-col">
                            <div class="results__subinfo">
                                <div class="results__info-block">
                                    <p class="results__info-title">Этаж</p>
                                    <p class="results__info-txt">2</p>
                                </div>
                                <div class="results__info-block">
                                    <p class="results__info-title">Комнат</p>
                                    <p class="results__info-txt">1</p>
                                </div>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Отделка</p>
                                <p class="results__info-txt">без отделки</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Цена за м<span class="sup">2</span>, тг</p>
                                <p class="results__info-txt">123 000</p>
                            </div>
                        </div>
                        <div class="results__info-col">
                            <div class="results__info-block">
                                <p class="results__info-title">Площадь, м<span class="sup">2</span></p>
                                <p class="results__info-txt">24,5</p>
                            </div class="results__info-block">
                            <div class="results__info-block">
                                <p class="results__info-title">Статус строит-ва</p>
                                <p class="results__info-txt">сдан</p>
                            </div>
                            <div class="results__info-block">
                                <p class="results__info-title">Общая стоимость, тг</p>
                                <p class="results__info-txt">12 345 678</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-main">
            <div class="social__pagination">
                <div class="pagination">
                    <span class="pagination__title">Страницы:</span>
                    <ul class="pagination__list">
                        <li class="pagination__item active">
                            <a href="#">1</a>
                        </li>
                        <li class="pagination__item">
                            <a href="#">2</a>
                        </li>
                        <li class="pagination__item">
                            <a href="#">3</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</main>
