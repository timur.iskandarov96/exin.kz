<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Url;

\app\assets\ContactsAsset::register($this);

// <script async defer src="https://api-maps.yandex.ru/2.1/?apikey=5e688e25-b597-4731-b0cf-064d2769a3c8&lang=ru_RU" type="text/javascript"></script>
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?apikey=5e688e25-b597-4731-b0cf-064d2769a3c8&lang=ru_RU', [
        'position' => $this::POS_END,
]);

?>
<main class="js-header-size">
    <div class="contacts-page page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Контакты'), 'url' => false]
        ]]) ?>
        <?= \app\widgets\Contacts::widget() ?>
        <div class="contacts-page__map" id="contactsMap" data-coords="43.24968657450528,76.948295"></div>
    </div>
</main>