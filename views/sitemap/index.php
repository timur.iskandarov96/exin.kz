<?php

/* @var $urls array */
/* @var $host string */

echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <?php foreach ($urls as $url): ?>
        <url>
            <loc><?= $host . $url['loc'] ?></loc>
            <xhtml:link
                rel="alternate"
                hreflang="kk-KZ"
                href="<?= $host . '/kk' . $url['loc'] ?>"/>

            <?php if (isset($url['lastmod'])): ?>
                <lastmod><?= $url['lastmod']; ?></lastmod>
            <?php endif; ?>
            <changefreq><?= $url['changefreq']; ?></changefreq>
            <priority><?= $url['priority']; ?></priority>
        </url>
    <?php endforeach; ?>
</urlset>