<p align="center">
    <a href="https://exin.rocketfirm.net/" target="_blank">
        <img src="https://exin.rocketfirm.net/images/icons/logo.png" height="100px" alt="logo">
    </a>
    <h1 align="center">Exclusive Qurylys</h1>
    <br>
</p>
![PHP](https://img.shields.io/badge/php-7.4.13-blue)
![MySQL](https://img.shields.io/badge/mysql-5.7.31-blue)
![framework](https://img.shields.io/badge/framework-Yii2%20Bridge-orange)

Сайт строительной компании Exclusive Qurylys

## Ссылки

- Jira - https://jira.rocketfirm.com/secure/RapidBoard.jspa?projectKey=EQSITE&rapidView=106
- Тестовый - https://exin.rocketfirm.net/
- Боевой - https://exin.kz
- Git front - https://gitlab.com/rocketfirm/exclusive-qurylys-frontend
- Git back - https://gitlab.com/rocketfirm/exclusive-qurylys-backend

## Стэк Backend

- PHP 7.4.13
- MySQL 5.7.31
- Yii2 Bridge https://gitlab.com/rocket-php-lab/yii2-bridge-core

## Инструкция по разворачиванию локальной версии

##### Требования
- Valet/Vagrant
- PHP (нужной версии)
- MySQL (нужной версии)
- Composer
- Deployer

##### Шаги
- склонировать проект в папку проекта
```bash
$ git clone git@gitlab.com:rocketfirm/exclusive-qurylys-backend.git
```
- запустить установку composer
```bash
$ composer install
```
- создать БД и импортировать дамб БД
- настроить подключение к локальной БД (пункт [Настройка окружения](#настройки-окружения))
> NOTE: в репозитории нет assets, нужно в папке web создать папку assets

## CI/CD

## Настройки окружения

Файлы конфигураций для различного окружения

- `config/local.php` — Локальный сервер, если существует этот файл, то другие файлы `mode_*.php` не работают
- `config/mode_stage.php` — Тестовый сервер
- `config/mode_prod.php` — Продакшн сервер
- `config/mode_test.php` — Тестовый(локальный) сервер

Переключение окружение настраивается в `mode.php`:

> Пример `config/mode.php`:
```php
prod
```

## Команда проекта

##### Project-Manager
- Мария Носкова - [Telegram](https://telegram.me/marie_framboises)✅

##### Frontend
- Дмитрий Червинский - [Telegram](https://telegram.me/BadRhino)✅
- Арман Куаныш - [Telegram](https://telegram.me/armankuanysh)✅

##### Backend
- Жанболат Сатыбалдинов - [Telegram](https://telegram.me/jambo_kz)✅
- Катерина Ермакова - [Telegram](https://telegram.me/Katrin2494)✅

##### Designer
- Михаил Марк - [Telegram](https://telegram.me/mikhailmark)✅

## Комментарии
