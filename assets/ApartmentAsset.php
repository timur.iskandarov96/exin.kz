<?php


namespace app\assets;

use yii\web\AssetBundle;

class ApartmentAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $depends = [
        'app\assets\AppAsset',
    ];

    public $css = [
    ];

    public $js = [
        'scripts/apartament.bundle.js',
    ];
}
