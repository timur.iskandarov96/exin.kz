<?php


namespace app\assets;

use yii\web\AssetBundle;

class AdvanceAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $depends = [
        'app\assets\AppAsset',
    ];

    public $css = [
    ];

    public $js = [
        'scripts/advance.bundle.js',
    ];
}
