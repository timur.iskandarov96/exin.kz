<?php


namespace app\assets;

use yii\web\AssetBundle;

class SearchResultAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $depends = [
        'app\assets\AppAsset',
    ];

    public $css = [
    ];

    public $js = [
        'scripts/searchResult.bundle.js',
    ];
}
