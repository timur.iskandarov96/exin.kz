<?php

namespace app\models;

/**
 * Override user logic here
 *
 * @package app\models
 */
class User extends \Da\User\Model\User
{
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }
}