<?php

namespace app\models;

use app\modules\request\models\Request;
use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;


/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
    public $housingEstateId;
    public $utm_source;
    public $utm_medium;
    public $utm_campaign;
    public $utm_term;
    public $utm_content;
    public $user_agent;
    public $ga;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                ['name', 'phone'],
                'required',
                'message' => \Yii::t('front', 'Необходимо заполнить это поле')
            ],
            [
                'phone',
                'match',
                'pattern' => '/^(\+7)+\(+(\d{3})+\)-+(\d{3})+-+(\d{2})+-+(\d{2})$/',
                'message' => \Yii::t('front', 'Не корректный номер телефона'),
            ],
            [
                'housingEstateId',
                'integer',
                'min' => 1,
            ],
            [['utm_source','utm_medium','utm_campaign','utm_term','utm_content','user_agent','ga'], 'default', 'value' => 'no'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * @param Request $model
     * @return bool
     * @throws BadRequestHttpException
     */
    public function save(Request $model) : bool
    {
        if (!$this->validate()) {
            throw new BadRequestHttpException();
        }

        $model->setName($this->name);
        $model->setPhone($this->phone);
        $model->setSource($this->utm_source);
        $model->setMedium($this->utm_medium);
        $model->setCampaign($this->utm_campaign);
        $model->setTerm($this->utm_term);
        $model->setContent($this->utm_content);
        $model->setAgent($this->user_agent);
        $model->setGa($this->ga);

        if ($this->housingEstateId) {
            $model->setHousingEstateId((int)$this->housingEstateId);
        }

        return $model->save();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public static function getFormToken() : string
    {
        $token = \Yii::$app->getSecurity()->generateRandomString();
        $token = str_replace('+', '.', base64_encode($token));

        \Yii::$app->session->set('form_token_param', $token);
        return Html::hiddenInput('form_token_param', $token);
    }
}
