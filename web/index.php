<?php
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
    header('Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Access-Control-Expose-Headers: X-Pagination-Current-Page, X-Pagination-Total-Count, X-Pagination-Page-Count');
    die();
}

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, OPTIONS");
header('Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Expose-Headers: X-Pagination-Current-Page, X-Pagination-Total-Count, X-Pagination-Page-Count');

require_once(dirname(__DIR__) . '/vendor/autoload.php');

$modeFile = dirname(__DIR__) . '/config/mode.php';
$mode = 'stage';
if (file_exists($modeFile)) {
    $mode = trim(file_get_contents($modeFile));
}

$dotenv = new \Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$env = new \janisto\environment\Environment(dirname(__DIR__) . '/config', $mode);
$env->setup();
(new yii\web\Application($env->web))->run();