<?php

namespace app\modules\vacancy\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * VacancyController implements the CRUD actions for [[app\modules\vacancy\models\Vacancy]] model.
 * @see app\modules\vacancy\models\Vacancy
 */
class VacancyController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\vacancy\models\Vacancy';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\vacancy\models\search\VacancySearch';

    /**
    * @inheritdoc
    */
    public $createScenario = 'create';

    /**
    * @inheritdoc
    */
    public $updateScenario = 'update';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\vacancy\models\Vacancy',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
