<?php


namespace app\modules\vacancy\controllers;


use app\modules\vacancy\models\Vacancy;
use yii\web\Controller;


/**
 * Class FrontController
 * @package app\modules\vacancy\controllers
 */
class FrontController extends Controller
{
    /**
     * @param string $slug
     * @return string
     */
    public function actionView(string $slug)
    {
        try {
            $model = Vacancy::findBySlug($slug);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
            $model = null;
        }

        \Yii::$app->metaTags->registerModel($model);

        return $this->render('view', [
            'model' => $model,
            'vacancies' => Vacancy::find()
                ->active()
                ->where(['<>', 'id', $model ? $model->id : 0])
                ->orderBy(['id' => SORT_DESC])
                ->all()
        ]);
    }
}