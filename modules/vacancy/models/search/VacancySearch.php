<?php

namespace app\modules\vacancy\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\vacancy\models\Vacancy;

/**
 * VacancySearch represents the model behind the search form of `app\modules\vacancy\models\Vacancy`.
 */
class VacancySearch extends Vacancy{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'salary'], 'integer'],
            [['publication_date', 'responsible_person_image', 'responsible_person_phone', 'responsible_person_email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vacancy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'salary' => $this->salary,
            'publication_date' => $this->publication_date,
        ]);

        $query->andFilterWhere(['like', 'is_active', $this->is_active])
            ->andFilterWhere(['like', 'responsible_person_image', $this->responsible_person_image])
            ->andFilterWhere(['like', 'responsible_person_phone', $this->responsible_person_phone])
            ->andFilterWhere(['like', 'responsible_person_email', $this->responsible_person_email]);

        return $dataProvider;
    }
}
