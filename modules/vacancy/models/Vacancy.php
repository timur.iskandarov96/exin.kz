<?php

namespace app\modules\vacancy\models;

use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\helpers\Url;
use yii\db\Exception;

/**
 * This is the model class for table "vacancies".
 *
 * @property integer $id
 * @property integer $is_active
 * @property integer $salary
 * @property string $publication_date
 * @property string $responsible_person_image
 * @property string $responsible_person_phone
 * @property string $responsible_person_email
 *
 * @property VacancyTranslation[] $vacanciesTranslations
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vacancies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'salary'], 'integer'],
            [['publication_date'], 'safe'],
            [['responsible_person_phone', 'responsible_person_email'], 'string', 'max' => 255],
            [['responsible_person_image'], 'file', 'on' => ['create', 'update', 'default'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активность',
            'salary' => 'Заработная плата',
            'publication_date' => 'Дата публикации',
            'responsible_person_image' => 'Фото ответственного лица',
            'responsible_person_phone' => 'Телефон ответственного лица',
            'responsible_person_email' => 'E-mail ответственного лица',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacanciesTranslations()
    {
        return $this->hasMany(VacancyTranslation::class, ['vacancy_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\vacancy\models\query\VacancyQuery the active query used by this AR class.
     */
    public static function find() : \app\modules\vacancy\models\query\VacancyQuery
    {
        return new \app\modules\vacancy\models\query\VacancyQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'responsible_person_image',
                'path' => '@webroot/media/vacancies/{id}',
                'url' => '@web/media/vacancies/{id}',
                'scenarios' => ['create', 'update', 'default'],
                'thumbs' => [
                    'thumb' => [
                        'width' => 200,
                        'height' => 200,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'preview' => [
                        'width' => 50,
                        'height' => 50,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => VacancyTranslation::class,
                'translationModelRelationColumn' => 'vacancy_id'
            ],
            'metaTag' => [
                'class' => 'Bridge\Core\Behaviors\MetaTagBehavior',
                'titleColumn' => 'translation.title',
                'descriptionColumn' => 'translation.description',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getResponsiblePhoto() : string
    {
        return Url::to('@web/media/vacancies/'.$this->id.'/'.$this->responsible_person_image);
    }

    /**
     * @param string $slug
     * @return Vacancy
     * @throws Exception
     */
    public static function findBySlug(string $slug) : Vacancy
    {
        $translated = VacancyTranslation::find()->where(['slug' => $slug])->one();

        if (!$translated) {
            throw new Exception('Could not find vacancy translation');
        }

        $modelId = $translated->vacancy_id;
        $model = self::findOne($modelId);

        if (!$model) {
            throw new Exception('Could not find vacancy');
        }

        return $model;
    }

    /**
     * @return string
     */
    public function getUrl() : string
    {
        return Url::to(['/vacancy/front/view', 'slug' => $this->translation->slug]);
    }
}
