<?php

namespace app\modules\vacancy\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\vacancy\models\Vacancy]].
 *
 * @see \app\modules\vacancy\models\Vacancy
 */
class VacancyQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\vacancy\models\Vacancy[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\vacancy\models\Vacancy|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
