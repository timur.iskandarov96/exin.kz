<?php

namespace app\modules\vacancy\models;

use Yii;

/**
 * This is the model class for table "vacancies_translation".
 *
 * @property integer $id
 * @property integer $vacancy_id
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $category
 * @property string $responsible_person_full_name
 * @property string $responsible_person_job
 *
 * @property Vacancy $vacancy
 */
class VacancyTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vacancies_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vacancy_id'], 'integer'],
            [['lang'], 'string', 'max' => 255],
            [['title', 'category', 'responsible_person_full_name', 'responsible_person_job', 'slug'], 'string', 'max' => 256],
            [['description'], 'string'],
            [['vacancy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vacancy::class, 'targetAttribute' => ['vacancy_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vacancy_id' => 'Вакансия',
            'lang' => 'Язык перевода',
            'title' => 'Название вакансии',
            'description' => 'Описание',
            'category' => 'Категория',
            'responsible_person_full_name' => 'ФИО ответственного лица',
            'responsible_person_job' => 'Должность ответственного лица',
            'slug' => 'ЧПУ'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancy()
    {
        return $this->hasOne(Vacancy::class, ['id' => 'vacancy_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Bridge\Slug\BridgeSlugBehavior',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                'ensureUnique' => 1,
                'replacement' => '-',
                'lowercase' => 1,
                'immutable' => 1,
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }
}
