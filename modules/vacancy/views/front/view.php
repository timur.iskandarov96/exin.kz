<?php

use yii\helpers\Url;

/* @var $model = app\modules\vacancy\models\Vacancy */
/* @var $vacancies array */

?>
<main class="js-header-size">
    <div class="vacancy page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Вакансии'), 'url' => Url::to('/our-team')],
            ['title' => $model->translation->title, 'url' => false]
        ]]) ?>
        <div class="vacancy__inner">
            <div class="container-main">
                <h1 class="vacancy__title"><?= $model->translation->title ?></h1>
                <div class="vacancy__block">
                    <div class="vacancy__desc">
                        <div class="vacancy__txt">
                            <p class="vacancy__desc-title"><?= \Yii::t('front', 'Описание') ?></p>
                            <?= $model->translation->description ?>
                        </div>
                        <div class="vacancy__txt">
                            <p class="vacancy__desc-title"><?= \Yii::t('front', 'Категория') ?></p>
                            <p><?= $model->translation->category ?></p>
                        </div>
                        <div class="vacancy__txt">
                            <p class="vacancy__desc-title"><?= \Yii::t('front', 'Заработная плата') ?></p>
                            <p><?= $model->salary.' тг / '. \Yii::t('front', 'месяц') ?></p>
                        </div>
                        <div class="vacancy__txt">
                            <p class="article__date">
                                <?= \app\components\front\Helper::getPrettyDate($model->publication_date, \Yii::$app->language) ?>
                            </p>
                        </div>
                    </div>
                    <div class="vacancy__main">
                        <p class="vacancy__desc-title"><?= \Yii::t('front', 'Ответственное лицо') ?></p>
                        <div class="vacancy__main-img" style="background-image: url('<?= $model->getResponsiblePhoto() ?>')"></div>
                        <h4 class="vacancy__name"><?= $model->translation->responsible_person_full_name ?></h4>
                        <p class="vacancy__main-desc"><?= $model->translation->responsible_person_job ?></p>
                        <div class="vacancy__contacts">
                            <a href="tel:<?= $model->responsible_person_phone ?>" class="header__phone-block" data-link="#question">
                                <img src="<?= Url::to('@web/images/icons/tel.png') ?>" alt="tel">
                                <p class="header__phone"><?= \app\components\front\Helper::getPrettyPhone($model->responsible_person_phone) ?></p>
                            </a>
                            <a href="mailto:<?= $model->responsible_person_email ?>" class="vacancy__email"><?=$model->responsible_person_email?></a>
                        </div>
                    </div>
                </div>
                <div class="vacancies">
                    <div class="vacancies__title-block">
                        <h2><?= \Yii::t('front', 'Другие вакансии') ?></h2>
                        <p class="vacancies__count">
                            <?= \Yii::t('front', 'Всего').' '.count($vacancies) ?>
                        </p>
                    </div>
                    <div class="vacancies__list">
                        <?php foreach ($vacancies as $v): ?>

                            <a href="<?= Url::to('/vacancy/'.$v->translation->slug) ?>" class="vacancies__item">
                                <h4 class="vacancies__title"><?= \Yii::t('front', $v->translation->title) ?></h4>
                                <p class="vacancies__text"><?= \Yii::t('front', $v->translation->category) ?></p>
                            </a>

                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
