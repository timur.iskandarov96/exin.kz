<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\vacancy\models\Vacancy */

$this->title = $model->translation->title;
$this->params['breadcrumbs'][] = ['label' => 'Вакансии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'is_active',
                'value' => function($data) {
                    if ($data->is_active) {
                        return $data->is_active === 1 ? 'Да' : 'Нет';
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'salary',
                'value' => function($model) {
                    if ($model->salary) {
                        return number_format($model->salary, 0, '', ' ') . ' тг';
                    }
                }
            ],
            [
                'attribute' => 'publication_date',
                'value' => function ($data) {
                    if ($data->publication_date) {
                        return \Yii::$app->formatter->asDate($data->publication_date, 'dd.MM.yyyy');
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'responsible_person_image',
                'value' => function($model) {
                    $imagePath = $model->getResponsiblePhoto();
                    return Html::img($imagePath);
                },
                'format' => 'raw'
            ],
            'responsible_person_phone',
            'responsible_person_email:email',
        ],
    ]) ?>
    </div>
</div>