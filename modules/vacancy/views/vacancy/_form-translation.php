<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\vacancy\models\Vacancy
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']vacancy_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']title')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']description')->richTextArea() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']category')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']responsible_person_full_name')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']responsible_person_job')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']slug')->textInput() ?>
