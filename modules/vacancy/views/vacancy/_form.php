<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\vacancy\models\Vacancy */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->translate($model, '@app/modules/vacancy/views/vacancy/_form-translation') ?>

        <?= $form->field($model, 'is_active')->switchInput() ?>

        <?= $form->field($model, 'salary')->textInput() ?>

        <?= $form->field($model, 'publication_date')->datePicker([ 'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true,
        ]]) ?>

        <?= $form->metaTags($model->metaTag) ?>

    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'responsible_person_image')->imageUpload() ?>

        <?= $form->field($model, 'responsible_person_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'responsible_person_email')->textInput(['maxlength' => true]) ?>

    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
