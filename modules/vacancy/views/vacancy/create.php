<?php

/* @var $this yii\web\View */
/* @var $model app\modules\vacancy\models\Vacancy */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Вакансии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

