<?php

namespace app\modules\aboutCompany\models;

use Yii;

/**
 * This is the model class for table "about_lists".
 *
 * @property integer $id
 * @property integer $about_block_id
 * @property string $lang
 * @property string $text
 *
 * @property AboutCompany $aboutBlock
 */
class AboutList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_lists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_block_id'], 'integer'],
            [['text'], 'string'],
            [['lang'], 'string', 'max' => 255],
            [['about_block_id'], 'exist', 'skipOnError' => true, 'targetClass' => AboutCompany::class, 'targetAttribute' => ['about_block_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'about_block_id' => 'Блок страницы',
            'lang' => 'Lang',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutBlock()
    {
        return $this->hasOne(AboutCompany::className(), ['id' => 'about_block_id']);
    }
    
    /**
     * @inheritdoc
     * @return AboutListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AboutListQuery(get_called_class());
    }

}
