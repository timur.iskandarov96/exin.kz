<?php

namespace app\modules\aboutCompany\models;

use Yii;

/**
 * This is the model class for table "about_company_lists".
 *
 * @property integer $id
 * @property integer $about_company_id
 * @property string $icon
 *
 * @property AboutCompany $aboutCompany
 * @property AboutCompanyListTranslation[] $aboutCompanyListsTranslations
 */
class AboutCompanyList extends \yii\db\ActiveRecord
{
    public $text;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_company_lists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['icon'], 'file', 'on' => ['create', 'update'], 'extensions' => ['svg']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>  'ID',
            'icon' =>  'Иконка',
            'is_active' => 'Активность'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'fileUpload' =>  [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadBehavior',
                'attribute' => 'icon',
                'path' => '@webroot/media/about-company-lists/icons/{id}',
                'url' => '@web/media/about-company-lists/icons/{id}',
                'scenarios' => ['create', 'update'],
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => AboutCompanyListTranslation::class,
                'translationModelRelationColumn' => 'about_company_lists_id'
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompany()
    {
        return $this->hasOne(AboutCompany::class, ['id' => 'about_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyListTranslations()
    {
        return $this->hasMany(AboutCompanyListTranslation::class, ['about_company_lists_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\aboutCompany\models\query\AboutCompanyListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\aboutCompany\models\query\AboutCompanyListQuery(get_called_class());
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        $query = self::find()->leftJoin(
            '{{about_company_lists_translation}}',
            '{{about_company_lists}}.`id` = {{about_company_lists_translation}}.`about_company_lists_id`'
        )
            ->select(['{{about_company_lists}}.*', '{{about_company_lists_translation}}.text as text'])
            ->where([
                '{{about_company_lists_translation}}.`lang`' => \Yii::$app->language,
                '{{about_company_lists_translation}}.`about_company_lists_id`' => $this->id
            ])
            ->one();

        return $query->text;
    }

    /**
     * @return string
     */
    public function getIconPath()
    {
        return \Yii::getAlias('@web') . "/media/about-company-lists/icons/{$this->id}/$this->icon";
    }
}
