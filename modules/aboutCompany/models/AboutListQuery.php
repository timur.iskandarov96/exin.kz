<?php

namespace app\modules\aboutCompany\models;

/**
 * This is the ActiveQuery class for [[AboutList]].
 *
 * @see AboutList
 */
class AboutListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return AboutList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AboutList|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
