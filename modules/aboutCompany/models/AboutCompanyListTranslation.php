<?php

namespace app\modules\aboutCompany\models;

use Yii;

/**
 * This is the model class for table "about_company_lists_translation".
 *
 * @property integer $id
 * @property string $lang
 * @property integer $about_company_lists_id
 * @property string $text
 *
 * @property AboutCompanyLists $aboutCompanyLists
 */
class AboutCompanyListTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_company_lists_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'text'], 'required'],
            [['about_company_lists_id'], 'integer'],
            [['lang', 'text'], 'string', 'max' => 255],
            [['about_company_lists_id'], 'exist', 'skipOnError' => true, 'targetClass' => AboutCompanyList::class, 'targetAttribute' => ['about_company_lists_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>  'ID',
            'lang' =>  'Язык перевода',
            'about_company_lists_id' =>  'Список',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyLists()
    {
        return $this->hasOne(AboutCompanyLists::className(), ['id' => 'about_company_lists_id']);
    }

}
