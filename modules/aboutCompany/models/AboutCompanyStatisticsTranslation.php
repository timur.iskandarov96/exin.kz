<?php

namespace app\modules\aboutCompany\models;

use Yii;

/**
 * This is the model class for table "about_company_statistics_translation".
 *
 * @property integer $id
 * @property integer $about_company_statistics_id
 * @property string $lang
 * @property string $title
 * @property string $subtitle
 * @property string $text
 *
 * @property AboutCompanyStatistics $aboutCompanyStatistics
 */
class AboutCompanyStatisticsTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_company_statistics_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_company_statistics_id'], 'integer'],
            [['lang', 'title', 'subtitle', 'text'], 'string', 'max' => 255],
            [['about_company_statistics_id'], 'exist', 'skipOnError' => true, 'targetClass' => AboutCompanyStatistics::class, 'targetAttribute' => ['about_company_statistics_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'about_company_statistics_id' => 'Статистика',
            'lang' => 'Язык перевода',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyStatistics()
    {
        return $this->hasOne(AboutCompanyStatistics::className(), ['id' => 'about_company_statistics_id']);
    }

}
