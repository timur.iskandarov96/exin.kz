<?php

namespace app\modules\aboutCompany\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\aboutCompany\models\AboutCompany]].
 *
 * @see \app\modules\aboutCompany\models\AboutCompany
 */
class AboutCompanyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\aboutCompany\models\AboutCompany[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\aboutCompany\models\AboutCompany|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
