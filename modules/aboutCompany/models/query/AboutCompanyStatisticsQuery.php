<?php

namespace app\modules\aboutCompany\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\aboutCompany\models\AboutCompanyStatistics]].
 *
 * @see \app\modules\aboutCompany\models\AboutCompanyStatistics
 */
class AboutCompanyStatisticsQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\aboutCompany\models\AboutCompanyStatistics[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\aboutCompany\models\AboutCompanyStatistics|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
