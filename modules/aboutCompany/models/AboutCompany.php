<?php

namespace app\modules\aboutCompany\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "about_company".
 *
 * @property integer $id
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class AboutCompany extends \yii\db\ActiveRecord
{
    const ABOUT_BLOCK = 1;
    const MISSION_BLOCK = 2;
    const VALUES_BLOCK = 3;


    public $title;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>  'ID',
            'image' => 'Изображение',
            'created_at' => 'Дата создания',
            'updated_at' =>  'Дата обновления',
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\aboutCompany\models\query\AboutCompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\aboutCompany\models\query\AboutCompanyQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image',
                'path' => '@webroot/media/about_company/{id}',
                'url' => '@web/media/about_company/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => AboutCompanyTranslation::class,
                'translationModelRelationColumn' => 'about_company_id'
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        $query = self::find()->leftJoin(
            '{{about_company_translation}}',
            '{{about_company}}.`id` = {{about_company_translation}}.`about_company_id`'
        )->select(['{{about_company}}.*', '{{about_company_translation}}.title as title'])
        ->where([
            '{{about_company_translation}}.`lang`' => \Yii::$app->language,
            '{{about_company_translation}}.`about_company_id`' => $this->id
        ])
        ->one();

        return $query->title;
    }

    public function getList()
    {
        return ArrayHelper::map(
            AboutList::find()->where(['about_block_id' => $this->id])->andWhere(['lang' => Yii::$app->language])->all(),
            'id',
            'text'
        );
    }
}
