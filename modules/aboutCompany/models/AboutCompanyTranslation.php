<?php

namespace app\modules\aboutCompany\models;

use Yii;

/**
 * This is the model class for table "about_company_translation".
 *
 * @property integer $id
 * @property string $lang
 * @property integer $about_company_id
 * @property string $title
 * @property string $subtitle
 * @property string $description
 */
class AboutCompanyTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_company_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'title'], 'required'],
            [['about_company_id'], 'integer'],
            [['description'], 'string'],
            [['lang', 'title', 'subtitle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Язык перевода',
            'about_company_id' => 'Блок страницы',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'description' => 'Описание',
        ];
    }

}
