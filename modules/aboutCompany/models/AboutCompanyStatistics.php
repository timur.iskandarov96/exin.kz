<?php

namespace app\modules\aboutCompany\models;

use Yii;

/**
 * This is the model class for table "about_company_statistics".
 *
 * @property integer $id
 * @property integer $is_active
 *
 * @property AboutCompanyStatisticsTranslation[] $aboutCompanyStatisticsTranslations
 */
class AboutCompanyStatistics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_company_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активность',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => AboutCompanyStatisticsTranslation::class,
                'translationModelRelationColumn' => 'about_company_statistics_id'
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanyStatisticsTranslations()
    {
        return $this->hasMany(AboutCompanyStatisticsTranslation::className(), ['about_company_statistics_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\aboutCompany\models\query\AboutCompanyStatisticsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\aboutCompany\models\query\AboutCompanyStatisticsQuery(get_called_class());
    }
}
