<?php

namespace app\modules\aboutCompany\controllers;

use app\modules\aboutCompany\models\AboutCompany;
use app\modules\aboutCompany\models\AboutCompanyList;
use app\modules\aboutCompany\models\AboutList;
use Bridge\Core\Controllers\BaseAdminController;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * AboutCompanyController implements the CRUD actions for [[app\modules\aboutCompany\models\AboutCompany]] model.
 * @see app\modules\aboutCompany\models\AboutCompany
 */
class AboutCompanyController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\aboutCompany\models\AboutCompany';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\aboutCompany\models\search\AboutCompanySearch';

    /**
    * @inheritdoc
    */
    public $createScenario = 'create';

    /**
    * @inheritdoc
    */
    public $updateScenario = 'update';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\aboutCompany\models\AboutCompany',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
        
        unset($actions['create']);
        unset($actions['update']);

        return $actions;
    }

    public function actionCreate()
    {
        $model = new $this->modelClass;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {

            if (\Yii::$app->request->post('list-kk')) {
                foreach (\Yii::$app->request->post('list-kk') as $elem) {
                    $listModel = new AboutList();
                    $listModel->lang = 'kk-KZ';
                    $listModel->text = $elem;
                    $listModel->about_block_id = $model->id;
                    $listModel->save();
                }
            }

            if (\Yii::$app->request->post('list-ru')) {
                foreach (\Yii::$app->request->post('list-ru') as $elem) {
                    $listModel = new AboutList();
                    $listModel->lang = 'ru-RU';
                    $listModel->text = $elem;
                    $listModel->about_block_id = $model->id;
                    $listModel->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = AboutCompany::findOne($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {

            if (\Yii::$app->request->post('list-kk')) {
                \Yii::$app->db->createCommand()->delete('about_lists', [
                    'about_block_id' => $id,
                    'lang' => 'kk-KZ'
                ])->execute();

                foreach (\Yii::$app->request->post('list-kk') as $elem) {
                        $listModel = new AboutList();
                        $listModel->lang = 'kk-KZ';
                        $listModel->text = $elem;
                        $listModel->about_block_id = $model->id;
                        $listModel->save();
                }
            }

            if (\Yii::$app->request->post('list-ru')) {
                \Yii::$app->db->createCommand()->delete('about_lists', [
                    'about_block_id' => $id,
                    'lang' => 'ru-RU'
                ])->execute();

                foreach (\Yii::$app->request->post('list-ru') as $elem) {
                        $listModel = new AboutList();
                        $listModel->lang = 'ru-RU';
                        $listModel->text = $elem;
                        $listModel->about_block_id = $model->id;
                        $listModel->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetListElements()
    {
        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $modelId = \Yii::$app->request->post('id');

            $listKz = ArrayHelper::map(
                AboutList::find()->where(['about_block_id' => $modelId])->andWhere(['lang' => 'kk-KZ'])->all(),
                'id',
                'text'
            );
            $listRu = ArrayHelper::map(
                AboutList::find()->where(['about_block_id' => $modelId])->andWhere(['lang' => 'ru-RU'])->all(),
            'id',
            'text'
            );

            if (count($listKz) === 0 || count($listRu) === 0) {
                return ['result' => 'empty'];
            }

            return [
                'kz' => $listKz,
                'ru' => $listRu
            ];
        }
    }
}
