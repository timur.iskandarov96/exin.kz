<?php

namespace app\modules\aboutCompany\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * AboutCompanyListController implements the CRUD actions for [[app\modules\aboutCompany\models\AboutCompanyList]] model.
 * @see app\modules\aboutCompany\models\AboutCompanyList
 */
class AboutCompanyListController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\aboutCompany\models\AboutCompanyList';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\aboutCompany\models\search\AboutCompanyListSearch';

    /**
    * @inheritdoc
    */
    public $createScenario = 'create';

    /**
    * @inheritdoc
    */
    public $updateScenario = 'update';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\aboutCompany\models\AboutCompanyList',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
