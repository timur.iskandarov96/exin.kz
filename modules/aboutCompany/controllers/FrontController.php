<?php


namespace app\modules\aboutCompany\controllers;


use app\modules\aboutCompany\models\AboutCompany;
use app\modules\aboutCompany\models\AboutCompanyList;
use app\modules\aboutCompany\models\AboutCompanyStatistics;
use yii\web\Controller;

class FrontController extends Controller
{
    public function actionIndex()
    {
        $aboutCompanyBlocks = AboutCompany::find()->all();
        $listWithIcons = AboutCompanyList::find()->active()->all();
        $statistics = AboutCompanyStatistics::find()->active()->all();

        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => 'О компании'
            ]
        ]);

        return $this->render('index', [
            'block1' => $aboutCompanyBlocks[0],
            'block2' => $aboutCompanyBlocks[1],
            'block3' => $aboutCompanyBlocks[2],
            'block4' => $aboutCompanyBlocks[3],
            'listWithIcons' => $listWithIcons,
            'statistics' => $statistics
        ]);
    }
}