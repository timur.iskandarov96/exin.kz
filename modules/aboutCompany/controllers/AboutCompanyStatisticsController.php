<?php

namespace app\modules\aboutCompany\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * AboutCompanyStatisticsController implements the CRUD actions for [[app\modules\aboutCompany\models\AboutCompanyStatistics]] model.
 * @see app\modules\aboutCompany\models\AboutCompanyStatistics
 */
class AboutCompanyStatisticsController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\aboutCompany\models\AboutCompanyStatistics';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\aboutCompany\models\search\AboutCompanyStatisticsSearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\aboutCompany\models\AboutCompanyStatistics',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
