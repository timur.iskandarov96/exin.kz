<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\aboutCompany\models\search\AboutCompanyListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Списки';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'label' => 'Текст',
            'value' => function($model) {
                return $model->translation->text;
            }
        ],
        [
            'class' => \app\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],
        [
            'attribute' => 'icon',
            'value' => function ($model) {
                $iconPath = $model->getIconPath();
                return '<object type="image/svg+xml" data="' . $iconPath . '" width="200" height="100">
                            Your browser does not support SVG
                        </object>';
            },
            'format' => 'raw'
        ],

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
