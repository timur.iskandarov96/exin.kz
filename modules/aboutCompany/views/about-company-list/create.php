<?php

/* @var $this yii\web\View */
/* @var $model app\modules\aboutCompany\models\AboutCompanyList */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Списки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

