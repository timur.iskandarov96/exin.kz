<?php

use yii\helpers\Url;

/* @var $block1 app\modules\aboutCompany\models\AboutCompany */
/* @var $block2 app\modules\aboutCompany\models\AboutCompany */
/* @var $block3 app\modules\aboutCompany\models\AboutCompany */
/* @var $block4 app\modules\aboutCompany\models\AboutCompany */
/* @var $listWithIcons array */
/* @var $statistics array */

\app\assets\AboutAsset::register($this);

?>

<main class="js-header-size">
    <div class="about page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'О компании'), 'url' => false]
        ]]) ?>
        <div class="about__info">
            <div class="container-main">
                <h1><?= $block1->translation->title ?></h1>
                <div class="about__info-txt">
                    <?= $block1->translation->description ?>
                </div>
                <div class="about__attr-list">
                    <?php foreach ($block1->getList() as $listItem): ?>
                        <div class="about__attr-item">
                            <div class="about__attr-icon"></div>
                            <p><?= $listItem ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="mission">
            <div class="mission__mountain-img"></div>
            <div class="container-main">
                <div class="mission__block">
                    <div class="mission__txt">
                        <h2><?= $block2->translation->title ?></h2>
                        <?= $block2->translation->description ?>
                    </div>
                    <div class="mission__title">
                        <?= $block2->translation->subtitle ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="values">
            <div class="container-main">
                <h2><?= $block3->translation->title ?></h2>
                <div class="values__list">
                    <?php foreach ($block3->getList() as $listElem): ?>
                        <?php
                            $phraseArray = explode(' ', trim($listElem));
                            $firstWord = $phraseArray[0];
                            $remainingPart = implode(' ', array_splice($phraseArray, 1));
                        ?>
                        <div class="values__item">
                            <p><strong><?= $firstWord . ' ' ?></strong><?= $remainingPart ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="activity">
            <div class="activity__bg">
                <?= $block4->translation->subtitle ?>
            </div>
            <div class="container-main">
                <div class="activity__wrap">
                    <div class="activity__info">
                        <h2 class="activity__title"><?= $block4->translation->title ?></h2>
                        <div class="activity__txt"><?= $block4->translation->description ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="activity-more">
            <div class="container-main">
                <div class="activity-more__list">
                    <?php $partialArr = array_chunk(array_keys($listWithIcons), 3) ?>
                    <?php foreach ($listWithIcons as $key => $listElem): ?>
                        <?php foreach ($partialArr as $index => $elem): ?>
                            <?php if (in_array($key, $elem)):
                                $count = $index + 1;
                            ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php $cssUrlIconPath = "background-image: url('" . $listElem->getIconPath()  . "')" ?>
                        <div class="activity-more__item" data-group="<?= $count ?>">
                            <div class="activity-more__icon" style="<?= $cssUrlIconPath ?>"></div>
                            <p class="activity-more__txt"><b><?= $listElem->translation->text ?></b></p>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="activity-more__controls">
                    <button class="activity-more__btn active" data-group="1">01</button>
                    <button class="activity-more__btn" data-group="2">02</button>
                    <button class="activity-more__btn" data-group="3">03</button>
                </div>
            </div>
        </div>
        <div class="progress">
            <div class="container-main">
                <h3 class="progress__title"><?= \Yii::t('front', 'Группа компаний Exclusive Qurylys на рынке с 2007 года') ?></h3>
                <div class="progress__list">
                    <?php foreach ($statistics as $info): ?>
                        <div class="progress__item">
                            <h2 class="progress__item-title"><?= $info->translation->title ?></h2>
                            <p class="progress__txt"><?= $info->translation->text ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</main>