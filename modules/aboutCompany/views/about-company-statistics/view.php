<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\aboutCompany\models\AboutCompanyStatistics */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Статистика', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Заголовок',
                'value' => function($model) {
                    return $model->translation->title;
                }
            ],
            [
                'label' => 'Текст',
                'value' => function($model) {
                    return $model->translation->text;
                }
            ],
            [
                'attribute' => 'is_active',
                'value' => function($model) {
                    return $model->is_active ? 'Да' : 'Нет';
                }
            ]
        ],
    ]) ?>
    </div>
</div>