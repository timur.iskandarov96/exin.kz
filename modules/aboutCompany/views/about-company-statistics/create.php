<?php

/* @var $this yii\web\View */
/* @var $model app\modules\aboutCompany\models\AboutCompanyStatistics */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Статистика', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

