<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\aboutCompany\models\AboutCompany
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */
use dosamigos\ckeditor\CKEditorWidgetAsset;

$this->registerJsFile("@web/js/ckeditor/styles.js", ['depends' => CKEditorWidgetAsset::class]);
$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']about_company_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']title')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']subtitle')->richTextArea() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']description')->richTextArea() ?>
