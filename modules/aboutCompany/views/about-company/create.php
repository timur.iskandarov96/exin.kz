<?php

/* @var $this yii\web\View */
/* @var $model app\modules\aboutCompany\models\AboutCompany */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Блоки страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model
]) ?>

