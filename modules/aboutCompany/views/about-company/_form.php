<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\aboutCompany\models\AboutCompany */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'aboutCompany']); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->translate($model, '@app/modules/aboutCompany/views/about-company/_form-translation') ?>

            <?= $form->field($model, 'image')->imageUpload() ?>
        </div>

        <div id="list-block" class="col-md-6">
            <?= Html::button('Добавить элемент списка', ['id' => 'add-item', 'class' => 'btn btn-info', 'style' => ['display' => 'block']]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ?
            \Yii::t('bridge', 'Create') :
            \Yii::t('bridge', 'Update'), [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

<?php ActiveForm::end() ?>

<?php
$lang = \Yii::$app->language;
$modelId = $model->id;

$js = <<<JS

$(document).ready(function() {
    $.ajax({
        type : 'post',
        url : 'get-list-elements',
        data : {id: "$modelId"}
    }).done(function(data) {
        if (data.error == null) {
            if (data.result !== 'empty') {
              
                for (let i = 0; i < Object.values(data.ru).length; i++) {
                    let listBlock = `<div class="list-block">
                                         <div class="col-md-10">
                                            <label class="control-label">Текст</label>
                                            <input type="text" class="form-control" name="list-kk[]" value="` + Object.values(data.kz)[i] + `">
                                            <input type="text" class="form-control" name="list-ru[]" value="` + Object.values(data.ru)[i] + `">
                                         </div>
                                         <div class="col-md-2">
                                            <button type="button" class="btn btn-danger btn-remove-list" style="margin-top: 24px">X</button>
                                         </div>
                                     </div>`;
                    $(listBlock).clone().appendTo('#list-block');
                }
                               
                let lang = $("li[role='presentation'].active")[0].innerText;
    
                if (lang === 'RU') {
                    $('input[name="list-kk[]"]').css('display', 'none');
                } else {
                    $('input[name="list-ru[]"]').css('display', 'none');
                }
            }
        } else {
            console.log("ERROR");
        }
    })
})

let listBlock = `<div class="list-block">
                     <div class="col-md-10">
                        <label class="control-label">Текст</label>
                        <input type="text" class="form-control" name="list-kk[]">
                        <input type="text" class="form-control" name="list-ru[]">
                     </div>
                     <div class="col-md-2">
                        <button type="button" class="btn btn-danger btn-remove-list" style="margin-top: 24px">X</button>
                     </div>
                 </div>`;

$('#add-item').click(function () {
    $(listBlock).clone().appendTo('#list-block');
    
    let lang = $("li[role='presentation'].active")[0].innerText;
    
    if (lang === 'RU') {
        $('input[name="list-kk[]"]').css('display', 'none');
    } else {
        $('input[name="list-ru[]"]').css('display', 'none');
    }
});

$('a[href$="translate-about_company-ru-RU"]').click(function() {
    $('input[name="list-ru[]"]').css('display', 'block');
    $('input[name="list-kk[]"]').css('display', 'none');
})

$('a[href$="translate-about_company-kk-KZ"]').click(function() {
    $('input[name="list-kk[]"]').css('display', 'block');
    $('input[name="list-ru[]"]').css('display', 'none');
})

$("body").on("click", ".btn-remove-list", function(e) {
	let grandpa = e.target.parentNode.parentNode;
	$(grandpa).remove();
});

JS;

$this->registerJs($js);

?>


