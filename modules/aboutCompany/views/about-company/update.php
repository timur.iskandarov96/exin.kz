<?php

/* @var $this yii\web\View */
/* @var $model app\modules\aboutCompany\models\AboutCompany */

$this->title = 'Редактировать запись: ' . $model->translation->title;
$this->params['breadcrumbs'][] = ['label' => 'Блоки страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->translation->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


