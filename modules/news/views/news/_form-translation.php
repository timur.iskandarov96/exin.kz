<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\news\models\News
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']news_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']title')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']description')->richTextArea(['options' => ['preset' => 'full', 'rows' => 6]]) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']slug')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']image_description')->textInput() ?>
