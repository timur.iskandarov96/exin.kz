<?php

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

