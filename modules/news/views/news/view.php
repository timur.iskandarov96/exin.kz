<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;
use yii\helpers\Html as YiiHtml;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */

$this->title = $model->translation->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'image',
                'value' => function ($model) {
                    return YiiHtml::img($model->getUploadUrl('image'), [
                            'width' => 480
                    ]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'publication_date',
                'value' => function ($data) {
                    if ($data->publication_date) {
                        return \Yii::$app->formatter->asDate($data->publication_date, 'dd.MM.yyyy');
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'is_active',
                'value' => function($data) {
                    if ($data->is_active) {
                        return $data->is_active === 1 ? 'Да' : 'Нет';
                    }
                    return null;
                }
            ],
            'position',
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    if ($data->created_at) {
                        return \Yii::$app->formatter->asDate($data->created_at, 'dd.MM.yyyy H:mm:ss');
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($data) {
                    if ($data->updated_at) {
                        return \Yii::$app->formatter->asDate($data->updated_at, 'dd.MM.yyyy H:mm:ss');
                    }
                    return null;
                }
            ],
        ],
    ]) ?>
    </div>
</div>