<?php

use yii\helpers\Url;

/* @var $articles \app\modules\news\models\News[] */
/* @var $sortLimits array */
/* @var $currentLimit integer */
/* @var $currentPage integer */
/* @var $paginator app\components\front\Paginator */

?>

<main class="js-header-size">
    <div class="news page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Наши новости'), 'url' => false]
        ]]) ?>
        <div class="news__inner">
            <div class="container-main">
                <h1 class="news__title"><?=\Yii::t('front', 'Наши новости') ?></h1>
                <div class="news__list">
                    <?php foreach ($articles as $article): ?>
                        <a href="<?= Url::to('/news/'.$article->translation->slug) ?>" class="news__item">
                            <?php if($article->getThumbUploadUrl('image', 'thumb_main')): ?>
                                <?php $imageUrl = $article->getThumbUploadUrl('image', 'thumb_main') ?>
                            <?php else: ?>
                                <?php $imageUrl = Url::to('@web/images/building_thumb.jpg') ?>
                            <?php endif; ?>

                            <div class="news__img">
                                <img src="<?= $imageUrl ?>"
                                     alt="<?= $article->translation->image_description ?? $article->translation->title ?>"
                                />
                            </div>

                            <div class="news__info">
                                <p class="news__time"><?= $article->getPrettyDate() ?></p>
                                <h4 class="news__info-title"><?= $article->translation->title ?></h4>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>

                <div class="social__pagination">
                    <?php if (count($paginator->getRange()) > 1): ?>
                        <?= \app\widgets\Paginator::widget([
                                'paginator' => $paginator,
                                'pageUrl' => 'news']
                        ); ?>
                    <?php endif; ?>
                    <div class="pagination">
                        <?php if (\Yii::$app->language === 'ru-RU'): ?>
                            <span class="pagination__title"><?= \Yii::t('front', 'Показывать по').':' ?></span>
                        <?php endif; ?>
                        <ul class="pagination__list">
                            <?php foreach ($sortLimits as $limit): ?>
                                <?php $active = $limit === $currentLimit ? ' active' : '' ?>
                                <li class="<?= 'pagination__item'.$active ?>">
                                    <a href="<?= Url::to("/news/sort-limit/$limit") ?>"><?= $limit ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php if (\Yii::$app->language === 'kk-KZ'): ?>
                            <span class="pagination__title"><?= \Yii::t('front', 'Показывать по') ?></span>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>