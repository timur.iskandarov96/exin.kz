<?php

use yii\helpers\Url;

\app\assets\ArticleAsset::register($this);

/* @var $article \app\modules\news\models\News */
/* @var $otherArticles array */
/* @var $banner app\modules\promoBanner\models\PromoBanner */

?>

<main class="js-header-size">
    <div class="article article--news page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Наши новости'), 'url' => Url::to('/news')],
            ['title' => $article->translation->title, 'url' => false]
        ]]) ?>
        <div class="article__wrap">
            <div class="container-main">
                <h1 class="article__title"><?= $article->translation->title ?></h1>
                <time class="article__date" date-time="2020-10-27"><?= $article->getPrettyDate() ?></time>
            </div>
            <div class="container-main article__img-container">
                <div class="article__img article__block">
                    <?php if ($article->getUploadUrl('image')): ?>
                        <?php $imageUrl = $article->getUploadUrl('image') ?>
                    <?php else: ?>
                        <?php $imageUrl = Url::to('@web/images/building_thumb.jpg') ?>
                    <?php endif; ?>
                    <img src="<?= $imageUrl ?>"
                         alt="<?= $article->translation->image_description ?? $article->translation->title ?>"
                    />
                </div>
            </div>
            <div class="container-main">
                <div class="article__content">
                    <div class="article__block">
                        <article class="article__txt">
                            <?= $article->translation->description ?>
                        </article>
                        <div class="share">
                            <p class="share__label"><?= \Yii::t('front', 'Поделиться') ?></p>
                            <div class="social-likes share__btns">
                                <div class="share__btn share__btn--vk" data-service="vkontakte" data-title=""></div>
                                <div class="share__btn share__btn--facebook" data-service="facebook"
                                     data-title=""></div>
                                <div class="share__btn share__btn--twitter" data-service="twitter" data-title=""></div>
                            </div>
                        </div>
                    </div>

                    <!--div class="article__banners">

                        <?php if ($banner): ?>
                            <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                <img src="<?= $banner->getImagePath('img_vertical_ru') ?>"
                                     alt="<?= \Yii::t('front', 'Акционный баннер') ?>"/>
                            <?php else: ?>
                                <img src="<?= $banner->getImagePath('img_vertical_kz') ?>"
                                     alt="<?= \Yii::t('front', 'Акционный баннер') ?>"/>
                            <?php endif; ?>
                        <?php endif; ?>

                    </div-->
                </div>
            </div>
        </div>
        <div class="article__other">
            <div class="container-main">
                <h3><?= \Yii::t('front', 'Другие публикации') ?></h3>
                <div class="article__other-wrap">
                    <?php foreach ($otherArticles as $article): ?>
                        <a href="<?= Url::to('/news/' . $article->translation->slug) ?>" class="news__item">
                            <div class="news__img">
                                <?php if ($article->getThumbUploadUrl('image', 'thumb') ): ?>
                                    <?php $imageUrl = $article->getThumbUploadUrl('image', 'thumb') ?>
                                <?php else: ?>
                                    <?php $imageUrl = Url::to('@web/images/building_thumb.jpg') ?>
                                <?php endif; ?>
                                <img src="<?= $imageUrl ?>"
                                     alt="<?= $article->translation->image_description ?? $article->translation->title ?>"
                                     width="372"
                                     height="260"
                                />
                            </div>
                            <div class="news__info">
                                <p class="news__time"><?= $article->getPrettyDate() ?></p>
                                <h4 class="news__info-title"><?= $article->translation->title ?></h4>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</main>
