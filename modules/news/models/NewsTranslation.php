<?php

namespace app\modules\news\models;

use Yii;

/**
 * This is the model class for table "news_translation".
 *
 * @property integer $id
 * @property integer $news_id
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $slug
 *
 * @property News $news
 *
 * @method mixed evaluateAttributes($event) Evaluates the attribute value and assigns it to the current attributes.
 */
class NewsTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id'], 'integer'],
            [['description', 'image_description'], 'string'],
            [['lang', 'slug', 'image_description'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 512],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::class, 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'Новость',
            'lang' => 'Язык перевода',
            'title' => 'Заголовок',
            'description' => 'Текст',
            'slug' => 'ЧПУ',
            'image_description' => 'Описание изображения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::class, ['id' => 'news_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Bridge\Slug\BridgeSlugBehavior',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                'ensureUnique' => 1,
                'replacement' => '-',
                'lowercase' => 1,
                'immutable' => 1,
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }
}
