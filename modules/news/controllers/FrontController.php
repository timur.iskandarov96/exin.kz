<?php


namespace app\modules\news\controllers;


use app\components\front\Paginator;
use app\modules\news\models\News;
use app\modules\promoBanner\models\PromoBanner;
use yii\base\Exception;
use yii\web\Controller;
use Yii;

class FrontController extends Controller
{
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $request = Yii::$app->request;

        if ($session->has('newsSortLimit') && $request->get('sort-limit') == null) {
            $sortLimit = $session->get('newsSortLimit');
        } else {
            $sortLimit = $request->get('sort-limit') ?? 6;
            $session->set('newsSortLimit', $sortLimit);
        }

        if ($session->has('newsPage') && $request->get('page') == null) {
            $page = $session->get('newsPage');
        } else {
            $page = $request->get('newsPage') ?? 1;
            $session->set('newsPage', $page);
        }

        $offset =  ((int) $page - 1) * (int) $sortLimit;

        $articles = News::find()
            ->active()
            ->limit($sortLimit)
            ->offset($offset)
            ->orderBy(['id' => SORT_DESC])
            ->all()
        ;
        $allArticlesCount = (int) News::find()->active()->count();

        $division = 1;

        if ($allArticlesCount % Paginator::LIMIT_PAGES_COUNT > 0) {
            $division = (int) $allArticlesCount / Paginator::LIMIT_PAGES_COUNT + 1;
        } else {
            $division = (int) $allArticlesCount / Paginator::LIMIT_PAGES_COUNT;
        }

        $paginatorLimit = $division > Paginator::LIMIT_PAGES_COUNT ? Paginator::LIMIT_PAGES_COUNT : $division;

        if ($allArticlesCount <= $sortLimit) {
            $paginatorLimit = 1;
        }

        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => 'Новости'
            ]
        ]);

        return $this->render('index', [
            'articles' => $articles,
            'sortLimits' => [6, 9, 12],
            'currentLimit' => (int) $sortLimit,
            'paginator' => new Paginator($allArticlesCount, $paginatorLimit, $page, $sortLimit),
            'currentPage' => (int) $page
        ]);
    }

    public function actionView($slug)
    {
        try {
            $article = News::findBySlug($slug);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            $article = null;
        }

        \Yii::$app->metaTags->registerModel($article);

        return $this->render('view', [
            'article' => $article,
            'otherArticles' => News::find()
                ->active()
                ->where(['<>', 'id', $article ? $article->id : 0])
                ->orderBy(['id' => SORT_DESC])
                ->limit(3)
                ->all(),
            'banner' => PromoBanner::find()->active()->one()
        ]);
    }
}