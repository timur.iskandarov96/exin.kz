<?php

namespace app\modules\news\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * NewsController implements the CRUD actions for [[app\modules\news\models\News]] model.
 * @see app\modules\news\models\News
 */
class NewsController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\news\models\News';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\news\models\search\NewsSearch';

    /**
    * @inheritdoc
    */
    public $createScenario = 'create';

    /**
    * @inheritdoc
    */
    public $updateScenario = 'update';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\news\models\News',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
