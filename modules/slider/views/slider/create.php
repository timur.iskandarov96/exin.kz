<?php

/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\Slider */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

