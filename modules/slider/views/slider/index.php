<?php

use dosamigos\grid\GridView;
use kartik\date\DatePicker;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\slider\models\search\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'class' => 'Bridge\Core\Widgets\Columns\ImageColumn',
            'attribute' => 'image',
        ],
        [
            'class' => 'Bridge\Core\Widgets\Columns\TruncatedTextColumn',
            'attribute' => 'text',
        ],
        [
            'class' => \app\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],
        [
            'class' => 'yii2tech\admin\grid\PositionColumn',
            'attribute' => 'position',
            'template' => '<b>({value})</b>&nbsp;<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        ],
        [
            'attribute' => 'created_at',
            'filter' => DatePicker::widget([
                'attribute' => 'created_at',
                'language' => 'ru',
                'model' => $searchModel,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]),
            'value' => function ($data) {
                if ($data->created_at) {
                    return \Yii::$app->formatter->asDate($data->created_at, 'dd.MM.yyyy H:mm:ss');
                } else {
                    return 'Не задано';
                }
            }
        ],
        // 'updated_at',
        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
