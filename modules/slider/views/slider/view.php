<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\Html as YiiHtml;

/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\Slider */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'image',
                'value' => function ($model) {
                    return YiiHtml::img(Url::to($model->getImagePath()));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'text',
                'format' => 'raw'
            ],
            [
                'attribute' => 'is_active',
                'value' => function($data) {
                    if ($data->is_active) {
                        return $data->is_active === 1 ? 'Да' : 'Нет';
                    }
                }
            ],
            'position',
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    if ($data->created_at) {
                        return \Yii::$app->formatter->asDate($data->created_at, 'dd.MM.yyyy H:mm:ss');
                    } else {
                        return 'Не задано';
                    }
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($data) {
                    if ($data->updated_at) {
                        return \Yii::$app->formatter->asDate($data->created_at, 'dd.MM.yyyy H:mm:ss');
                    } else {
                        return 'Не задано';
                    }
                }
            ],
        ],
    ]) ?>
    </div>
</div>