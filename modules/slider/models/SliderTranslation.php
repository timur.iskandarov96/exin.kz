<?php

namespace app\modules\slider\models;

use Yii;

/**
 * This is the model class for table "slider_translations".
 *
 * @property integer $id
 * @property string $lang
 * @property integer $slider_id
 * @property string $text
 *
 * @property Slider $slider
 */
class SliderTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_translations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'text'], 'required'],
            [['slider_id'], 'integer'],
            [['text'], 'string'],
            [['lang'], 'string', 'max' => 255],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Slider::class, 'targetAttribute' => ['slider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Язык перевода',
            'slider_id' => 'Slider ID',
            'text' => 'Текстовое поле',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Slider::className(), ['id' => 'slider_id']);
    }

}
