<?php

namespace app\modules\slider\models;

use app\modules\faq\models\translation\FaqTranslation;
use app\modules\telegram_texts\models\TelegramTextTranslation;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $image
 * @property string $text
 * @property integer $is_active
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition($position) Moves owner record to the specific position.
 * @method bool getIsFirst() Checks whether this record is the first in the list.
 * @method bool getIsLast() Checks whether this record is the the last in the list.
 * @method \BaseActiveRecord|static|null findPrev() Finds record previous to this one.
 * @method \BaseActiveRecord|static|null findNext() Finds record next to this one.
 * @method \BaseActiveRecord|static|null findFirst() Finds the first record in the list.
 * @method \BaseActiveRecord|static|null findLast() Finds the last record in the list.
 * @method mixed beforeInsert($event) Handles owner 'beforeInsert' owner event, preparing its positioning.
 * @method mixed beforeUpdate($event) Handles owner 'beforeInsert' owner event, preparing its possible re-positioning.
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'text' => 'Текстовое поле',
            'is_active' => 'Активность',
            'position' => 'Порядок вывода',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\slider\models\query\SliderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\slider\models\query\SliderQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image',
                'path' => '@webroot/media/slider/{id}',
                'url' => '@web/media/slider/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => [
                    '200x200' => [
                        'thumb' => 200,
                        'height' => 200,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    '1920x1080' => [
                        'width' => 1920,
                        'height' => 1080,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'preview' => [
                        'width' => 50,
                        'height' => 50,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => SliderTranslation::class,
                'translationModelRelationColumn' => 'slider_id'
            ]
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrentTranslation(): ActiveQuery
    {
        return $this->hasOne(SliderTranslation::class, ['slider_id' => 'id'])
            ->onCondition([SliderTranslation::tableName() . '.lang' => \Yii::$app->language]);
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->currentTranslation->text;
    }

    public function getImagePath()
    {
        return \Yii::getAlias('@web').'/media/slider/'.$this->id.'/'.$this->image;
    }

    public function get1920Image()
    {
        return $this->getThumbUploadUrl('image', '1920x1080');
    }
}
