<?php

namespace app\modules\slider\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\slider\models\Slider]].
 *
 * @see \app\modules\slider\models\Slider
 */
class SliderQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => true]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\slider\models\Slider[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\slider\models\Slider|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
