<?php

namespace app\modules\exclusiveSocial\models;

use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\base\Exception;

/**
 * This is the model class for table "exclusive_social".
 *
 * @property integer $id
 * @property string $publication_date
 * @property integer $position
 * @property integer $is_active
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ExclusiveSocialTranslation[] $exclusiveSocialTranslations
 *
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition($position) Moves owner record to the specific position.
 * @method bool getIsFirst() Checks whether this record is the first in the list.
 * @method bool getIsLast() Checks whether this record is the the last in the list.
 * @method \BaseActiveRecord|static|null findPrev() Finds record previous to this one.
 * @method \BaseActiveRecord|static|null findNext() Finds record next to this one.
 * @method \BaseActiveRecord|static|null findFirst() Finds the first record in the list.
 * @method \BaseActiveRecord|static|null findLast() Finds the last record in the list.
 * @method mixed beforeInsert($event) Handles owner 'beforeInsert' owner event, preparing its positioning.
 * @method mixed beforeUpdate($event) Handles owner 'beforeInsert' owner event, preparing its possible re-positioning.
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class ExclusiveSocial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exclusive_social';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publication_date'], 'required'],
            [['publication_date', 'created_at', 'updated_at'], 'safe'],
            [['position', 'is_active'], 'integer'],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publication_date' => 'Дата публикации',
            'position' => 'Порядок вывода',
            'is_active' => 'Активность',
            'image' => 'Изображение',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExclusiveSocialTranslations()
    {
        return $this->hasMany(ExclusiveSocialTranslation::className(), ['exclusive_social_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\exclusiveSocial\models\query\ExclusiveSocialQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\exclusiveSocial\models\query\ExclusiveSocialQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image',
                'path' => '@webroot/media/exclusive_social/{id}',
                'url' => '@web/media/exclusive_social/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => [
                    'thumb_main' => [
                        'width' => 763,
                        'height' => 550,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'thumb' => [
                        'width' => 352,
                        'height' => 240,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'preview' => [
                        'width' => 50,
                        'height' => 50,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => ExclusiveSocialTranslation::class,
                'translationModelRelationColumn' => 'exclusive_social_id'
            ],
            'metaTag' => [
                'class' => 'Bridge\Core\Behaviors\MetaTagBehavior',
                'titleColumn' => 'translation.title',
                'descriptionColumn' => 'translation.description',
            ],
            "publication_dateBeforeSave" => [
                "class" => TimestampBehavior::class,
                "attributes" => [
                    ActiveRecord::EVENT_BEFORE_INSERT => "publication_date",
                    ActiveRecord::EVENT_BEFORE_UPDATE => "publication_date",
                ],
                "value" => function() {
                    return Yii::$app->formatter->asDate($this->publication_date, "Y-MM-dd");
                }
            ],
        ];
    }

    /**
     * @param string $slug
     * @return ExclusiveSocial
     * @throws Exception
     */
    public static function findBySlug(string $slug) : ExclusiveSocial
    {
        $translated = ExclusiveSocialTranslation::find()->where(['slug' => $slug])->one();

        if (!$translated) {
            throw new Exception('Could not find news article translation');
        }

        $articleId = $translated->exclusive_social_id;
        $article = self::findOne($articleId);

        if (!$article) {
            throw new Exception('Could not find news article');
        }

        return $article;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getPrettyDate() : string
    {
        Yii::$app->formatter->locale = Yii::$app->language;
        return Yii::$app->formatter->asDate($this->publication_date);
    }

    /**
     * @return string
     */
    public function getUrl() : string
    {
        return Url::to(['/exclusiveSocial/front/view', 'slug' => $this->translation->slug]);
    }
}
