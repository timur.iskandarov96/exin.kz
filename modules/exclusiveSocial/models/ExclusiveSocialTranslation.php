<?php

namespace app\modules\exclusiveSocial\models;

use Yii;

/**
 * This is the model class for table "exclusive_social_translation".
 *
 * @property integer $id
 * @property string $lang
 * @property integer $exclusive_social_id
 * @property string $preview_title
 * @property string $main_title
 * @property string $description
 * @property string $preview_description
 * @property string $slug
 *
 * @property ExclusiveSocial $exclusiveSocial
 */
class ExclusiveSocialTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exclusive_social_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exclusive_social_id'], 'integer'],
            [['description', 'preview_description', 'slug', 'image_description'], 'string'],
            [['lang', 'preview_title', 'main_title', 'slug', 'image_description'], 'string', 'max' => 255],
            [['exclusive_social_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExclusiveSocial::class, 'targetAttribute' => ['exclusive_social_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Язык перевода',
            'exclusive_social_id' => 'Exclusive Social ID',
            'preview_title' => 'Заголовок превью',
            'main_title' => 'Заголовок',
            'description' => 'Текст',
            'preview_description' => 'Текст превью',
            'slug' => 'ЧПУ',
            'image_description' => 'Описание изображения'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExclusiveSocial()
    {
        return $this->hasOne(ExclusiveSocial::class, ['id' => 'exclusive_social_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Bridge\Slug\BridgeSlugBehavior',
                'slugAttribute' => 'slug',
                'attribute' => 'preview_title',
                'ensureUnique' => 1,
                'replacement' => '-',
                'lowercase' => 1,
                'immutable' => 1,
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }
}
