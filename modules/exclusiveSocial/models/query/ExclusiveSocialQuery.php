<?php

namespace app\modules\exclusiveSocial\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\exclusiveSocial\models\ExclusiveSocial]].
 *
 * @see \app\modules\exclusiveSocial\models\ExclusiveSocial
 */
class ExclusiveSocialQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\exclusiveSocial\models\ExclusiveSocial[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\exclusiveSocial\models\ExclusiveSocial|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
