<?php

use yii\helpers\Url;

/* @var $articles \app\modules\exclusiveSocial\models\ExclusiveSocial[] */
/* @var $sortLimits array */
/* @var $currentLimit integer */
/* @var $currentPage integer */
/* @var $paginator app\components\front\Paginator */

?>

<main class="js-header-size">
    <div class="social page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => 'Exclusive Social', 'url' => false]
        ]]) ?>
        <div class="social__inner">
            <div class="container-main">
                <h1 class="social__title">Exclusive Social</h1>
                <div class="social__news-wrap">
                    <div class="social__news">
                        <?php foreach ($articles as $key => $article): ?>
                            <?php if (current($articles) === $article): ?>
                                <?php $isMain = ' social__new--big'; ?>
                                <?php
                                    $width = 763;
                                    $height = 550;
                                    $image = $article->getThumbUploadUrl('image', 'thumb_main');

                                ?>
                            <?php else: ?>
                                <?php $isMain = ''; ?>
                                <?php
                                    $width = 352;
                                    $height = 240;
                                    $image = $article->getThumbUploadUrl('image', 'thumb');
                                ?>
                            <?php endif ?>
                            <div class="<?= "social__new".$isMain ?>">
                                <a href="<?= Url::to('/exclusive-social/'.$article->translation->slug) ?>">
                                    <?php if($image): ?>
                                        <?php $imageUrl = $image ?>
                                    <?php else: ?>
                                        <?php $imageUrl = Url::to('@web/images/building_thumb.jpg') ?>
                                    <?php endif; ?>
                                    <div class="social__new-img">
                                        <img src="<?= $imageUrl ?>"
                                             alt="<?= $article->translation->image_description ?? $article->translation->preview_title ?>"
                                             width="<?= $width ?>"
                                             height="<?= $height ?>"
                                        />
                                    </div>
                                </a>

                                <div class="social__new-desc">
                                    <div class="social__title-wrap">
                                        <a href="<?= Url::to('/exclusive-social/'.$article->translation->slug) ?>" class="social__new-title">
                                            <?= $article->translation->preview_title ?>
                                        </a>
                                    </div>
                                    <p class="social__new-txt"><?= $article->translation->preview_description ?></p>
                                    <a href="<?= Url::to('/exclusive-social/'.$article->translation->slug) ?>" class="social__new-link"><?= \Yii::t('front', 'Подробнее') ?></a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="social__pagination">
                    <?php if (count($paginator->getRange()) > 1): ?>
                        <?= \app\widgets\Paginator::widget([
                                'paginator' => $paginator,
                                'pageUrl' => 'exclusive-social']
                        ); ?>
                    <?php endif; ?>
                    <div class="pagination">
                        <?php if (\Yii::$app->language === 'ru-RU'): ?>
                            <span class="pagination__title"><?= \Yii::t('front', 'Показывать по').':' ?></span>
                        <?php endif; ?>
                        <ul class="pagination__list">
                            <?php foreach ($sortLimits as $limit): ?>
                                <?php $active = $limit === $currentLimit ? ' active' : '' ?>
                                    <li class="<?= 'pagination__item'.$active ?>">
                                        <a href="<?= Url::to("/exclusive-social/sort-limit/$limit") ?>"><?= $limit ?></a>
                                    </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php if (\Yii::$app->language === 'kk-KZ'): ?>
                            <span class="pagination__title"><?= \Yii::t('front', 'Показывать по') ?></span>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
