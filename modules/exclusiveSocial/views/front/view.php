<?php

use yii\helpers\Html;
use yii\helpers\Url;

\app\assets\ArticleAsset::register($this);

/* @var $article app\modules\exclusiveSocial\models\ExclusiveSocial */
/* @var $otherArticles array */
/* @var $banner app\modules\promoBanner\models\PromoBanner */

?>
<main class="js-header-size">
    <div class="article article--social page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => 'Exclusive social', 'url' => Url::to('/exclusive-social')],
            ['title' => $article->translation->preview_title, 'url' => false]
        ]]) ?>
        <div class="article__wrap">
            <div class="container-main">
                <h1 class="article__title"><?= $article->translation->main_title ?></h1>
                <time class="article__date" date-time="2020-10-27"><?= $article->getPrettyDate() ?></time>
            </div>
            <div class="container-main article__img-container">
                <div class="article__img article__block">
                    <?php if ($article->getUploadUrl('image')): ?>
                        <?php $imageUrl = $article->getUploadUrl('image') ?>
                    <?php else: ?>
                        <?php $imageUrl = Url::to('@web/images/building_thumb.jpg') ?>
                    <?php endif; ?>
                    <img src="<?= $imageUrl ?>"
                         alt="<?= $article->translation->image_description ?>"
                         width="1175"
                         height="603"
                    />
                </div>
            </div>
            <div class="container-main">
                <div class="article__content">
                    <div class="article__block">
                        <article class="article__txt">
                           <?= $article->translation->description ?>
                        </article>
                        <div class="share">
                            <p class="share__label">Поделиться</p>
                            <div class="social-likes share__btns">
                                <div class="share__btn share__btn--vk" data-service="vkontakte" data-title=""></div>
                                <div class="share__btn share__btn--facebook" data-service="facebook" data-title=""></div>
                                <div class="share__btn share__btn--twitter" data-service="twitter" data-title=""></div>
                            </div>
                        </div>
                    </div>
                    <!--div class="article__banners">

                        <?php if ($banner): ?>
                            <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                <img src="<?= $banner->getImagePath('img_vertical_ru') ?>"
                                     alt="<?= \Yii::t('front', 'Акционный баннер') ?>"/>
                            <?php else: ?>
                                <img src="<?= $banner->getImagePath('img_vertical_kz') ?>"
                                     alt="<?= \Yii::t('front', 'Акционный баннер') ?>"/>
                            <?php endif; ?>
                        <?php endif; ?>

                    </div-->
                </div>
            </div>
        </div>
        <div class="article__other">
            <div class="container-main">
                <h3><?= \Yii::t('front', 'Другие публикации') ?></h3>
                <div class="article__other-wrap">
                    <?php foreach ($otherArticles as $article): ?>
                        <div class="social__new">
                            <a href="<?= Url::to('/exclusive-social/'.$article->translation->slug) ?>">
                                <div class="social__new-img">
                                    <?php if ($article->getThumbUploadUrl('image', 'thumb') ): ?>
                                        <?php $imageUrl = $article->getThumbUploadUrl('image', 'thumb') ?>
                                    <?php else: ?>
                                        <?php $imageUrl = Url::to('@web/images/building_thumb.jpg') ?>
                                    <?php endif; ?>
                                    <img src="<?= $imageUrl ?>"
                                         alt="<?= $article->translation->image_description ??
                                            $article->translation->preview_title ?>"
                                         width="352"
                                         height="240"
                                    />
                                </div>
                            </a>
                            <div class="social__new-desc">
                                <div class="social__title-wrap">
                                    <a
                                        class="social__new-title"
                                        href="<?= Url::to('/exclusive-social/'.$article->translation->slug) ?>"
                                    >
                                        <?= $article->translation->preview_title ?>
                                    </a>
                                </div>
                                <p class="social__new-txt"><?= $article->translation->preview_description ?></p>
                                <a
                                    href="<?= Url::to('/exclusive-social/'.$article->translation->slug) ?>"
                                    class="social__new-link"
                                >
                                    <?= \Yii::t('front', 'Подробнее') ?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</main>
