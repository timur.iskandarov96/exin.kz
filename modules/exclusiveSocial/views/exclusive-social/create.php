<?php

/* @var $this yii\web\View */
/* @var $model app\modules\exclusiveSocial\models\ExclusiveSocial */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Exclusive Socials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

