<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\exclusiveSocial\models\ExclusiveSocial
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']exclusive_social_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']preview_title')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']main_title')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']preview_description')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']description')->richTextArea(['options' => ['preset' => 'full','rows' => 6]]) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']slug')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']image_description')->textInput() ?>
