<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\exclusiveSocial\models\ExclusiveSocial */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->translate($model, '@app/modules/exclusiveSocial/views/exclusive-social/_form-translation') ?>

        <?php if ($model->isNewRecord): ?>

            <?= $form->field($model, 'publication_date')
                ->datePicker([
                    'options' => [
                        'value' => (new \DateTimeImmutable())->format('d.m.Y'),
                    ],
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                        'autoclose' => true,
                    ]
                ]) ?>

        <?php else: ?>

            <?= $form->field($model, 'publication_date')
                ->datePicker([
                    'options' => [
                        'value' => (new DateTimeImmutable($model->publication_date))->format('d.m.Y')
                    ],
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                        'autoclose' => true,
                    ]
                ]) ?>

        <?php endif; ?>

        <?= $form->field($model, 'is_active')->switchInput() ?>

        <?= $form->field($model, 'image')->imageUpload() ?>

    </div>

    <div class="col-md-4">
        <?= $form->metaTags($model->metaTag) ?>
    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
