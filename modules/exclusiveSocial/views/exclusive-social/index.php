<?php

use dosamigos\grid\GridView;
use kartik\widgets\DatePicker;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\exclusiveSocial\models\search\ExclusiveSocialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Exclusive Social';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'label' => 'Заголовок',
            'value' => function($model) {
                return $model->translation->preview_title;
            }
        ],
        [
            'attribute' => 'publication_date',
            'filter' => DatePicker::widget([
                'attribute' => 'publication_date',
                'language' => 'ru',
                'model' => $searchModel,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]),
            'value' => function ($data) {
                if ($data->created_at) {
                    return \Yii::$app->formatter->asDate($data->publication_date, 'dd.MM.yyyy');
                } else {
                    return 'Не задано';
                }
            }
        ],
        [
            'class' => \app\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],
        [
            'class' => 'Bridge\Core\Widgets\Columns\ImageColumn',
            'attribute' => 'image',
        ],
        [
            'class' => 'yii2tech\admin\grid\PositionColumn',
            'value' => 'position',
            'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        ],

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
