<?php

/* @var $this yii\web\View */
/* @var $model app\modules\exclusiveSocial\models\ExclusiveSocial */

$this->title = 'Редактировать запись: ' . $model->translation->preview_title;
$this->params['breadcrumbs'][] = ['label' => 'Exclusive Socials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->translation->preview_title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


