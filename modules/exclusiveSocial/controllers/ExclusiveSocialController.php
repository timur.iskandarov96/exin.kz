<?php

namespace app\modules\exclusiveSocial\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * ExclusiveSocialController implements the CRUD actions for [[app\modules\exclusiveSocial\models\ExclusiveSocial]] model.
 * @see app\modules\exclusiveSocial\models\ExclusiveSocial
 */
class ExclusiveSocialController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\exclusiveSocial\models\ExclusiveSocial';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\exclusiveSocial\models\search\ExclusiveSocialSearch';

    /**
    * @inheritdoc
    */
    public $createScenario = 'create';

    /**
    * @inheritdoc
    */
    public $updateScenario = 'update';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\exclusiveSocial\models\ExclusiveSocial',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
