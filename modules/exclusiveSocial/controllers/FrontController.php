<?php


namespace app\modules\exclusiveSocial\controllers;


use app\components\front\Paginator;
use app\modules\exclusiveSocial\models\ExclusiveSocial;
use app\modules\promoBanner\models\PromoBanner;
use yii\base\Exception;
use yii\web\Controller;
use Yii;


class FrontController extends Controller
{
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $request = Yii::$app->request;

        if ($session->has('sortLimit') && $request->get('sort-limit') == null) {
            $sortLimit = $session->get('sortLimit');
        } else {
            $sortLimit = $request->get('sort-limit') ?? 5;
            $session->set('sortLimit', $sortLimit);
        }

        if ($session->has('page') && $request->get('page') == null) {
            $page = $session->get('page');
        } else {
            $page = $request->get('page') ?? 1;
            $session->set('page', $page);
        }

        $offset =  ((int) $page - 1) * (int) $sortLimit;

        $articles = ExclusiveSocial::find()
            ->active()
            ->limit($sortLimit)
            ->offset($offset)
            ->orderBy(['id' => SORT_DESC])
            ->all()
        ;
        $allArticlesCount = (int) ExclusiveSocial::find()->active()->count();

        $division = 1;

        if ($allArticlesCount % Paginator::LIMIT_PAGES_COUNT > 0) {
            $division = (int) $allArticlesCount / Paginator::LIMIT_PAGES_COUNT + 1;
        } else {
            $division = (int) $allArticlesCount / Paginator::LIMIT_PAGES_COUNT;
        }

        $paginatorLimit = $division > Paginator::LIMIT_PAGES_COUNT ? Paginator::LIMIT_PAGES_COUNT : $division;

        if ($allArticlesCount <= $sortLimit) {
            $paginatorLimit = 1;
        }

        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => 'exclusive-social'
            ]
        ]);

        return $this->render('index', [
            'articles' => $articles,
            'sortLimits' => [5, 8, 11],
            'currentLimit' => (int) $sortLimit,
            'paginator' => new Paginator($allArticlesCount, $paginatorLimit, $page, $sortLimit),
            'currentPage' => (int) $page
        ]);
    }

    public function actionView(string $slug)
    {
        try {
            $article = ExclusiveSocial::findBySlug($slug);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            $article = null;
        }

        try {
            \Yii::$app->metaTags->registerModel($article);
        } catch (\Exception $e) {
            \Yii::error('Meta tag error in ExclusiveSocial article');
        }

        return $this->render('view', [
            'article' => $article,
            'otherArticles' => ExclusiveSocial::find()
                ->active()
                ->where(['<>', 'id', $article ? $article->id : 0])
                ->orderBy(['id' => SORT_DESC])
                ->limit(3)
                ->all(),
            'banner' => PromoBanner::find()->active()->one()
        ]);
    }
}