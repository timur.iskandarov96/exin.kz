<?php

namespace app\modules\project\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * ApartmentController implements the CRUD actions for [[app\modules\project\models\Apartment]] model.
 * @see app\modules\project\models\Apartment
 */
class ApartmentController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\project\models\Apartment';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\project\models\search\ApartmentSearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\project\models\Apartment',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
