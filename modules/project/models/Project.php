<?php

namespace app\modules\project\models;

use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\helpers\Url;


/**
 * This is the model class for table "projects".
 *
 * @property string $name
 * @property integer $id
 * @property string $image
 * @property string $logo
 * @property string $address
 * @property string $url
 * @property integer $status_id
 * @property integer $is_active
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Apartment[] $apartments
 * @property ProjectStatus $status
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition($position) Moves owner record to the specific position.
 * @method bool getIsFirst() Checks whether this record is the first in the list.
 * @method bool getIsLast() Checks whether this record is the the last in the list.
 * @method \BaseActiveRecord|static|null findPrev() Finds record previous to this one.
 * @method \BaseActiveRecord|static|null findNext() Finds record next to this one.
 * @method \BaseActiveRecord|static|null findFirst() Finds the first record in the list.
 * @method \BaseActiveRecord|static|null findLast() Finds the last record in the list.
 * @method mixed beforeInsert($event) Handles owner 'beforeInsert' owner event, preparing its positioning.
 * @method mixed beforeUpdate($event) Handles owner 'beforeInsert' owner event, preparing its possible re-positioning.
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'name',
//                'image',
//                'logo',
            ], 'required'],
            [[
                'status_id',
                'is_active',
                'position',
                'no_locations'
            ], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['logo'], 'file', 'on' => ['create', 'update'], 'extensions' => ['svg']],
            [['name', 'url'], 'string', 'max' => 256],
            [['lat', 'lon'], 'string', 'max' => 255],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
            [['background_image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
            [['scheme_image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
            [['address_image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectStatus::class, 'targetAttribute' => ['status_id' => 'id']],
            [['color_logo'], 'file', 'on' => ['create', 'update'], 'extensions' => ['svg', 'jpg', 'jpeg', 'png']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'logo' => 'Логотип',
            'status_id' => 'Статус',
            'is_active' => 'Активность',
            'position' => 'Порядок вывода',
            'url' => 'Ссылка',
            'lat' => 'Широта',
            'lon' => 'Долгота',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'background_image' => 'Изображение фона',
            'scheme_image' => 'Ген план',
            'address_image' => 'Изображение расположения',
            'color_logo' => 'Цветной логотип',
            'no_locations' => 'Все квартиры проданы'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartments()
    {
        return $this->hasMany(Apartment::class, ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ProjectStatus::class, ['id' => 'status_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\project\models\query\ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\project\models\query\ProjectQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image',
                'path' => '@webroot/media/projects/images/{id}',
                'url' => '@web/media/projects/images/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => [
                    'thumb' => [
                        'width' => 372,
                        'height' => 280,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'preview' => [
                        'width' => 50,
                        'height' => 50,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'thumb_location' => [
                        'width' => 655,
                        'height' => 450,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
            'background_imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'background_image',
                'path' => '@webroot/media/projects/background_images/{id}',
                'url' => '@web/media/projects/background_images/{id}',
                'scenarios' => ['create', 'update', 'delete'],
                'thumbs' => [
                    'thumb' => [
                        'width' => 372,
                        'height' => 280,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    '1600х860' => [
                        'width' => 1600,
                        'height' => 860,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
            'scheme_imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'scheme_image',
                'path' => '@webroot/media/projects/scheme_images/{id}',
                'url' => '@web/media/projects/scheme_images/{id}',
                'scenarios' => ['create', 'update', 'delete'],
                'thumbs' => [
                    'thumb' => [
                        'width' => 435,
                        'height' => 240,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
            'address_imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'address_image',
                'path' => '@webroot/media/projects/address_images/{id}',
                'url' => '@web/media/projects/address_images/{id}',
                'scenarios' => ['create', 'update', 'delete'],
                'thumbs' => [
                    'thumb' => [
                        'width' => 240,
                        'height' => 240,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
            'fileUpload' =>
                [
                    'class' => 'Bridge\Core\Behaviors\BridgeUploadBehavior',
                    'attribute' => 'logo',
                    'path' => '@webroot/media/projects/logos/{id}',
                    'url' => '@web/media/projects/logos/{id}',
                    'scenarios' => ['create', 'update', 'delete'],

            ],
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => ProjectTranslation::class,
                'translationModelRelationColumn' => 'project_id'
            ],
            'color_logoFileUpload' =>  [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadBehavior',
                'attribute' => 'color_logo',
                'path' => '@webroot/media/projects/color_logos/{id}',
                'url' => '@web/media/projects/color_logos/{id}',
                'scenarios' => ['create', 'update', 'delete'],
            ],
        ];
    }

    public function getPeopleModel() : ProjectPeople
    {
        return new ProjectPeople();
    }

    public function getBackgroundImagePath() : ?string
    {
        if ($this->background_image) {
            $fileName = '1600х860-'.$this->background_image;
            return Url::to('@web/media/projects/background_images/').$this->id.'/'.$fileName;
        }

        return null;
    }

    public function getLogoUrl() : string
    {
        return Url::to('@web/media/projects/logos/'.$this->id.'/'.$this->logo);
    }

    public function getColorLogoUrl() : ?string
    {
        if ($this->color_logo) {
            return Url::to('@web/media/projects/color_logos/' . $this->id . '/' . $this->color_logo);
        }

        return null;
    }

    public function getSchemeImageThumbUrl($profile = 'thumb') : ?string
    {
        if ($this->scheme_image) {
            return Url::to('@web/media/projects/scheme_images/' . $this->id . '/' . $profile . '-' . $this->scheme_image);
        }

        return null;
    }

    public function getAddressImageThumbUrl($profile = 'thumb') : ?string
    {
        if ($this->address_image) {
            return Url::to('@web/media/projects/address_images/' . $this->id . '/' . $profile . '-' . $this->address_image);
        }

        return null;
    }
}
