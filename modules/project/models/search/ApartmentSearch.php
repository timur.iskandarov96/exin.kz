<?php

namespace app\modules\project\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\project\models\Apartment;

/**
 * ApartmentSearch represents the model behind the search form of `app\modules\project\models\Apartment`.
 */
class ApartmentSearch extends Apartment{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'number_of_rooms', 'price', 'project_id'], 'integer'],
            [['square', 'max_square'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Apartment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number_of_rooms' => $this->number_of_rooms,
            'square' => $this->square,
            'price' => $this->price,
            'project_id' => $this->project_id,
        ]);

        return $dataProvider;
    }
}
