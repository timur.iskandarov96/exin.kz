<?php


namespace app\modules\project\models;


use yii\helpers\ArrayHelper;

class ApartmentDropdowns
{
    public static function projects()
    {
        return ArrayHelper::map(
            Project::find()->active()->all(),
            'id',
            'name'
        );
    }
}