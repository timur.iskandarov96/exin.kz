<?php

namespace app\modules\project\models;

use Yii;

/**
 * @deprecated
 *
 * This is the model class for table "project_people".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $full_name
 * @property string $job_position
 * @property string $lang
 *
 * @property Project $project
 */
class ProjectPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'full_name', 'job_position', 'lang'], 'required'],
            [['project_id'], 'integer'],
            [['full_name'], 'string', 'max' => 512],
            [['job_position', 'lang'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'full_name' => 'ФИО',
            'job_position' => 'Должность',
            'lang' => 'Язык перевода',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\project\models\query\ProjectPeopleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\project\models\query\ProjectPeopleQuery(get_called_class());
    }

}
