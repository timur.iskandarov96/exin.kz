<?php

namespace app\modules\project\models;

use Yii;

/**
 * This is the model class for table "apartments".
 *
 * @property integer $id
 * @property integer $number_of_rooms
 * @property double $square
 * @property integer $price
 * @property integer $project_id
 *
 * @property Project $project
 */
class Apartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apartments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number_of_rooms', 'price', 'project_id', 'square'], 'required'],
            [['number_of_rooms', 'price', 'project_id'], 'integer'],
            [['square', 'max_square'], 'number'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number_of_rooms' => 'Комнатность',
            'square' => 'Квадратура',
            'max_square' => 'Максимальная квадратура',
            'price' => 'Цена',
            'project_id' => 'Проект',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\project\models\query\ApartmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\project\models\query\ApartmentQuery(get_called_class());
    }
}
