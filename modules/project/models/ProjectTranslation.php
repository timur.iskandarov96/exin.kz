<?php

namespace app\modules\project\models;

use Yii;

/**
 * This is the model class for table "projects_translation".
 *
 * @property integer $id
 * @property string $lang
 * @property integer $project_id
 * @property string $address
 *
 * @property Project $project
 */
class ProjectTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['lang'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 512],
            [['description'], 'string'],
            [['address'], 'required'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Язык перевода',
            'project_id' => 'Проект',
            'address' => 'Адрес',
            'description' => 'Описание'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

}
