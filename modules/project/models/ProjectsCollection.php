<?php


namespace app\modules\project\models;


use app\widgets\Projects;

class ProjectsCollection
{
    /**
     * @return Project[]|array
     */
    public static function getAll()
    {
        return Project::find()->active()->all();
    }

    /**
     * @return Project[]|array
     */
    public static function getReady()
    {
        $readyStatusId = ProjectStatus::find()->where(['status' => 'сдан'])->one()->id;

        return Project::find()->where(['status_id' => $readyStatusId])->active()->all();
    }

    /**
     * @return Project[]|array
     */
    public static function getUnderConstruction()
    {
        $underConstructionStatusId = ProjectStatus::find()->where(['status' => 'строится'])->one()->id;

        return Project::find()->where(['status_id' => $underConstructionStatusId])->active()->all();
    }
}