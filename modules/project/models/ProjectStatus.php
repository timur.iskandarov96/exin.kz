<?php

namespace app\modules\project\models;

use Yii;

/**
 * This is the model class for table "project_statuses".
 *
 * @property integer $id
 * @property string $status
 *
 * @property Project[] $projects
 */
class ProjectStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::class, ['status_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\project\models\query\ProjectStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\project\models\query\ProjectStatusQuery(get_called_class());
    }

}
