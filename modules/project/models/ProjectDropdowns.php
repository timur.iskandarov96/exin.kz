<?php


namespace app\modules\project\models;

use yii\helpers\ArrayHelper;


/**
 * Class ProjectDropdowns
 * @package app\modules\project\models
 */
class ProjectDropdowns
{
    /**
     * @return array
     * @deprecated
     */
    public static function housingEstates()
    {
        return ArrayHelper::map(
            Project::find()->active()->all(),
            'id',
            'name'
        );
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return ArrayHelper::map(
            ProjectStatus::find()->all(),
            'id',
            'status'
        );
    }
}