<?php

namespace app\modules\project\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\project\models\Apartment]].
 *
 * @see \app\modules\project\models\Apartment
 */
class ApartmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\project\models\Apartment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\project\models\Apartment|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
