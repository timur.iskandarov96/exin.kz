<?php

namespace app\modules\project\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\project\models\ProjectStatus]].
 *
 * @see \app\modules\project\models\ProjectStatus
 */
class ProjectStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\project\models\ProjectStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\project\models\ProjectStatus|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
