<?php

namespace app\modules\project\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\project\models\Project]].
 *
 * @see \app\modules\project\models\Project
 */
class ProjectQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => true]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\project\models\Project[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\project\models\Project|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
