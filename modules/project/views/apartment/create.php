<?php

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Apartment */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Квартиры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

