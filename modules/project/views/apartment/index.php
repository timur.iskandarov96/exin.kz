<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\project\models\search\ApartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Квартиры';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'number_of_rooms',
            'value' => function($model) {
                if ($model->number_of_rooms) {
                    return $model->number_of_rooms . '-комн';
                }
            }
        ],
        [
            'attribute' => 'square',
            'value' => function($model) {
                if ($model->square) {
                    return number_format($model->square, 1, ',', '') . ' м²';
                }
            }
        ],
        [
            'attribute' => 'max_square',
            'value' => function($model) {
                if ($model->max_square) {
                    return number_format($model->max_square, 1, ',', '') . ' м²';
                }
            }
        ],
        [
            'attribute' => 'price',
            'value' => function($model) {
                if ($model->price) {
                    return number_format($model->price, 0, '', ' ') . ' тг';
                }
            }
        ],
        [
            'attribute' => 'project_id',
            'value' => function($model) {
                if ($model->project) {
                    return $model->project->name;
                }
            },
            'filter' => \app\modules\project\models\ApartmentDropdowns::projects()
        ],

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
