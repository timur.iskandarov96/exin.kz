<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;
use app\modules\project\models\ApartmentDropdowns;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Apartment */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->field($model, 'project_id')->dropDownList(
            ApartmentDropdowns::projects(),
            ['prompt' => 'Выберите...']
        ) ?>

        <?= $form->field($model, 'number_of_rooms')->textInput() ?>

        <?= $form->field($model, 'square')->textInput() ?>

        <?= $form->field($model, 'max_square')->textInput() ?>

        <?= $form->field($model, 'price')->textInput() ?>

    </div>

    <div class="col-md-4">

    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ?
            \Yii::t('bridge', 'Create') :
            \Yii::t('bridge', 'Update'), [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

<?php ActiveForm::end(); ?>
