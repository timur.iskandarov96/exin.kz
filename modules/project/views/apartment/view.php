<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Apartment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Квартиры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'number_of_rooms',
                'value' => function($model) {
                    if ($model->number_of_rooms) {
                        return $model->number_of_rooms . '-комн';
                    }
                }
            ],
            [
                'attribute' => 'square',
                'value' => function($model) {
                    if ($model->square) {
                        return number_format($model->square, 1, ',', '') . ' м²';
                    }
                }
            ],
            [
                'attribute' => 'max_square',
                'value' => function($model) {
                    if ($model->max_square) {
                        return number_format($model->max_square, 1, ',', '') . ' м²';
                    }
                }
            ],
            [
                'attribute' => 'price',
                'value' => function($model) {
                    if ($model->price) {
                        return number_format($model->price, 0, '', ' ') . ' тг';
                    }
                }
            ],
            [
                'attribute' => 'project_id',
                'value' => function($model) {
                    if ($model->project) {
                        return $model->project->name;
                    }
                }
            ],
        ],
    ]) ?>
    </div>
</div>