<?php

use app\widgets\core\CoreActiveForm;
use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Project */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = CoreActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'image')->imageUpload() ?>

        <?= $form->field($model, 'logo')->fileUpload() ?>

        <?= $form->field($model, 'color_logo')->fileUpload([], 'color_logoFileUpload') ?>

        <?= $form->translate($model, '@app/modules/project/views/project/_form-translation') ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status_id')->dropDownList(
                \app\modules\project\models\ProjectDropdowns::statuses(),
            ['prompt' => 'Выберите...']
        ) ?>

        <?= $form->field($model, 'is_active')->switchInput() ?>

        <?= $form->field($model, 'no_locations')->switchInput() ?>

        <h3 class="text-info text-center">Информация для фильтра квартир</h3>

        <?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'lon')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'background_image')->imageUpload([], 'background_imageUpload') ?>

        <?= $form->field($model, 'scheme_image')->imageUpload([], 'scheme_imageUpload') ?>

        <?= $form->field($model, 'address_image')->imageUpload([], 'address_imageUpload') ?>

    </div>

</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? \Yii::t('bridge', 'Create') : \Yii::t('bridge', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php CoreActiveForm::end(); ?>

<?php

$this->registerCss('
    .apartment-block {
        border: #191919 solid 1px; 
        border-radius: 5px;
        padding: 10px 10px;
        margin-top: 10px;
    }
');
?>
