<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\project\models\Project
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$peopleModel = $model->getPeopleModel();

?>
<?= $form->field($peopleModel, '[' . $languageCode . ']project_id')->hiddenInput()->label(false) ?>
<?= $form->field($peopleModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($peopleModel, '[' . $languageCode . ']full_name')->textInput() ?>
<?= $form->field($peopleModel, '[' . $languageCode . ']job_position')->textInput() ?>
