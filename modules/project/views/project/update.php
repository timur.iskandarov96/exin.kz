<?php

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Project */

$this->title = 'Редактировать проект: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Проекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


