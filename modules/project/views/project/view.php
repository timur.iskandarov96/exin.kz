<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Проекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'image',
                'value' => function($model) {
                    return Html::img($model->getUploadUrl('image'));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'logo',
                'value' => function($model) {
                    $logoPath = $model->getLogoUrl();
                    return '<object type="image/svg+xml" data="' . $logoPath . '" width="200" height="100">
                            Your browser does not support SVG
                        </object>';
                },
                'format' => 'raw'
            ],
            [
                'label' => 'Адрес',
                'value' => function(\app\modules\project\models\Project $model) {
                    return $model->translation->address;
                }
            ],
            [
                'attribute' => 'status_id',
                'value' => function($model) {
                    return $model->status->status;
                }
            ],
            [
                'attribute' => 'is_active',
                'value' => function($model) {
                    return $model->is_active ? 'Активен' : 'Не активен';
                }
            ],
            'position',
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    if ($data->created_at) {
                        return \Yii::$app->formatter->asDate($data->created_at, 'dd.MM.yyyy H:mm:ss');
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($data) {
                    if ($data->updated_at) {
                        return \Yii::$app->formatter->asDate($data->updated_at, 'dd.MM.yyyy H:mm:ss');
                    }
                    return null;
                }
            ],
        ],
    ]) ?>
    </div>
</div>

<?php if ($model->apartments): ?>
    <h2>Квартиры</h2>
    <div class="row">
        <div class="col-lg-4 detail-view-wrap">
            <?php foreach ($model->apartments as $apartment): ?>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'label' => 'Комнатность',
                            'value' => $apartment->number_of_rooms
                        ],
                        [
                            'label' => 'Площадь',
                            'value' => $apartment->square
                        ],
                        [
                            'label' => 'Цена',
                            'value' => $apartment->price
                        ]
                    ]
                ]) ?>
           <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<?php

$this->registerCss('
    object {
        background-color: #323232;
    }
');
?>