<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\project\models\Project
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']project_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']address')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']description')->richTextArea(['options' => ['rows' => 6]]) ?>
