<?php

use dosamigos\grid\GridView;
use yii\helpers\Url;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\project\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'name',
        [
            'class' => 'Bridge\Core\Widgets\Columns\ImageColumn',
            'attribute' => 'image',
        ],
        [
            'attribute' => 'logo',
            'value' => function (\app\modules\project\models\Project $model) {
                $logoPath = $model->getLogoUrl();
                return '<object type="image/svg+xml" data="' . $logoPath . '" width="200" height="100">
                            Your browser does not support SVG
                        </object>';
            },
            'format' => 'raw'
        ],
        [
            'label' => 'Адрес',
            'value' => function(\app\modules\project\models\Project $model) {
                return $model->translation->address;
            }
        ],
        [
            'class' => \app\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],
        [
            'attribute' => 'status_id',
            'filter' => \app\modules\project\models\ProjectDropdowns::statuses(),
            'value' => function($model) {
                return $model->status->status;
            }
        ],
        [
            'class' => 'yii2tech\admin\grid\PositionColumn',
            'attribute' => 'position',
            'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        ],

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>

<?php

$this->registerCss('
    object {
        background-color: #323232;
    }
');
?>
