<?php

namespace app\modules\crew\models;

use Yii;

/**
 * This is the model class for table "crew_translation".
 *
 * @property integer $id
 * @property integer $crew_id
 * @property string $full_name
 * @property string $job_position
 *
 * @property Crew $crew
 */
class CrewTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crew_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crew_id'], 'integer'],
            [['full_name', 'job_position'], 'required'],
            [['full_name', 'job_position'], 'string', 'max' => 255],
            [['crew_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crew::class, 'targetAttribute' => ['crew_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'crew_id' => 'Crew ID',
            'full_name' => 'ФИО',
            'job_position' => 'Должность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrew()
    {
        return $this->hasOne(Crew::className(), ['id' => 'crew_id']);
    }

}
