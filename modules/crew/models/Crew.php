<?php

namespace app\modules\crew\models;

use Yii;

/**
 * This is the model class for table "crew".
 *
 * @property integer $id
 * @property string $image
 * @property integer $is_active
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CrewTranslation[] $crewTranslations
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class Crew extends \yii\db\ActiveRecord
{
    public $fullName;

    public $jobPosition;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crew';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'position'], 'integer'],
            [['created_at', 'updated_at', 'fullName', 'jobPosition'], 'safe'],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Фото',
            'is_active' => 'Активность',
            'position' => 'Порядок очередности',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewTranslations()
    {
        return $this->hasMany(CrewTranslation::className(), ['position' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\crew\models\query\CrewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\crew\models\query\CrewQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image',
                'path' => '@webroot/media/crew/{id}',
                'url' => '@web/media/crew/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => CrewTranslation::class,
                'translationModelRelationColumn' => 'crew_id'
            ],
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
        ];
    }
}
