<?php

namespace app\modules\crew\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\crew\models\Crew]].
 *
 * @see \app\modules\crew\models\Crew
 */
class CrewQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['is_active' => 1]);
        return $this->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\crew\models\Crew[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\crew\models\Crew|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
