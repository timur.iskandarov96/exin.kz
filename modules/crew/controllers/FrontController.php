<?php


namespace app\modules\crew\controllers;


use app\modules\crew\models\Crew;
use app\modules\vacancy\models\Vacancy;
use yii\web\Controller;

class FrontController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => 'Наша команда'
            ]
        ]);

        return $this->render('index', [
            'crew' => Crew::find()->active()->all(),
            'vacancies' => Vacancy::find()->active()->all()
        ]);
    }
}