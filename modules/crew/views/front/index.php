<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $crew \app\modules\crew\models\Crew[] */
/* @var $vacancies array */

?>

<main class="js-header-size">
    <div class="team page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Наша команда'), 'url' => false]
        ]]) ?>
        <div class="container-main">
            <div class="team__block">
                <h1 class="team__title"><?= \Yii::t('front', 'Наша команда') ?></h1>
                <div class="team__list">
                    <?php foreach($crew as $key => $member): ?>
                        <?php $imgSrc = "background-image: url('" . $member->getUploadUrl('image') . "')" ?>
                        <div class="<?= 'team__item' ?>" style="<?= $imgSrc ?>">
                            <div class="team__info">
                                <h4 class="team__name"><?= $member->translation->full_name ?></h4>
                                <p class="team__position"><?= $member->translation->job_position ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="vacancies">
                    <div class="vacancies__title-block">
                        <h2><?= \Yii::t('front', 'Другие вакансии') ?></h2>
                        <p class="vacancies__count">
                            <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                <?= \Yii::t('front', 'Всего').' '.
                                    count($vacancies).' '.
                                    \app\components\front\Helper::spellVacancyCount(count($vacancies))
                                ?>
                            <?php else: ?>
                                <?= \Yii::t('front', 'Всего').' '.
                                    count($vacancies).' '.
                                    \Yii::t('front', 'вакансий')
                                ?>
                            <?php endif; ?>
                        </p>
                    </div>
                    <div class="vacancies__list">
                        <?php foreach ($vacancies as $v): ?>

                            <a href="<?= Url::to('/vacancy/'.$v->translation->slug)  ?>" class="vacancies__item">
                                <h4 class="vacancies__title"><?= $v->translation->title ?></h4>
                                <p class="vacancies__text"><?= $v->translation->category ?></p>
                            </a>

                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
</main>
