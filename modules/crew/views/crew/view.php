<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\crew\models\Crew */

$this->title = $model->translation->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Наша Команда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'ФИО',
                'value' => function($model) {
                    return $model->translation->full_name;
                }
            ],
            [
                'label' => 'Должность',
                'value' => function($model) {
                    return $model->translation->job_position;
                }
            ],
            [
                'attribute' => 'Фото',
                'value' => function($model) {
                    if ($model->image) {
                        return Html::img($model->getUploadUrl('image'), [
                                'width' => 480
                        ]);
                    }
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'is_active',
                'value' => function($model) {
                    if ($model->is_active) {
                        return $model->is_active === 1 ? 'Да' : 'Нет';
                    }
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>
    </div>
</div>