<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\crew\models\Crew
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']crew_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']full_name')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']job_position')->textInput() ?>
