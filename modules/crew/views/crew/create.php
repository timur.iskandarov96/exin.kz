<?php

/* @var $this yii\web\View */
/* @var $model app\modules\crew\models\Crew */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Наша Команда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

