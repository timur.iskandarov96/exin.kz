<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\crew\models\search\CrewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Наша Команда';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'label' => 'ФИО',
            'value' => function($model) {
                return $model->translation->full_name;
            }
        ],
        [
            'label' => 'Должность',
            'value' => function($model) {
                return $model->translation->job_position;
            }
        ],
        [
            'class' => 'Bridge\Core\Widgets\Columns\ImageColumn',
            'attribute' => 'image',
        ],
        [
            'class' => \app\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],
        [
            'class' => 'yii2tech\admin\grid\PositionColumn',
            'value' => 'position',
            'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        ],
        'created_at',

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
