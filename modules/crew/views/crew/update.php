<?php

/* @var $this yii\web\View */
/* @var $model app\modules\crew\models\Crew */

$this->title = 'Редактировать запись: ' . $model->translation->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Наша Команда', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->translation->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


