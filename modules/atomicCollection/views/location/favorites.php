<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $locations \app\modules\collection\models\Location[] */
/* @var $paginator app\components\front\Paginator */

?>

<main class="js-header-size">
    <div class="favorites page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Избранное'), 'url' => false]
        ]]) ?>
        <div class="container-main">
            <h1><?= \Yii::t('front', 'Избранное') ?></h1>
            <div class="results__controls">
                <div class="results__filter">
                    <p class="results__count"><?= \Yii::t('front', 'Всего') ?>: <span><?= count($locations) ?></span></p>
                </div>
            </div>
            <div class="results__list">

                <?php foreach ($locations as $location): ?>
                    <div class="results__item">
                        <a href="<?= Url::to(['/locations/'.$location->uuid]) ?>"><p class="results__item-title"><?= $location->getProjectName() ?></p></a>
                        <div class="results__img">
                            <a href="<?= Url::to(['/locations/'.$location->uuid]) ?>"><?= Html::img($location->getPlanImagePath(), ['alt' => 'Схема кваритиры']) ?></a>
                        </div>
                        <div class="results__info">
                            <div class="results__info-col">
                                <div class="results__subinfo">
                                    <div class="results__info-block">
                                        <p class="results__info-title"><?= \Yii::t('front', 'Комнат' ) ?></p>
                                        <p class="results__info-txt"><?= $location->room_number ?></p>
                                    </div>
                                </div>
                                <?php if ($location->finish): ?>
                                    <div class="results__info-block">
                                        <p class="results__info-title"><?= \Yii::t('front', 'Отделка') ?></p>
                                        <p class="results__info-txt"><?= \Yii::t('front', $location->finish) ?></p>
                                    </div>
                                <?php endif; ?>
                                <div class="results__info-block">
                                    <p class="results__info-title"><?= \Yii::t('front', 'Цена за м²') ?>, тг</p>
                                    <p class="results__info-txt"><?= $location->priceformetre ?></p>
                                </div>
                            </div>
                            <div class="results__info-col">
                                <div class="results__info-block">
                                    <p class="results__info-title"><?= \Yii::t('front', 'Площадь') ?>, м<span class="sup">2</span></p>
                                    <p class="results__info-txt"><?= $location->area ?></p>
                                </div class="results__info-block">
                                <div class="results__info-block">
                                    <p class="results__info-title"><?= \Yii::t('front', 'Статус строит-ва') ?></p>
                                    <p class="results__info-txt"><?= \Yii::t('front', $location->getProjectStatus())  ?></p>
                                </div>
                                <div class="results__info-block">
                                    <p class="results__info-title"><?= \Yii::t('front', 'Общая стоимость') ?>, тг</p>
                                    <p class="results__info-txt"><?= number_format($location->price, 0, '', ' ') ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>

        <?php if (count($paginator->getRange()) > 1): ?>
            <div class="container-main">
                <div class="social__pagination">
                    <span class="pagination__title"><?= \Yii::t('front', 'Страницы').':' ?></span>
                    <ul class="pagination__list">
                        <?php foreach ($paginator->getRange() as $page): ?>
                            <?php $active = $page === $paginator->getPage() ? ' active' : '' ?>
                            <li class="<?= 'pagination__item'.$active ?>">
                                <a href="<?= Url::current(['page' => $page]) ?>"><?= $page ?></a>
                            </li>
                        <?php endforeach; ?>
                        <?php if ($paginator->hasTail()): ?>
                            <li class="pagination__item">
                                <p>...</p>
                            </li>
                            <li class="pagination__item">
                                <a href="<?= Url::current(['page' => $page]) ?>"><?= $paginator->getTail() ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
    </div>
</main>
