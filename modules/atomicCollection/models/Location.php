<?php

namespace app\modules\collection\models;

use app\modules\buildingProgress\models\BuildingProgress;
use yii\helpers\Url;
use Yii;

/**
 * This is the model class for table "location_collection".
 *
 * @property integer $id
 * @property string $uuid
 * @property string $project
 * @property integer $floor
 * @property double $area
 * @property string $locationtype
 * @property integer $room_number
 * @property string $locationstatus
 */
class Location extends \yii\db\ActiveRecord
{
    // unset temporary
    const FINISH_TYPES_RU = [
//        1 => 'Чистовая',
//        'Предчистовая',
//        'Черновая'
    ];

    // unset temporary
    const FINISH_TYPES_KZ = [
//        1 => 'Чистовая',
//        'Предчистовая',
//        'Черновая'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_number', 'blocked', 'price', 'priceformetre', 'room_number', 'image_saved', 'sold'], 'integer'],
            [['area', 'arealife', 'heightarea'], 'number'],
            [['uuid', 'project', 'locationtype', 'locationstatus', 'finish', 'file', 'file_path'], 'string', 'max' => 256],
            [['uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'UUID',
            'project' => 'ЖК',
            'area' => 'Площадь',
            'arealife' => 'Жилая площадь',
            'locationtype' => 'Тип помещения',
            'room_number' => 'Количество комнат',
            'locationstatus' => 'Статус помещения',
            'priceformetre' => 'Цена за кв м',
            'heightarea' => 'Высота потолков',
            'finish' => 'Отделка',
            'file' => 'URL Изображения планировки',
            'blocked' => 'Активность',
            'file_path' => 'Изображение планировки',
            'image_saved' => 'Статус обработки изображения планировки',
            'sold' => 'Статус продажи квартиры'
        ];
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\query\LocationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\collection\models\query\LocationQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getProjectName() : string
    {
        $project = Project::find()->where(['uuid' => $this->project])->one();

        if ($project) {
            return trim(mb_ereg_replace('ЖК', '', $project->name));
        }

        return '';
    }

    /**
     * @return string
     */
    public function getStatus() : string
    {
        $status = LocationStatus::find()->where(['uuid' => $this->locationstatus])->one();

        if ($status) {
            return $status->name;
        }

        return '';
    }

    /**
     * @return array
     */
    public function fields() : array
    {
        $fields = parent::fields();
        unset($fields['id']);
        return $fields;
    }

    /**
     * @return string[]
     */
    public function getProjectData() : array
    {
        $project = Project::find()->where(['uuid' => $this->project])->one();
        $projectName = trim(mb_ereg_replace('ЖК', '', $project->name));

        $projectInDb = null;

        if ($project) {
            $projectInDb = \app\modules\project\models\Project::find()->where(['name' => $projectName])->one();
        }

        $result = [
            'status' => '',
            'lat' => '',
            'lon' => '',
            'background_image' => '',
            'scheme_image' => '',
            'address_image' => '',
            'link' => '',
            'address' => '',
            'logo' => '',
            'image' => '',
            'desc' => ''
        ];

        if ($projectInDb) {
            $result['status'] = $projectInDb->status->status ?? '';
            $result['lat'] = $projectInDb->lat ?? '';
            $result['lon'] = $projectInDb->lon ?? '';
            $result['background_image'] = $projectInDb->getBackgroundImagePath() ?? '';
            $result['scheme_image'] = $projectInDb->getSchemeImageThumbUrl() ?? '';
            $result['address_image'] = $projectInDb->getAddressImageThumbUrl() ?? '';
            $result['link'] = $projectInDb->url ?? '#';
            $result['address'] = $projectInDb->translation->address ?? '';
            $result['logo'] = $projectInDb->getColorLogoUrl() ?? '';
            $result['image'] = $projectInDb->getThumbUploadUrl('image', 'thumb_location') ??
                $projectInDb->getUploadUrl('image')
                ?? ''
            ;
            $result['desc'] = $projectInDb->translation->description ?? '';
        }

        return $result;
    }

    /**
     * @return string|null
     */
    public function getPlanImagePath() : ?string
    {

        $params = Array(
            'filter' => array('uuid'=>$this->uuid),
        );

        $ch = curl_init('https://cms.abpx.kz/api/collections/get/exin_plans_test?token=account-98a82c65cb0b513d2be55d1b56b599');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $responce = curl_exec($ch);

        curl_close($ch);

        $responce = json_decode($responce, true);

        $path = isset($responce['entries'][0]['plan']['path']) ? $responce['entries'][0]['plan']['path'] : '';

        if (!empty($path)) {
            $path = "http://cms.abpx.kz".$path;
            return $path;
        } else {
            return Url::to('@web/media/location_collection/'.$this->uuid.'/'.$this->file_path);
        }

        return false;

    }

    /**
     * @return string
     */
    public function getProjectStatus() : string
    {
        $project = Project::find()->where(['uuid' => $this->project])->one();
        $projectName = trim(mb_ereg_replace('ЖК', '', $project->name));

        if ($project) {
            $projectInDb = \app\modules\project\models\Project::find()->where(['name' => $projectName])->one();

            return  $projectInDb->status->status;
        }

        return '';
    }
}
