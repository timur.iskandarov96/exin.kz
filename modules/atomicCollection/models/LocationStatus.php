<?php

namespace app\modules\collection\models;

use Yii;

/**
 * @deprecated
 * This is the model class for table "location_status_collection".
 *
 * @property integer $id
 * @property string $uuid
 * @property string $name
 */
class LocationStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_status_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blocked'], 'integer'],
            [['uuid', 'name'], 'string', 'max' => 256],
            [['uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'UUID',
            'name' => 'Название',
            'blocked' => 'Активность'
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\collection\models\query\LocationStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\collection\models\query\LocationStatusQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function fields() : array
    {
        $fields = parent::fields();
        unset($fields['id']);
        return $fields;
    }
}
