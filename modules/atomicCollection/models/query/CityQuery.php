<?php

namespace app\modules\collection\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\collection\models\City]].
 *
 * @see \app\modules\collection\models\City
 */
class CityQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['blocked' => 0]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\City[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\City|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
