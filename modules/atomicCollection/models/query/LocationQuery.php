<?php

namespace app\modules\atomicCollection\models\query;

class LocationQuery
{
    const LIMIT = 6;
    const MILLION = 1000000;

    public function allLocations() 
    {
        $params = Array(
            // 'filter' => array('uuid'=>$this->uuid),
            "type"=> "getLocationPlans", 
            "filters"=> array(  
              array (
                "preoperator"=> "AND", 
                "attribute_name"=> "priceFrom", 
                "predicate"=> "=", 
                "value"=> "0", 
                "postoperator"=> "" 
              ), 
              array (
                "preoperator"=> "AND", 
                "attribute_name"=> "priceTo", 
                "predicate"=> "=", 
                "value"=> "100000000000", 
                "postoperator"=> "" 
              )
            ) 
        );

        $ch = curl_init('https://crm.ex-group.kz:5011/locations');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $responce = curl_exec($ch);

        curl_close($ch);

        return $responce = json_decode($responce, true);
    }

    public function searchByParams(array $params) 
    {
        $filter = Array(
            // 'filter' => array('uuid'=>$this->uuid),
            "type"=> "getLocationPlans", 
            "filters"=> array(  
              array (
                "preoperator"=> "AND", 
                "attribute_name"=> "priceFrom", 
                "predicate"=> "=", 
                "value"=> "0", 
                "postoperator"=> "" 
              ), 
              array (
                "preoperator"=> "AND", 
                "attribute_name"=> "priceTo", 
                "predicate"=> "=", 
                "value"=> "100000000000", 
                "postoperator"=> "" 
              )
            ) 
        );

        $ch = curl_init('https://crm.ex-group.kz:5011/locations');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($filter));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $responce = curl_exec($ch);

        curl_close($ch);

        $responce = json_decode($responce, true);
        return array(
            'locations' => self::locationsSlice($responce['data'], $params), 
            'countLocations' => $responce['data']
        );

    }

    public function locationsSlice($locations, $params)
    {

        $limit = isset($params['limit']) && $params['limit'] !== null ? $params['limit'] : self::LIMIT;
        $times = isset($params['page']) && $params['page'] !== null ? $params['page'] - 1 : 0;

        $offset = $times * $limit;
        return array_slice($locations, $offset, $limit, false);

    }

    public function active()
    {
        return $this->andWhere(['{{location_collection}}.blocked' => 0]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }

    public static function search(array $params) : array
    {
        $query = self::searchByParams($params);

        $limit = isset($params['limit']) && $params['limit'] !== null ? $params['limit'] : self::LIMIT;
        $times = isset($params['page']) && $params['page'] !== null ? $params['page'] - 1 : 0;

        $offset = $times * $limit;

        return $query
            ->limit($limit)
            ->offset($offset)
            ->active()
            ->all();
    }

    public static function searchCount(array $params) : int
    {
        $query = self::searchByParams($params);

        return $query->active()->count();
    }

    public static function getMinPrice()
    {
        return floor(Location::find()->min('price') / LocationQuery::MILLION);
    }

    private static function searchByParamss(array $params) : LocationQuery
    {
        $query = Location::find();
        $currentlyMinPrice = LocationQuery::getMinPrice();

        if (isset($params['room_number']) && $params['room_number']) {
            $roomNumberAsArr = explode('-', $params['room_number']);
            $roomNumberAsArr = array_map(function($item) {
                return (int) $item;
            }, $roomNumberAsArr);

            $query->andFilterWhere(['IN', 'room_number', $roomNumberAsArr]);
        }

        if (isset($params['price_max']) && $params['price_max']) {
            $priceMax = $currentlyMinPrice > (int) $params['price_max'] ?
                $currentlyMinPrice :
                (int) $params['price_max']
            ;
            $query->andFilterWhere(['<=', 'price', $priceMax * self::MILLION]);
        }

        if (isset($params['project']) && $params['project'] !== null) {
            $query->andFilterWhere(['project' => $params['project']]);
        }

        if (isset($params['district']) && $params['district']) {
            $query->leftJoin(
                'projects_collection',
                '{{projects_collection}}.uuid = {{location_collection}}.project'
            )->andWhere(['{{projects_collection}}.district' => $params['district']]);
        }

        if (isset($params['price_min']) && $params['price_min']) {
            $query->andFilterWhere(['>=', 'price', (int) $params['price_min'] * self::MILLION]);
        }

        if (isset($params['area_max']) && $params['area_max']) {
            $query->andFilterWhere(['<=', 'area', (int) $params['area_max']]);
        }

        if (isset($params['area_min']) && $params['area_min']) {
            $query->andFilterWhere(['>=', 'area', (int) $params['area_min']]);
        }

        if (isset($params['location_type']) && $params['location_type']) {
            $query->andFilterWhere(['locationtype' => $params['location_type']]);
        }

        if (isset($params['finish_type']) && $params['finish_type']) {
            $finishType = Location::FINISH_TYPES[$params['finish_type']];
            $query->andFilterWhere(['like', 'area', $finishType]);
        }

        return $query;
    }
}
