<?php

namespace app\modules\collection\models\query;

/**
 * @deprecated
 * This is the ActiveQuery class for [[\app\modules\collection\models\Plan]].
 *
 * @see \app\modules\collection\models\Plan
 */
class PlanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\Plan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\Plan|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
