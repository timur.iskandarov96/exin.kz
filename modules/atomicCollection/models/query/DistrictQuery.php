<?php

namespace app\modules\collection\models\query;

use app\modules\collection\models\District;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[\app\modules\collection\models\District]].
 *
 * @see \app\modules\collection\models\District
 */
class DistrictQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['blocked' => 0]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\District[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\District|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public static function getCollection() : array
    {
        return ArrayHelper::map(District::find()->all(), 'uuid', 'name');
    }
}
