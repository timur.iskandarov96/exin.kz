<?php

namespace app\modules\collection\models\query;

use app\modules\collection\models\Project;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[\app\modules\collection\models\Project]].
 *
 * @see \app\modules\collection\models\Project
 */
class ProjectQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['blocked' => 0]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\Project[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\Project|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return array
     */
    public static function getCollection() : array
    {
        $hasNoLocations = ArrayHelper::map(
            \app\modules\project\models\Project::find()->where(['no_locations' => true])->all(),
        'id',
        'name'
        );

        $projectsFromApi = ArrayHelper::map(
            Project::find()->all(),
            'uuid',
            'name'
        );

        foreach ($projectsFromApi as $key => $project) {
            $projectName =  trim(mb_ereg_replace('ЖК', '', $project));

            if (in_array($projectName, $hasNoLocations)) {
                unset($projectsFromApi[$key]);
            }
        }

        return $projectsFromApi;
    }
}
