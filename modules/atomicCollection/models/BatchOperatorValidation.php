<?php


namespace app\modules\collection\models;


/**
 * Class BatchOperatorValidation
 * @package app\modules\collection\models
 */
class BatchOperatorValidation
{
    /**
     * @param array $data
     * @return array|bool[]
     */
    public static function validateInsert(array $data, array $attributes) : array
    {
        if (count($data) > BatchOperator::LIMIT) {
            return [
                'result' => false,
                'message' => 'Total number of elements MUST NOT exceed 1000!'
            ];
        }

        if (count($attributes) !== count($data[0])) {
            return [
                'result' => false,
                'message' => 'Attributes number mismatch. Correct order of arguments: '.implode(', ', $attributes)
            ];
        }

        foreach ($data as $datum) {
            if ($attributes !== array_keys($datum)) {
                return [
                    'result' => false,
                    'message' => 'Attributes and arguments mismatch. Correct order of arguments: '.implode(', ', $attributes)
                ];
            }
        }

        return ['result' => true];
    }

    /**
     * @param array $data
     * @return array|bool
     */
    public static function validateUpdate(array $data) : array
    {
        if (count($data) > BatchOperator::LIMIT) {
            return [
                'result' => false,
                'message' => 'Total number of elements MUST NOT exceed 1000!'
            ];
        }

        return ['result' => true];
    }
}
