<?php

namespace app\modules\collection\models\search;

use app\modules\buildingProgress\models\BuildingProgress;
use app\modules\collection\models\Location;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class LocationSearch extends Location
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['floor', 'room_number'], 'integer'],
            [['area'], 'number'],
            [['uuid', 'project', 'locationtype', 'locationstatus', 'plan_ru', 'plan_kk'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Location::find();
        $query->leftJoin(
            'location_status_collection',
            '{{location_collection}}.locationstatus = {{location_status_collection}}.uuid'
        );
        $query->leftJoin(
            'location_type_collection',
            '{{location_collection}}.locationtype = {{location_type_collection}}.uuid'
        );

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ]
        ]);

        //$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // filtering conditions
        $query->andFilterWhere([
            'area' => $params['area'],
            'floor' => $params['floor'],
            'room_number' => $params['room_number'],
        ]);

        return $dataProvider;
    }
}