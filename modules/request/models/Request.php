<?php

namespace app\modules\request\models;

use app\components\amocrm\AmoCrmService;
use app\components\sendpulse\EmailSender;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "requests".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property integer $housing_estate_id
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_campaign
 * @property string $utm_term
 * @property string $utm_content
 * @property string $user_agent
 * @property string $ga
 * @property string $created_at
 * @property string $updated_at
 *
 * @property HousingEstate $housingEstate
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['housing_estate_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'phone'], 'string', 'max' => 255],
//            [
//                'phone',
//                'match',
//                'pattern' => '/^(\+7)+\s+(\d{3})+\s+(\d{3})+\s+(\d{2})+\s+(\d{2})$/',
//                'message' => 'Не корректный номер телефона'
//            ],
            [
                ['housing_estate_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => HousingEstate::class,
                'targetAttribute' => ['housing_estate_id' => 'id']
            ],
            [['utm_source','utm_medium','utm_campaign','utm_term','utm_content','user_agent','ga'], 'default'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'housing_estate_id' => 'Выбранный ЖК',
            'utm_source' => 'utm_source',
            'utm_medium' => 'utm_medium',
            'utm_campaign' => 'utm_campaign',
            'utm_term' => 'utm_term',
            'utm_content' => 'utm_content',
            'user_agent' => 'user_agent',
            'ga' => 'ga',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

//        $amoCrmEnumId = null;
//
//        $amoCrmEnum = (new \yii\db\Query())
//            ->select('amo_id')
//            ->from('amo_housing_enums')
//            ->where(['housing_estate_id' => $this->housingEstate->id])
//            ->one();
//
//        if ($amoCrmEnum) {
//            $amoCrmEnumId = (int) $amoCrmEnum['amo_id'];
//        }
//
//        (new AmoCrmService())->createUnsorted($this->name, $this->phone, $amoCrmEnumId);
//        $this->notifyAdmin();
        try {
            \Yii::$app->db->createCommand()->insert('request_tasks', [
                'name' => $this->name,
                'phone' => $this->phone,
                'housing_estate_id' => $this->housing_estate_id,
                'utm_source' => $this->utm_source,
                'utm_medium' => $this->utm_medium,
                'utm_campaign' => $this->utm_campaign,
                'utm_term' => $this->utm_term,
                'utm_content' => $this->utm_content,
                'user_agent' => $this->user_agent,
                'ga' => $this->ga,
            ])->execute();
        } catch (\Exception $e) {
            \Yii::error('afterSave Request event ERROR.');
            \Yii::error($e->getMessage());
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHousingEstate()
    {
        return $this->hasOne(HousingEstate::class, ['id' => 'housing_estate_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\request\models\query\RequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\request\models\query\RequestQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function getHousingEstates() : array
    {
        return ArrayHelper::map(
            HousingEstate::find()->all(),
            'id',
            'name'
        );
    }

    /**
     * @param int $housingEstateId
     * @return string
     */
    public function getHousingEstateName(int $housingEstateId) : string
    {
        return HousingEstate::findOne($housingEstateId)->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone) : void
    {
        $this->phone = $phone;
    }

    /**
     * @param int $id
     */
    public function setHousingEstateId(int $id) : void
    {
        $this->housing_estate_id = $id;
    }

    /**
     * @param string $utm_source
     */
    public function setSource(string $utm_source) : void
    {
        $this->utm_source = $utm_source;
    }

    /**
     * @param string $utm_medium
     */
    public function setMedium(string $utm_medium) : void
    {
        $this->utm_medium = $utm_medium;
    }

    /**
     * @param string $utm_campaign
     */
    public function setCampaign(string $utm_campaign) : void
    {
        $this->utm_campaign = $utm_campaign;
    }

    /**
     * @param string $utm_term
     */
    public function setTerm(string $utm_term) : void
    {
        $this->utm_term = $utm_term;
    }

    /**
     * @param string $utm_content
     */
    public function setContent(string $utm_content) : void
    {
        $this->utm_content = $utm_content;
    }

    /**
     * @param string $user_agent
     */
    public function setAgent(string $user_agent) : void
    {
        $this->user_agent = $user_agent;
    }

    /**
     * @param string $ga
     */
    public function setGa(string $ga) : void
    {
        $this->ga = $ga;
    }

    /**
     * @deprecated
     */
    private function notifyAdmin()
    {
        $name = $this->name;
        $phone = $this->phone;
        $housingEstate = $this->housingEstate->name;

        $htmlMsg = "<p>Имя: $name<p>";
        $htmlMsg .= "<p>Телефон: $phone</p>";
        $htmlMsg .= "<p>ЖК: $housingEstate</p>";

        $textMsg = "Имя: $name" . PHP_EOL;
        $textMsg .= "Телефон: $phone" . PHP_EOL;
        $textMsg .= "ЖК: $housingEstate" . PHP_EOL;

        try {
            (new EmailSender())->sendEmail($htmlMsg, $textMsg);
        } catch (\Exception $e) {
            Yii::error('Sendpulse email error');
        }
    }

}
