<?php

namespace app\modules\request\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\request\models\Request]].
 *
 * @see \app\modules\request\models\Request
 */
class RequestQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\request\models\Request[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\request\models\Request|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
