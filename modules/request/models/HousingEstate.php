<?php

namespace app\modules\request\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "housing_estates".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Requests[] $requests
 */
class HousingEstate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'housing_estates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Requests::className(), ['housing_estate_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\request\models\query\HousingEstateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\request\models\query\HousingEstateQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
