<?php

namespace app\modules\request\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * RequestController implements the CRUD actions for [[app\modules\request\models\Request]] model.
 * @see app\modules\request\models\Request
 */
class RequestController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\request\models\Request';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\request\models\search\RequestSearch';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\request\models\Request',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }
}
