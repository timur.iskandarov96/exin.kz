<?php

namespace app\modules\request\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * HousingEstateController implements the CRUD actions for [[app\modules\request\models\HousingEstate]] model.
 * @see app\modules\request\models\HousingEstate
 */
class HousingEstateController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\request\models\HousingEstate';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\request\models\search\HousingEstateSearch';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\request\models\HousingEstate',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
