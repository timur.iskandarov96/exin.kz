<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\request\models\search\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        'phone',
        [
            'attribute' => 'housing_estate_id',
            'value' => function($data) {
                if ($data->housing_estate_id) {
                    return $data->getHousingEstateName($data->housing_estate_id);
                }
            },
            'filter' => $searchModel->getHousingEstates()
        ],
        [
            'attribute' => 'created_at',
            'filter' => DatePicker::widget([
                'attribute' => 'created_at',
                'language' => 'ru',
                'model' => $searchModel,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]),
            'value' => function ($data) {
                if ($data->created_at) {
                    return \Yii::$app->formatter->asDate($data->created_at, 'dd.MM.yyyy H:mm:ss');
                } else {
                    return 'Не задано';
                }
            }
        ],
        // 'updated_at',
        [
            'class' => ActionColumn::class,
            'template' => '{view}'
        ],
    ],
]); ?>
