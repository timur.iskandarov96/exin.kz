<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\request\models\Request */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'phone',
            [
                'attribute' => 'housing_estate_id',
                'value' => function($data) {
                    if ($data->housing_estate_id) {
                        return $data->getHousingEstateName($data->housing_estate_id);
                    }
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>
    </div>
</div>