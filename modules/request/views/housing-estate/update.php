<?php

/* @var $this yii\web\View */
/* @var $model app\modules\request\models\HousingEstate */

$this->title = 'Редактировать ЖК: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Housing Estates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


