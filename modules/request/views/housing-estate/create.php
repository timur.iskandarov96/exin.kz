<?php

/* @var $this yii\web\View */
/* @var $model app\modules\request\models\HousingEstate */

$this->title = 'Создать ЖК';
$this->params['breadcrumbs'][] = ['label' => 'Housing Estates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

