<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;
use yii\helpers\Html as YiiHtml;

/* @var $this yii\web\View */
/* @var $model app\modules\partner\models\Partner */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Партнеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'is_active',
                'value' => function($data) {
                    if ($data->is_active) {
                        return $data->is_active === 1 ? 'Да' : 'Нет';
                    }
                    return null;
                }
            ],
            'position',
            [
                'attribute' => 'image',
                'value' => function ($model) {
                    return YiiHtml::img($model->getUploadUrl('image'), [
                            'width' => 200
                    ]);
                },
                'format' => 'raw'
            ],
            'web_site',
        ],
    ]) ?>
    </div>
</div>