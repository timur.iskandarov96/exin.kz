<?php

/* @var $this yii\web\View */
/* @var $model app\modules\partner\models\Partner */

$this->title = 'Редактировать запись: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Партнеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


