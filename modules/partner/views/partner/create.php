<?php

/* @var $this yii\web\View */
/* @var $model app\modules\partner\models\Partner */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Партнеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

