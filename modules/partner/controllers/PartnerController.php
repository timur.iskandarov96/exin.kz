<?php

namespace app\modules\partner\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * PartnerController implements the CRUD actions for [[app\modules\partner\models\Partner]] model.
 * @see app\modules\partner\models\Partner
 */
class PartnerController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\partner\models\Partner';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\partner\models\search\PartnerSearch';

    /**
    * @inheritdoc
    */
    public $createScenario = 'create';

    /**
    * @inheritdoc
    */
    public $updateScenario = 'update';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\partner\models\Partner',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
