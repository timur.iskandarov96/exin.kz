<?php

namespace app\modules\partner\models;

use Imagine\Image\ManipulatorInterface;
use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property integer $id
 * @property integer $is_active
 * @property integer $position
 * @property string $image
 * @property string $web_site
 *
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition($position) Moves owner record to the specific position.
 * @method bool getIsFirst() Checks whether this record is the first in the list.
 * @method bool getIsLast() Checks whether this record is the the last in the list.
 * @method \BaseActiveRecord|static|null findPrev() Finds record previous to this one.
 * @method \BaseActiveRecord|static|null findNext() Finds record next to this one.
 * @method \BaseActiveRecord|static|null findFirst() Finds the first record in the list.
 * @method \BaseActiveRecord|static|null findLast() Finds the last record in the list.
 * @method mixed beforeInsert($event) Handles owner 'beforeInsert' owner event, preparing its positioning.
 * @method mixed beforeUpdate($event) Handles owner 'beforeInsert' owner event, preparing its possible re-positioning.
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class Partner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'position'], 'integer'],
            [['web_site'], 'safe'],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активность',
            'position' => 'Порядок вывода',
            'image' => 'Изображение',
            'web_site' => 'Ссылка'
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\partner\models\query\PartnerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\partner\models\query\PartnerQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image',
                'path' => '@webroot/media/partners/{id}',
                'url' => '@web/media/partners/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => [
                    'thumb' => [
                        'width' => 286,
                        'height' => 205,
                        'quality' => 85,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'preview' => [
                        'width' => 50,
                        'height' => 50,
                        'quality' => 80,
                        'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ]
                ],
            ],
        ];
    }
}
