<?php

namespace app\modules\partner\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\partner\models\Partner]].
 *
 * @see \app\modules\partner\models\Partner
 */
class PartnerQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\partner\models\Partner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\partner\models\Partner|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
