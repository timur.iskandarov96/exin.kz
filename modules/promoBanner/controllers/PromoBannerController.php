<?php

namespace app\modules\promoBanner\controllers;

use app\modules\promoBanner\models\PromoBanner;
use app\modules\promoBanner\models\search\PromoBannerSearch;
use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * PromoBannerController implements the CRUD actions for [[app\modules\promoBanner\models\PromoBanner]] model.
 * @see \app\modules\promoBanner\models\PromoBanner
 */
class PromoBannerController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = PromoBanner::class;
    /**
     * @inheritdoc
     */
    public $searchModelClass = PromoBannerSearch::class;

    /**
    * @inheritdoc
    */
    public $createScenario = 'create';

    /**
    * @inheritdoc
    */
    public $updateScenario = 'update';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\promoBanner\models\PromoBanner',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
