<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\promoBanner\models\PromoBanner
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']promo_banner_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']title')->richTextArea(['preset' => 'full','options' => ['rows' => 6]]) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']description')->richTextArea(['options' => ['rows' => 6]]) ?>
