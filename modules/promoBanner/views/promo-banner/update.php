<?php

/* @var $this yii\web\View */
/* @var $model app\modules\promoBanner\models\PromoBanner */

$this->title = 'Редактировать запись: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Акционнеы баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


