<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\promoBanner\models\PromoBanner */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->field($model, 'is_active')->switchInput() ?>

        <?= $form->field($model, 'image')->imageUpload() ?>
        <?= $form->field($model, 'img_xs_ru')->imageUpload() ?>
        <?= $form->field($model, 'img_md_ru')->imageUpload() ?>
        <?= $form->field($model, 'img_vertical_ru')->imageUpload() ?>

        <?= $form->field($model, 'image_kz')->imageUpload() ?>
        <?= $form->field($model, 'img_xs_kz')->imageUpload() ?>
        <?= $form->field($model, 'img_md_kz')->imageUpload() ?>
        <?= $form->field($model, 'img_vertical_kz')->imageUpload() ?>

    </div>

    <div class="col-md-4">

    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
