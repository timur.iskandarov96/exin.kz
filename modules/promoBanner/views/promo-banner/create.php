<?php

/* @var $this yii\web\View */
/* @var $model app\modules\promoBanner\models\PromoBanner */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Акционные баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

