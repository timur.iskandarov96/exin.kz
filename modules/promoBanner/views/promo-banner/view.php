<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\promoBanner\models\PromoBanner */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Акционные баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'is_active',
            'position',
            'image',
            'created_at',
            'updated_at',
        ],
    ]) ?>
    </div>
</div>