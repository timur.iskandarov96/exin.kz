<?php

namespace app\modules\promoBanner\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\promoBanner\models\PromoBanner]].
 *
 * @see \app\modules\promoBanner\models\PromoBanner
 */
class PromoBannerQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\promoBanner\models\PromoBanner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\promoBanner\models\PromoBanner|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
