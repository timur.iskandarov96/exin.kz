<?php

namespace app\modules\promoBanner\models;

use Yii;

/**
 * This is the model class for table "promo_banners_translation".
 *
 * @property integer $id
 * @property integer $promo_banner_id
 * @property string $lang
 * @property string $title
 * @property string $description
 *
 * @property PromoBanner $promoBanner
 */
class PromoBannerTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_banners_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promo_banner_id'], 'integer'],
            [['description'], 'string'],
            [['lang', 'title'], 'string', 'max' => 255],
            [['promo_banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoBanner::class, 'targetAttribute' => ['promo_banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_banner_id' => 'Промо баннер',
            'lang' => 'Язык перевода',
            'title' => 'Заголовок',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoBanner()
    {
        return $this->hasOne(PromoBanner::class, ['id' => 'promo_banner_id']);
    }

}
