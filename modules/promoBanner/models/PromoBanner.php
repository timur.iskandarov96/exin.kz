<?php

namespace app\modules\promoBanner\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "promo_banners".
 *
 * @property integer $id
 * @property integer $is_active
 * @property integer $position
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PromoBannerTranslation[] $promoBannersTranslations
 *
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition($position) Moves owner record to the specific position.
 * @method bool getIsFirst() Checks whether this record is the first in the list.
 * @method bool getIsLast() Checks whether this record is the the last in the list.
 * @method \BaseActiveRecord|static|null findPrev() Finds record previous to this one.
 * @method \BaseActiveRecord|static|null findNext() Finds record next to this one.
 * @method \BaseActiveRecord|static|null findFirst() Finds the first record in the list.
 * @method \BaseActiveRecord|static|null findLast() Finds the last record in the list.
 * @method mixed beforeInsert($event) Handles owner 'beforeInsert' owner event, preparing its positioning.
 * @method mixed beforeUpdate($event) Handles owner 'beforeInsert' owner event, preparing its possible re-positioning.
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class PromoBanner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'],'maxSize' => 1024 * 1024 * 1],
            [['img_xs_ru'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024 * 1],
            [['img_md_ru'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024 * 1],
            [['img_vertical_ru'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024 * 1],
            [['image_kz'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024 * 1],
            [['img_xs_kz'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024 * 1],
            [['img_md_kz'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024 * 1],
            [['img_vertical_kz'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg'], 'maxSize' => 1024 * 1024 * 1],
            [['image', 'img_xs_ru', 'img_md_ru', 'img_vertical_ru', 'image_kz', 'img_xs_kz', 'img_md_kz', 'img_vertical_kz'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активность',
            'position' => 'Порядок очередности',
            'image' => 'Изображение основное на русском языке',
            'img_xs_ru' => 'Маленькое изображение на русском языке',
            'img_md_ru' => 'Среднее изображение на русском языке',
            'img_vertical_ru' => 'Вертикальное изображение на русском языке',
            'image_kz' => 'Изображение основное на казахском языке',
            'img_xs_kz' => 'Маленькое изображение на казахском языке',
            'img_md_kz' => 'Среднее изображение на казахском языке',
            'img_vertical_kz' => 'Вертикальное изображение на казахском языке',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoBannersTranslations()
    {
        return $this->hasMany(PromoBannerTranslation::class, ['promo_banner_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\promoBanner\models\query\PromoBannerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\promoBanner\models\query\PromoBannerQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image',
                'path' => '@webroot/media/promo_banners/image/{id}',
                'url' => '@web/media/promo_banners/image/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'img_xs_ruUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'img_xs_ru',
                'path' => '@webroot/media/promo_banners/img_xs_ru/{id}',
                'url' => '@web/media/promo_banners/img_xs_ru/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'img_md_ruUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'img_md_ru',
                'path' => '@webroot/media/promo_banners/img_md_ru/{id}',
                'url' => '@web/media/promo_banners/img_md_ru/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'img_vertical_ruUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'img_vertical_ru',
                'path' => '@webroot/media/promo_banners/img_vertical_ru/{id}',
                'url' => '@web/media/promo_banners/img_vertical_ru/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'image_kzUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'image_kz',
                'path' => '@webroot/media/promo_banners/image_kz/{id}',
                'url' => '@web/media/promo_banners/image_kz/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'img_xs_kzUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'img_xs_kz',
                'path' => '@webroot/media/promo_banners/img_xs_kz/{id}',
                'url' => '@web/media/promo_banners/img_xs_kz/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'img_md_kzUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'img_md_kz',
                'path' => '@webroot/media/promo_banners/img_md_kz/{id}',
                'url' => '@web/media/promo_banners/img_md_kz/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ],
            'img_vertical_kzUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'attribute' => 'img_vertical_kz',
                'path' => '@webroot/media/promo_banners/img_vertical_kz/{id}',
                'url' => '@web/media/promo_banners/img_vertical_kz/{id}',
                'scenarios' => ['create', 'update'],
                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
            ]
//            'translation' => [
//                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
//                'translationModelClass' => PromoBannerTranslation::class,
//                'translationModelRelationColumn' => 'promo_banner_id'
//            ]
        ];
    }

    public function getImagePath(string $imageType)
    {
        return Url::to('@web/media/promo_banners/'.$imageType.'/'.$this->id.'/'.$this->$imageType);
    }
}
