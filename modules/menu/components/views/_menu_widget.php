<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $menu array */
?>

<nav class="header__links-block">
    <ul class="header__links">
        <?php foreach ($menu as $item): ?>
            <?php $active = ''; ?>
            <?php if ($item['url'] === \Yii::$app->request->getUrl()): ?>
                <?php $active = ' active'; ?>
            <?php endif; ?>
            <li>
                <a href="<?= $item['url'] ?>" class="<?= 'header__link' . $active ?>"><?= $item['name'] ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
    <a href="<?= Url::to('/favorites') ?>" class="header__favorites only-menu">
        <?php
        $cookies = \Yii::$app->request->cookies;
        if (isset($cookies['locations']) && count($cookies) > 0):
            ?>
            <?php $locationsCount = count(\yii\helpers\Json::decode($cookies['locations']->value)); ?>
            <div class="header__favorites-icon header__favorites-icon--count">
                <span><?= $locationsCount ?></span>
            </div>
        <?php else: ?>
            <div class="header__favorites-icon">
                <span></span>
            </div>
        <?php endif; ?>
        <p><?= \Yii::t('front', 'Избранное') ?></p>
    </a>
    <a href="#" class="header__bid only-menu" data-modal-btn="modal-question">
        <div class="header__bid-icon">
            <svg class="svg-icon svg-icon-tel" width="25" height="25" viewBox="0 0 19 19" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.965 14.6105L14.792 13.4375C14.206 12.8515 13.256 12.8515 12.671 13.4375L11.749 14.3595C11.543 14.5655 11.231 14.6345 10.965 14.5175C9.62902 13.9325 8.30902 13.0455 7.13202 11.8685C5.96002 10.6965 5.07602 9.38246 4.49002 8.05146C4.36802 7.77646 4.43902 7.45346 4.65202 7.24046L5.47802 6.41446C6.14902 5.74346 6.14902 4.79446 5.56302 4.20846L4.39002 3.03546C3.60902 2.25446 2.34302 2.25446 1.56202 3.03546L0.910025 3.68646C0.169025 4.42746 -0.139975 5.49646 0.0600247 6.55646C0.554025 9.16946 2.07202 12.0305 4.52102 14.4795C6.97002 16.9285 9.83103 18.4465 12.444 18.9405C13.504 19.1405 14.573 18.8315 15.314 18.0905L15.965 17.4395C16.746 16.6585 16.746 15.3925 15.965 14.6105Z"/>
                <path class="stroke" d="M10 4.99134C11.031 4.97734 12.067 5.35934 12.854 6.14634" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path class="stroke" d="M15.682 3.31836C14.113 1.74936 12.056 0.964355 10 0.964355" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path class="stroke" d="M14.009 9.00024C14.023 7.96924 13.641 6.93324 12.854 6.14624" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path class="stroke" d="M15.682 3.31836C17.251 4.88736 18.036 6.94436 18.036 9.00036" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </div>
        <p><?= \Yii::t('front', 'Заказать звонок') ?></p>
    </a>
    <div class="header__langs-mob only-menu">
        <?php foreach (\Yii::$app->urlManager->languages as $label => $code): ?>
            <a href="<?= Url::to(ArrayHelper::merge(['', 'language' => $code], \Yii::$app->request->get())) ?>"
                class="<?= \Yii::$app->language == $code ? 'active' : '' ?>"><?= Yii::t('front', $label) ?>
            </a>
        <?php endforeach ?>
    </div>
</nav>

