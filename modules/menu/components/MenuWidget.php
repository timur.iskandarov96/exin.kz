<?php


namespace app\modules\menu\components;

use yii\base\Widget;
use yii\helpers\Url;

class MenuWidget extends Widget
{
    public $type;
    
    public function run()
    {
        $menu = [
            ['url' => Url::to(['/about']), 'name' => \Yii::t('front', 'О компании')],
            ['url' => Url::to(['/#project']), 'name' => \Yii::t('front', 'Проекты')],
            ['url' => Url::to(['/building-progress']), 'name' => \Yii::t('front', 'Ход строительства')],
            ['url' => Url::to(['/exclusive-social']), 'name' => \Yii::t('front', 'Exclusive Social')],
            ['url' => Url::to(['/our-team']), 'name' => \Yii::t('front', 'Наша команда')],
            ['url' => Url::to(['/news']), 'name' => \Yii::t('front', 'Новости')],
            ['url' => Url::to(['/contacts']), 'name' => \Yii::t('front', 'Контакты')],
        ];

        return $this->render('_menu_widget', ['menu' => $menu]);
    }
}
