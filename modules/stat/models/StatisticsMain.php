<?php

namespace app\modules\stat\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "statistics_main".
 *
 * @property integer $id
 * @property string $icon
 * @property integer $is_active
 *
 * @property StatisticsMainTranslation[] $statisticsMainTranslations
 */
class StatisticsMain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistics_main';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['icon'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg', 'svg'], 'maxSize' => 1024 * 1024 * 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'icon' => 'Иконка',
            'is_active' => 'Активность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatisticsMainTranslations()
    {
        return $this->hasMany(StatisticsMainTranslation::className(), ['stat_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\stat\models\query\StatisticsMainQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\stat\models\query\StatisticsMainQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'fileUpload' =>  [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadBehavior',
                'attribute' => 'icon',
                'path' => '@webroot/media/statistics-main/icons/{id}',
                'url' => '@web/media/statistics-main/icons/{id}',
                'scenarios' => ['create', 'update'],
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => StatisticsMainTranslation::class,
                'translationModelRelationColumn' => 'stat_id'
            ],
//            'iconUpload' => [
//                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
//                'attribute' => 'icon',
//                'path' => '@webroot/media/statistics-main/icons/{id}',
//                'url' => '@web/media/statistics-main/icons/{id}',
//                'scenarios' => ['create', 'update'],
//                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
//            ],
        ];
    }

    public function getLogoPath()
    {
        return Url::to('@web/media/statistics-main/icons/'.$this->id.'/'.$this->icon);
    }

}
