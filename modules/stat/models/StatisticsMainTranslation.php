<?php

namespace app\modules\stat\models;

use Yii;

/**
 * This is the model class for table "statistics_main_translation".
 *
 * @property integer $id
 * @property integer $stat_id
 * @property string $title
 * @property string $text
 *
 * @property StatisticsMain $stat
 */
class StatisticsMainTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistics_main_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stat_id'], 'integer'],
            [['title', 'text', 'lang'], 'string', 'max' => 255],
            [['stat_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatisticsMain::class, 'targetAttribute' => ['stat_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Язык перевода',
            'stat_id' => 'Блок статистики',
            'title' => 'Заголовок',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStat()
    {
        return $this->hasOne(StatisticsMain::class, ['id' => 'stat_id']);
    }

}
