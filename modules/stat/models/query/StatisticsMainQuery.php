<?php

namespace app\modules\stat\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\stat\models\StatisticsMain]].
 *
 * @see \app\modules\stat\models\StatisticsMain
 */
class StatisticsMainQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\stat\models\StatisticsMain[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\stat\models\StatisticsMain|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
