<?php

namespace app\modules\stat\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * StatisticsMainController implements the CRUD actions for [[app\modules\stat\models\StatisticsMain]] model.
 * @see app\modules\stat\models\StatisticsMain
 */
class StatisticsMainController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\stat\models\StatisticsMain';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\stat\models\search\StatisticsMainSearch';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\stat\models\StatisticsMain',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
