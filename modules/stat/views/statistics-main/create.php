<?php

/* @var $this yii\web\View */
/* @var $model app\modules\stat\models\StatisticsMain */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Статистика на главной', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

