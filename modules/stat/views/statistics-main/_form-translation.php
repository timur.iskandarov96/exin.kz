<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\stat\models\StatisticsMain
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']stat_id[]')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']title')->textInput() ?>
<?= $form->field($translationModel, '[' . $languageCode . ']text')->textInput() ?>

