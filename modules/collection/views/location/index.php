<?php

use yii\helpers\Html;
use yii\helpers\Url;

\app\assets\SearchResultAsset::register($this);

/* @var $locations \app\modules\collection\models\Location[] */
/* @var $projects_collection array */
/* @var $room_number string */
/* @var $price string */
/* @var $district string */
/* @var $paginator app\components\front\Paginator */
/* @var $current_limit int */
/* @var $count_locations int */

?>
<main class="js-header-size">
    <section class="filter pr-pages">
        <div class="container">
            <a href="<?= Url::home() ?>" class="link__back indent-block"><?= \Yii::t('front','Главная') ?></a>
            <h1 class="page__title indent-block"><?= \Yii::t('front', 'Результаты поиска') ?></h1>
            <div class="filter" id="filter"></div>
            <div class="results">
                <h3 class="results__title"><?= \Yii::t('front', 'Результаты') ?></h3>
                <div class="results__controls">
                    <div class="results__filter">
                        <p class="results__count"><?= \Yii::t('front', 'Всего').': ' ?><span><?= $count_locations ?></span></p>
                        <div class="header__lang results__filter-name">
                            <span><?= \Yii::t('front', 'Все ЖК') ?></span>
                            <div class="header__menu-wrap">
                                <div class="header__lang-menu">
                                    <?php foreach ($projects_collection as $uuid => $name): ?>
                                        <?php $name = trim(mb_ereg_replace('ЖК', '', $name)); ?>
                                        <?= Html::a($name, Url::to(
                                            [
                                                '/locations',
                                                'project' => $uuid,
                                                'room_number' => $room_number,
                                                'price' => $price,
                                                'district' => $district
                                            ]
                                        )) ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="results__pagination">

                        <?php if (\Yii::$app->language === 'ru-RU'): ?>
                            <span class="pagination__title"><?= \Yii::t('front', 'Показывать по').':' ?></span>
                        <?php endif; ?>
                        <ul class="pagination__list">
                            <?php foreach ([6,9,12] as $limit): ?>
                                <?php $active = $limit === (int)$current_limit ? ' active' : '' ?>
                                <li class="<?= 'pagination__item'.$active ?>">
                                    <a href="<?= Url::current(['limit' => $limit]) ?>"><?= $limit ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php if (\Yii::$app->language === 'kk-KZ'): ?>
                            <span class="pagination__title"><?= \Yii::t('front', 'Показывать по') ?></span>
                        <?php endif; ?>

                    </div>
                </div>
                <?php if (count($locations) === 0): ?>
                    <h4><?= \Yii::t('front', 'По заданным параметрам ничего не найдено') ?></h4>
                <?php endif; ?>

                <div class="results__list">

                    <?php foreach ($locations as $location): ?>

                        <div class="results__item <?php if ($location->sold) { echo "item-sold"; }?>">
                            <a href="<?= Url::to(['/locations/'.$location->uuid]) ?>"><p class="results__item-title"><?= $location->getProjectName() ?><?php if ($location->sold) { echo "<span class='sold-marker'>Нет в наличии</span>"; }?></p></a>
                            <div class="results__img">
                                <a href="<?= Url::to(['/locations/'.$location->uuid]) ?>"><?= Html::img($location->getPlanImagePath(), ['alt' => 'Схема кваритиры']) ?></a>
                            </div>
                            <div class="results__info">
                                <div class="results__info-col">
                                    <div class="results__subinfo">
                                        <div class="results__info-block">
                                            <p class="results__info-title"><?= \Yii::t('front', 'Комнат') ?></p>
                                            <p class="results__info-txt"><?= $location->room_number ?></p>
                                        </div>
                                    </div>
                                    <?php if ($location->finish): ?>
                                        <div class="results__info-block">
                                            <p class="results__info-title"><?= \Yii::t('front', 'Отделка') ?></p>
                                            <p class="results__info-txt"><?= \Yii::t('front', $location->finish) ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <div class="results__info-block">
                                        <p class="results__info-title"><?= \Yii::t('front', 'Цена за м²') ?></p>
                                        <p class="results__info-txt"><?= number_format($location->priceformetre, 0, ',', ' ') ?> тг</p>
                                    </div>
                                </div>
                                <div class="results__info-col">
                                    <div class="results__info-block">
                                        <p class="results__info-title"><?= \Yii::t('front', 'Площадь') ?>, м<span class="sup">2</span></p>
                                        <p class="results__info-txt"><?= $location->area ?></p>
                                    </div class="results__info-block">
                                    <div class="results__info-block">
                                        <p class="results__info-title"><?= \Yii::t('front', 'Статус строит-ва') ?></p>
                                        <p class="results__info-txt"><?= \Yii::t('front', $location->getProjectStatus())  ?></p>
                                    </div>
                                    <!--div class="results__info-block">
                                        <p class="results__info-title"><?= \Yii::t('front', 'Общая стоимость') ?>, тг</p>
                                        <p class="results__info-txt"><?= number_format($location->price, 0, '', ' ') ?></p>
                                    </div-->
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                </div>

                <?php if (count($paginator->getRange()) > 1): ?>
                    <div class="social__pagination">
                        <p><?= \Yii::t('front', 'Страницы').':' ?></p>
                        <ul class="pagination__list">
                            <?php foreach ($paginator->getRange() as $page): ?>
                                <?php $active = $page === $paginator->getPage() ? ' active' : '' ?>
                                <li class="<?= 'pagination__item'.$active ?>">
                                    <a href="<?= Url::current(['page' => $page]) ?>"><?= $page ?></a>
                                </li>
                            <?php endforeach; ?>
                            <?php if ($paginator->hasTail()): ?>
                                <li class="pagination__item">
                                    <p>...</p>
                                </li>
                                <li class="pagination__item">
                                    <a href="<?= Url::current(['page' => $paginator->getTail()]) ?>"><?= $paginator->getTail() ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
</main>