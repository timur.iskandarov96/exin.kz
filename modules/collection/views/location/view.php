<?php

\app\assets\ApartmentAsset::register($this);

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJsFile('https://api-maps.yandex.ru/2.1/?apikey=5e688e25-b597-4731-b0cf-064d2769a3c8&lang=ru_RU', [
    'position' => $this::POS_END,
]);

/* @var $model \app\modules\collection\models\Location */
/* @var blocks string */
/* @var floor string */
/* @var $count_similar int */
/* @var $price_min int */
/* @var $price_max int */
/* @var $area_min int */
/* @var $area_max int */
/* @var $is_in_favorites bool */

$projectData = $model->getProjectData();
$projectName = $model->getProjectName();

$this->registerJsFile('https://yastatic.net/share2/share.js', [
    'position' => $this::POS_END
]);

$shareBtn = "%3Csvg width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M14.6 2.9a2.7 2.7 0 01-4.554 1.963L5.303 7.28a2.702 2.702 0 010 1.44l4.743 2.417a2.7 2.7 0 11-.834 1.708l-5.05-2.575a2.7 2.7 0 110-4.54l5.05-2.575A2.7 2.7 0 1114.6 2.9z' fill='%237eb621'/%3E%3C/svg%3E";
$shareBtnHover = "%3Csvg width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M14.6 2.9a2.7 2.7 0 01-4.554 1.963L5.303 7.28a2.702 2.702 0 010 1.44l4.743 2.417a2.7 2.7 0 11-.834 1.708l-5.05-2.575a2.7 2.7 0 110-4.54l5.05-2.575A2.7 2.7 0 1114.6 2.9z' fill='%23fff'/%3E%3C/svg%3E";

$this->registerCss(
    <<<CSS
.ya-share2__icon_more {
    background-image: url("") !important;
}

.ya-share2__link:hover,
.ya-share2__icon_more:hover {
    background-image: url("") !important;
}

.apart__attributes.color .results__info-txt {
  color: #7eb621;
}

CSS

);

?>

<main class="apart js-header-size">
    <?php if ($projectData['background_image']): ?>
        <?php $backgroundUrl = "background-image: url('".$projectData['background_image']."');" ?>
    <?php else: ?>
        <?php $backgroundUrl = "background-image: url('images/apartament_bg.png')"; ?>
    <?php endif; ?>
    <div class="apart__bg" style="<?=$backgroundUrl?>"></div>
    <div class="apart__wrap">
        <div class="container-main">
            <div class="link__back-btn">
                <svg class="svg-icon svg-icon-arrow-back">
                    <use xlink:href="<?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#arrow-back') ?>"></use>
                </svg>
                <a href="javascript:history.back()"><?= \Yii::t('front', 'Назад') ?></a>
            </div>
            <div class="apart__content">
                <div class="apart__block">
                    <div class="apart__info">
                        <ul class="breadcrumbs">
                            <li class="breadcrumbs__item breadcrumbs--active">
                                <p><?= $projectName ?></p>
                            </li>
                            <li class="breadcrumbs__item">
                                <p><?= \Yii::t('front', $projectData['status']) ?></p>
                            </li>
                        </ul>
                        <div class="apart__subdesck">
                            <div>
                                <?php if ($model->locationtype === \app\modules\collection\models\LocationType::getLocationUUUID()): ?>
                                    <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                        <h2 class="apart__title"><?= $model->room_number ?>-комн. квартира, <span class="no-wrap"><?= $model->area ?> м²</span></h2>
                                    <?php else: ?>
                                        <h2 class="apart__title"><?= $model->room_number ?> бөлмелі пәтер, <span class="no-wrap"><?= $model->area?> м²</span></h2>
                                    <?php endif ?>
                                <?php else: ?>
                                    <h2 class="apart__title">
                                        <?= \Yii::t('front', 'Коммерческое помещение') ?>,
                                        <span class="no-wrap"><?= $model->area ?> м²</span>
                                    </h2>
                                <?php endif; ?>
                                <p class="apart__place"><?= $projectData['address'] ?></p>
                            </div>
                            <button class="apart__favorite">
                                <?php if ($is_in_favorites): ?>
                                    <svg stroke="#519A08" style="fill: #519A08; stroke: #519A08" class="svg-icon svg-icon-star">
                                        <use xlink:href="
                                                <?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#star') ?>"
                                        ></use>
                                    </svg>
                                <?php else: ?>
                                    <svg class="svg-icon svg-icon-star">
                                        <use xlink:href="
                                                <?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#star') ?>"
                                        ></use>
                                    </svg>
                                <?php endif; ?>
                            </button>
                        </div>
                        <div class="apart__slider-block">
                            <?php $imageUrl = "background-image: url('".$projectData['image']."');" ?>
                            <?php $planUrl = "background-image: url('".$model->getPlanImagePath()."');" ?>
                            <div class="swiper-container apart__slider-sub">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide apart__slider-item" data-slide="0" style="<?=$planUrl?>"></div>
                                    <?php
                                      $i = 1;
                                      if ($gallery != ''):
                                        foreach ($gallery as $photo):
                                    ?>
                                      <div class="swiper-slide apart__slider-item" data-slide="<?=$i?>" style="background-image: url('<?=$photo?>');"></div>
                                    <?php
                                        $i++;
                                        endforeach;
                                      endif;
                                    ?>
                                    <div class="swiper-slide apart__slider-item" data-slide="<?=$i?>" style="<?=$imageUrl?>"></div>
                                </div>
                            </div>
                            <div class="swiper-container apart__slider-main">
                                <?php if ($model->sold) { echo "<span class='sold-marker' style='position:absolute; left: 10px; top: 10px; z-index: 9999'>Нет в наличии</span>"; }?>
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide apart__slider-item" data-slide="0" style="<?=$planUrl?>"></div>
                                    <?php
                                      $i = 1;
                                      if ($gallery != ''):
                                        foreach ($gallery as $photo):
                                    ?>
                                      <div class="swiper-slide apart__slider-item" data-slide="<?=$i?>" style="background-image: url('<?=$photo?>');"></div>
                                    <?php
                                        $i++;
                                        endforeach;
                                      endif;
                                    ?>
                                    <div class="swiper-slide apart__slider-item" data-slide="<?=$i?>" style="<?=$imageUrl?>"></div>
                                </div>
                            </div>
                            <div class="apart__slider-btn apart__slider-btn--prev"><svg class="svg-icon svg-icon-arrow-left">
                                    <use xlink:href="images/symbol/svg/sprite.symbol.svg#arrow-left"></use>
                                </svg></div>
                            <div class="apart__slider-btn apart__slider-btn--next"><svg class="svg-icon svg-icon-arrow-left">
                                    <use xlink:href="images/symbol/svg/sprite.symbol.svg#arrow-left"></use>
                                </svg></div>
                        </div>

                        <div class="apart__mob-info">
                            <div class="apart__mob-title">
                                <div class="apart__mob-price">
                                    <h3><?= number_format($model->price, 0, '', ' ') ?> Т</h3>
                                    <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                        <h4><?= number_format($model->priceformetre, 0, '', ' ') ?> Т за м²</h4>
                                    <?php else: ?>
                                        <h4><?= number_format($model->priceformetre, 0, '', ' ') ?> Т м² үшін</h4>
                                    <?php endif ?>
                                </div>
                                <button class="apart__favorite">
                                    <?php if ($is_in_favorites): ?>
                                        <svg stroke="#519A08" style="fill: #519A08; stroke: #519A08" class="svg-icon svg-icon-star">
                                            <use xlink:href="
                                                <?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#star') ?>"
                                            ></use>
                                        </svg>
                                    <?php else: ?>
                                        <svg class="svg-icon svg-icon-star">
                                            <use xlink:href="
                                                <?= Url::to('@web/images/symbol/svg/sprite.symbol.svg#star') ?>"
                                            ></use>
                                        </svg>
                                    <?php endif; ?>
                                </button>
                            </div>

                            <ul class="breadcrumbs">
                                <li class="breadcrumbs__item">
                                    <a href="#"><?= $projectName ?></a>
                                </li><br />
                                <li class="breadcrumbs__item breadcrumbs--active">
                                    <p><?= \Yii::t('front', $projectData['status']) ?></p>
                                </li>
                            </ul>

                        </div>

                        <div class="apart__attributes">

                            <?php if ($model->finish): ?>
                                <div>
                                    <p class="results__info-title"><?= \Yii::t('front', 'Отделка') ?></p>
                                    <p class="results__info-txt"><?= \Yii::t('front', $model->finish) ?></p>
                                </div>
                            <?php endif; ?>

                            <?php if ($model->heightarea): ?>
                                <div>

                                    <p class="results__info-title"><?= \Yii::t('front', 'Высота потолков') ?>, м</p>
                                    <p class="results__info-txt"><?= $model->heightarea ?></p>
                                </div>
                            <?php endif; ?>

                            <div>
                                <p class="results__info-title"><?= \Yii::t('front', 'Комнат') ?></p>
                                <p class="results__info-txt"><?= $model->room_number ?></p>
                            </div>

                            <div>
                                <p class="results__info-title"><?= \Yii::t('front', 'Общая площадь') ?>, м²</p>
                                <p class="results__info-txt"><?= $model->area ?></p>
                            </div>

                            <?php if ($model->arealife): ?>
                                <div>
                                    <p class="results__info-title"><?= \Yii::t('front', 'Жилая площадь') ?>, м²</p>
                                    <p class="results__info-txt"><?= $model->arealife ?></p>
                                </div>
                            <?php endif; ?>

                        </div>

                        <div class="apart__attributes color">
                          <?php if ($blocks): ?>
                              <div>
                                  <p class="results__info-title"><?= \Yii::t('front', 'Блоки') ?></p>
                                  <p class="results__info-txt"><?= implode(',', $blocks) ?></p>
                              </div>
                          <?php endif; ?>

                          <?php if ($floor): ?>
                              <div>

                                  <p class="results__info-title"><?= \Yii::t('front', 'Этажи') ?></p>
                                  <p class="results__info-txt"><?= implode(',', $floor) ?></p>
                              </div>
                          <?php endif; ?>
                        </div>

                        <?php if ($projectData['scheme_image'] || $projectData['address_image']): ?>
                            <div class="apart__images">

                                <?php if ($projectData['scheme_image']): ?>
                                    <img src="<?= $projectData['scheme_image'] ?>" alt="<?= \Yii::t('front', 'Генеральный план').' '.$projectName ?>" />
                                <?php endif; ?>

                                <?php if ($projectData['address_image']): ?>
                                    <img src="<?= $projectData['address_image'] ?>" alt="<?= \Yii::t('front', 'Схема расположения').' '.$projectName ?>" />
                                <?php endif; ?>

                            </div>
                        <?php endif; ?>

                        <div class="apart__desc">
                            <div class="apart__desc-block">
                                <?php if (\Yii::$app->language === 'ru-RU'): ?>
                                    <h3 class="apart__desc-title"><?= \Yii::t('front', 'О жилом комплексе') ?> <?=$projectName?></h3>
                                <?php else: ?>
                                    <h3 class="apart__desc-title"><?=$projectName?> <?= \Yii::t('front', 'О жилом комплексе') ?></h3>
                                <?php endif; ?>
                                <?php if ($projectData['logo']): ?>
                                    <div class="apart__desc-img">
                                        <?php $logoUrl = "background-image: url('".$projectData['logo']."')" ?>
                                        <div class="project-block__icon" style="<?= $logoUrl ?>"></div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="apart__desc-txt">
                                <?= $projectData['desc'] ?>
                            </div>
                            <a class="btn" target="_blank" href="<?= $projectData['link'] ?>"><?= \Yii::t('front', 'Подробнее') ?></a>
                        </div>
                    </div>

                    <?php if ($projectData['lat'] && $projectData['lon']): ?>
                        <div class="apart__map" id="apartMap" data-coords="<?= $projectData['lat'].','.$projectData['lon'] ?>"></div>
                    <?php endif; ?>

                </div>
                <div class="apart__subinfo apart__block">
                    <ul class="apart__subinfo-list">
                        <li>
                            <p class="results__info-title"><?= \Yii::t('front', 'Цена за м²') ?>, тг</p>
                            <p class="results__info-txt"><?= number_format($model->priceformetre, 0, '', ' ') ?></p>
                        </li>
                        <li>
                            <p class="results__info-title"><?= \Yii::t('front', 'Общая стоимость') ?>, тг</p>
                            <p class="results__info-txt"><?= number_format($model->price, 0, '', ' ') ?></p>
                        </li>
                    </ul>
                    <div class="apart__btn-wrap">
                        <a data-modal-btn="modal-question" class="btn"><?= \Yii::t('front', 'Заказать звонок') ?></a>
                    </div>
                    <div class="apart__controls">
                        <?php if ($model->locationtype === \app\modules\collection\models\LocationType::getLocationUUUID()): ?>
                            <?php $similar_text = \Yii::t('front', 'Похожие квартиры'); ?>
                        <?php else: ?>
                            <?php $similar_text = \Yii::t('front', 'Похожие помещения'); ?>
                        <?php endif; ?>
                        <a href="<?= Url::to(['/collection/location',
                            'price_min' => $price_min,
                            'price_max' => $price_max,
                            'area_min' => $area_min,
                            'area_max' => $area_max,
                            'location_type' => $model->locationtype
                        ]) ?>" class="apart__similar"><?= "$similar_text ($count_similar)" ?></a>
                        <div class="apart__links">
                            <div class="ya-share2 js-share"
                                 data-curtain
                                 data-shape="round"
                                 data-lang="<?= \Yii::$app->language === 'ru-RU' ? 'ru' : 'kk' ?>"
                                 data-limit="0"
                                 data-more-button-type="short"
                                 data-popup-position="outer"
                                 data-services="vkontakte,facebook,telegram,whatsapp"
                                 data-use-links
                                 ?>"
                            ></div>
                            <a href="#">
                                <svg class="svg-icon" width="14" height="20" viewBox="0 0 14 20" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.49949 1.5C5.49949 0.671573 6.17107 0 6.99949 0C7.82792 0 8.49949 0.671573 8.49949 1.5V10.5C8.49949 11.3284 7.82792 12 6.99949 12C6.17107 12 5.49949 11.3284 5.49949 10.5V1.5Z" />
                                    <path d="M10.7389 8.06066C11.3247 7.47487 12.2744 7.47487 12.8602 8.06066C13.446 8.64645 13.446 9.59619 12.8602 10.182L8.08203 14.9602C7.49624 15.5459 6.54649 15.5459 5.96071 14.9602C5.37492 14.3744 5.37492 13.4246 5.96071 12.8388L10.7389 8.06066Z" />
                                    <path d="M3.26011 8.06066C2.67432 7.47487 1.72457 7.47487 1.13879 8.06066C0.552999 8.64645 0.552999 9.59619 1.13878 10.182L5.91696 14.9602C6.50275 15.5459 7.45249 15.5459 8.03828 14.9602C8.62407 14.3744 8.62407 13.4246 8.03828 12.8388L3.26011 8.06066Z" />
                                    <path d="M0 18.5C0 17.6716 0.671573 17 1.5 17H12.5C13.3284 17 14 17.6716 14 18.5C14 19.3284 13.3284 20 12.5 20H1.5C0.671573 20 0 19.3284 0 18.5Z" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php

$modelId = $model->uuid;
$url = Url::to('/toggle-favorites');

$js = <<<JS

$(".ya-share2__link").addClass('js-share');

// logic to add/remove location from favorites list
$(".apart__favorite").click(function () {

    $.ajax({
        url: `$url`,
        data: {id: `$modelId`},
        success: function (data) {
            let countFavorites = $('.header__favorites-icon').find('span').html();
            if (countFavorites === '' && data !== 0) {
                $('.header__favorites-icon').find('span').html(1);
                $('.header__favorites-icon').addClass('header__favorites-icon--count');
                $('.apart__favorite').find('svg').attr('style', 'fill: #519A08; stroke: #519A08');
                $('.apart__favorite').find('svg').attr('stroke', '#519A08');
            }
            if (data === 1) {
                $('.header__favorites-icon').find('span').html(+countFavorites + 1);
                $('.header__favorites-icon').addClass('header__favorites-icon--count');
                $('.apart__favorite').find('svg').attr('style', 'fill: #519A08; stroke: #519A08');
                $('.apart__favorite').find('svg').attr('stroke', '#519A08');
            }

            if (data === -1) {
                $('.header__favorites-icon').find('span').html(+countFavorites - 1);
                $('.header__favorites-icon').addClass('header__favorites-icon--count');
                $('.apart__favorite').find('svg').removeAttr('style');
                $('.apart__favorite').find('svg').removeAttr('stroke');
                let checkCounter = parseInt($('.header__favorites-icon').find('span').html());

                if (checkCounter === 0) {
                    $('.header__favorites-icon').removeClass('header__favorites-icon--count');
                }
            }
        },
        error: function (exception) {
            console.log('ERROR');
        }
    });



})

JS;

$this->registerJs($js);
?>
