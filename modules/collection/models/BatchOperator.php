<?php


namespace app\modules\collection\models;


use yii\base\InvalidArgumentException;
use yii\db\Query;


/**
 * Class BatchOperator
 * @package app\modules\collection\models
 */
class BatchOperator
{
    const LIMIT = 1000;

    /**
     * @param array $data
     * @param string $tableName
     * @param array $attributes
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
     public static function clearTable(string $tableName) {
       $connection = \Yii::$app->db;
       $connection
           ->createCommand()
           ->delete($tableName)
           ->execute();

       //return false;
     }
    public static function batchInsert(array $data,
                                       string $tableName,
                                       array $attributes)
    {
        $data = self::filterCollection($data, $tableName);

//        if ($tableName === Location::tableName()) {
//            $data = ImageSaver::batchSaveImages($data);
//        }

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $connection->createCommand()->batchInsert(
                $tableName,
                $attributes,
                $data
            )->execute();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function batchUpdate(array $data, string $tableName)
    {
        if ($tableName === Plan::tableName()) {
            $data = ImageSaver::batchSaveImages($data);
        }

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            foreach ($data as $datum) {
                $connection->createCommand()->update($tableName, $datum, ['uuid' => $datum['uuid']])->execute();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function sendToCmsData(array $data)
    {

      foreach ($data as $location) {

        $query = new Query();
        $query->select('name')->from('projects_collection')->where('uuid=:project', array(':project'=>$location['project']));
        $command = $query->createCommand();
        $project = $command->queryAll();
        $location['project'] = $project[0]['name'] ? $project[0]['name'] : 'Не найдено';

        $query = new Query();
        $query->select('name')->from('location_type_collection')->where('uuid=:locationtype', array(':locationtype'=>$location['locationtype']));
        $command = $query->createCommand();
        $locationtype = $command->queryAll();
        $location['locationtype'] = $locationtype[0]['name'] ? $locationtype[0]['name'] : 'Не найдено';

        $params = Array(
            'filter' => array('uuid'=>$location['uuid']),
        );

        $ch = curl_init('https://cms.abpx.kz/api/collections/get/exin_plans_test?token=account-98a82c65cb0b513d2be55d1b56b599');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $responce = curl_exec($ch);

        $responce = json_decode($responce, true);

        if (isset($responce['entries'][0]['_id']) && !empty($responce['entries'][0]['_id'])) {

          $location['id'] = $responce['entries'][0]['_id'];
          $params = Array(
              'data' => array(
                '_id' => $location['id'],
                'project' => $location['project'],
                'uuid' => $location['uuid'],
                'area' => $location['area'],
                'room_number' => $location['room_number'],
                'locationtype' => $location['locationtype'],
                'location_file' => $location['file'],
                'sold' => $location['sold']
              ),
          );

        } else {

          $params = Array(
              'data' => array(
                'project' => $location['project'],
                'uuid' => $location['uuid'],
                'area' => $location['area'],
                'room_number' => $location['room_number'],
                'locationtype' => $location['locationtype'],
                'location_file' => $location['file'],
                'sold' => $location['sold']
              ),
          );

        }



        $ch1 = curl_init('https://cms.abpx.kz/api/collections/save/exin_plans_test?token=account-98a82c65cb0b513d2be55d1b56b599');
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch1, CURLOPT_HEADER, false);
        curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $responce = curl_exec($ch1);

        curl_close($ch1);

      }


    }

    /**
     * Impure function to gracefully clear collection from records that already exist in database
     *
     * @param array $data
     * @param string $tableName
     * @return array
     */
    private static function filterCollection(array $data, string $tableName)
    {
        foreach ($data as $key => $model) {
            $isInDb = (new Query())->from($tableName)->where(['uuid' => $model['uuid']])->one();

            if ($isInDb) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
