<?php

namespace app\modules\collection\models;

use Yii;

/**
 * This is the model class for table "projects_collection".
 *
 * @property integer $id
 * @property string $uuid
 * @property string $name
 * @property string $city
 * @property string $district
 * @property integer $blocked
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blocked'], 'integer'],
            [['blocked'], 'integer'],
            [['uuid', 'name', 'city', 'district'], 'string', 'max' => 256],
            [['uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'UUID',
            'name' => 'Название',
            'city' => 'Город',
            'district' => 'Район',
            'blocked' => 'Активность',
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\collection\models\query\ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return (new \app\modules\collection\models\query\ProjectQuery(get_called_class()))->active();
    }

    /**
     * @return array
     */
    public function fields() : array
    {
        $fields = parent::fields();
        $fields['name'] = function ($model) {
            return trim(mb_ereg_replace('ЖК', '', $model->name));
        };
        unset($fields['id']);

        return $fields;
    }
}
