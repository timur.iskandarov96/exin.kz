<?php


namespace app\modules\collection\models;


use yii\db\Exception;
use yii\helpers\FileHelper;

class ImageSaver
{
    public static function saveImage($uniqueDir, $img) : string
    {
        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $fileData = base64_decode($img);

        $path = \Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR .
            'media' . DIRECTORY_SEPARATOR . Location::tableName() . DIRECTORY_SEPARATOR . $uniqueDir;

        FileHelper::createDirectory($path);

        $fileName = md5(microtime()).'.jpeg';
        file_put_contents( $path.DIRECTORY_SEPARATOR.$fileName, $fileData);

        return $fileName;
    }

    public static function batchSaveImages(array $data)
    {
        $result = [];
        foreach ($data as $datum) {
            try {
                $fileName = '';

                if (isset($datum['file'])) {
                    $fileName = self::saveImage($datum['uuid'], $datum['file']);
                } else {
                    $model = Location::find()->where(['uuid' => $datum['uuid']])->one();

                    if ($model) {
                        $fileName = $model->file;
                    }
                }

                $result[] = [
                    'uuid' => $datum['uuid'],
                    'project' => $datum['project'],
                    'area' => $datum['area'],
                    'arealife' => $datum['arealife'],
                    'locationtype' => $datum['locationtype'],
                    'room_number' => $datum['room_number'],
                    'locationstatus' => $datum['locationstatus'],
                    'price' => $datum['price'],
                    'priceformetre' => $datum['priceformetre'],
                    'finish' => $datum['finish'],
                    'heightarea' => $datum['heightarea'],
                    'file' => $fileName,
                    'blocked' => $datum['blocked'] ?? false,
                ];
            } catch (\Exception $e) {
                \Yii::error('Image saving error');
                throw new \yii\base\Exception('Could not save image file');
            }
        }

        return $result;
    }
}