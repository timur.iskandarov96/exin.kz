<?php

namespace app\modules\collection\models;

use Yii;

/**
 * This is the model class for table "cities_collection".
 *
 * @property integer $id
 * @property string $uuid
 * @property string $name
 * @property integer $blocked
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blocked'], 'integer'],
            [['uuid', 'name'], 'string', 'max' => 256],
            [['uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'UUID',
            'name' => 'Название',
            'blocked' => 'Активность',
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\collection\models\query\CityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return (new \app\modules\collection\models\query\CityQuery(get_called_class()))->active();
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['id']);
        return $fields;
    }
}
