<?php

namespace app\modules\collection\models;

use Yii;

/**
 * @deprecated
 * This is the model class for table "plans_collection".
 *
 * @property integer $id
 * @property string $uuid
 * @property string $text
 * @property string $ru_image
 * @property string $kk_image
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blocked'], 'integer'],
            [['uuid'], 'string', 'max' => 256],
            [['text'], 'string', 'max' => 512],
//            [['ru_image', 'kk_image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg']],
            [['ru_image', 'kk_image'], 'string', 'max' => 256],
            [['uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'UUID',
            'text' => 'Текс',
            'ru_image' => 'Файл изображенияна на русском языке',
            'kk_image' => 'Файл изображения на казахском языке',
            'blocked' => 'Активность'
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\collection\models\query\PlanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\collection\models\query\PlanQuery(get_called_class());
    }

//    /**
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return [
//            'ru_imageUpload' => [
//                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
//                'attribute' => 'ru_image',
//                'path' => '@webroot/media/plans_collection/{id}',
//                'url' => '@web/media/plans_collection/{id}',
//                'scenarios' => ['create', 'update'],
//                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
//            ],
//            'kk_imageUpload' => [
//                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
//                'attribute' => 'kk_image',
//                'path' => '@webroot/media/plans_collection/{id}',
//                'url' => '@web/media/plans_collection/{id}',
//                'scenarios' => ['create', 'update'],
//                'thumbs' => ['thumb' => ['width' => 200, 'height' => 200, 'quality' => 90], 'preview' => ['width' => 50, 'height' => 50, 'quality' => 90]],
//            ],
//        ];
//    }

    /**
     * @return array
     */
    public function fields() : array
    {
        $fields = parent::fields();
        unset($fields['id']);
        return $fields;
    }
}
