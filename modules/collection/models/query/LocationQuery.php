<?php

namespace app\modules\collection\models\query;

use app\modules\collection\models\Location;


/**
 * This is the ActiveQuery class for [[\app\modules\collection\models\Location]].
 *
 * @see \app\modules\collection\models\Location
 */
class LocationQuery extends \yii\db\ActiveQuery
{
    const LIMIT = 6;
    const MILLION = 1000000;

    public function active()
    {
        return $this->andWhere(['{{location_collection}}.blocked' => 0]);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\Location[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\collection\models\Location|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param array $params
     * @return array
     */
    public static function search(array $params) : array
    {
        $query = self::searchByParams($params);

        $limit = isset($params['limit']) && $params['limit'] !== null ? $params['limit'] : self::LIMIT;
        $times = isset($params['page']) && $params['page'] !== null ? $params['page'] - 1 : 0;

        $offset = $times * $limit;

        return $query
            ->limit($limit)
            ->offset($offset)
            ->active()
            ->all();
    }

    /**
     * @param array $params
     * @return int
     */
    public static function searchCount(array $params) : int
    {
        $query = self::searchByParams($params);

        return $query->active()->count();
    }

    /**
     * @return false|float
     */
    public static function getMinPrice()
    {
        return floor(Location::find()->min('price') / LocationQuery::MILLION);
    }

    /**
     * @param array $params
     * @return LocationQuery
     */
    private static function searchByParams(array $params) : LocationQuery
    {
        $query = Location::find();
        $currentlyMinPrice = LocationQuery::getMinPrice();

        if (isset($params['room_number']) && $params['room_number']) {
            $roomNumberAsArr = explode('-', $params['room_number']);
            $roomNumberAsArr = array_map(function($item) {
                return (int) $item;
            }, $roomNumberAsArr);

            $query->andFilterWhere(['IN', 'room_number', $roomNumberAsArr]);
        }

        if (isset($params['price_max']) && $params['price_max']) {
            $priceMax = $currentlyMinPrice > (int) $params['price_max'] ?
                $currentlyMinPrice :
                (int) $params['price_max']
            ;
            $query->andFilterWhere(['<=', 'price', $priceMax * self::MILLION]);
        }

        if (isset($params['project']) && $params['project'] !== null) {
            $query->andFilterWhere(['project' => $params['project']]);
        }

        if (isset($params['district']) && $params['district']) {
            $query->leftJoin(
                'projects_collection',
                '{{projects_collection}}.uuid = {{location_collection}}.project'
            )->andWhere(['{{projects_collection}}.district' => $params['district']]);
        }

        if (isset($params['price_min']) && $params['price_min']) {
            $query->andFilterWhere(['>=', 'price', (int) $params['price_min'] * self::MILLION]);
        }

        if (isset($params['area_max']) && $params['area_max']) {
            $query->andFilterWhere(['<=', 'area', (int) $params['area_max']]);
        }

        if (isset($params['area_min']) && $params['area_min']) {
            $query->andFilterWhere(['>=', 'area', (int) $params['area_min']]);
        }

        if (isset($params['location_type']) && $params['location_type']) {
            $query->andFilterWhere(['locationtype' => $params['location_type']]);
        }

        if (isset($params['finish_type']) && $params['finish_type']) {
            $finishType = Location::FINISH_TYPES[$params['finish_type']];
            $query->andFilterWhere(['like', 'area', $finishType]);
        }

        return $query;
    }
}
