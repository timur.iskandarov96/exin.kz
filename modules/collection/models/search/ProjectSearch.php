<?php


namespace app\modules\collection\models\search;


use app\modules\collection\models\Location;
use app\modules\collection\models\Project;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ProjectSearch extends Project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blocked'], 'integer'],
            [['uuid', 'name', 'city', 'district'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ]
        ]);

        //$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // filtering conditions
        $query->andFilterWhere([
            'name' => $params['name'],
            'city' => $params['city'],
            'district' => $params['district'],
        ]);

        return $dataProvider;
    }
}