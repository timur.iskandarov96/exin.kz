<?php

namespace app\modules\collection\models;

use Yii;

/**
 * This is the model class for table "location_type_collection".
 *
 * @property integer $id
 * @property string $uuid
 * @property string $name
 */
class LocationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_type_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blocked'], 'integer'],
            [['uuid', 'name'], 'string', 'max' => 256],
            [['uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'UUID',
            'name' => 'Название',
            'blocked' => 'Активность'
        ];
    }
    
    /**
     * @inheritdoc
     * @return \app\modules\collection\models\query\LocationTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return (new \app\modules\collection\models\query\LocationTypeQuery(get_called_class()))->active();
    }

    /**
     * @return array
     */
    public function fields() : array
    {
        $fields = parent::fields();
        unset($fields['id']);
        unset($fields['blocked']);
        return $fields;
    }

    /**
     * @return string
     */
    public static function getLocationUUUID() : string
    {
        return self::find()
            ->where(['name' => 'Квартира'])
            ->one()
            ->uuid ?? '';
    }
}
