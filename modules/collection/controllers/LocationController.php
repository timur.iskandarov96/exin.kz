<?php

namespace app\modules\collection\controllers;


use app\components\front\Paginator;
use app\modules\collection\models\Location;
use app\modules\collection\models\query\LocationQuery;
use app\modules\collection\models\query\ProjectQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * Class LocationController
 * @package app\modules\collection\controllers
 */
class LocationController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $params = \Yii::$app->request->queryParams;
        $locations = LocationQuery::search($params);
        $page = $params['page'] ?? 1;
        $limit = $params['limit'] ?? LocationQuery::LIMIT;
        $countLocations = LocationQuery::searchCount($params);

        $tail = $countLocations % $limit > 0 ? 1 : 0;
        $paginatorLimit = floor($countLocations / $limit) + $tail;

        if ($countLocations <= $limit) {
            $paginatorLimit = 1;
        }

        $paginator = new Paginator($countLocations, $paginatorLimit, $page, $limit);

        return $this->render('index', [
            'locations' => $locations,
            'projects_collection' => ProjectQuery::getCollection(),
            'room_number' => $params['room_number'] ?? '',
            'price' => $params['price_max'] ?? '',
            'district' => $params['district'] ?? '',
            'paginator' => $paginator,
            'current_limit' => $limit,
            'count_locations' => $countLocations
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id) : string
    {
        $model = Location::find()->where(['uuid' => $id])->one();

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($model->blocked) {
            throw new NotFoundHttpException();
        }


        list($priceMax, $priceMin) = [
            floor($model->price / LocationQuery::MILLION) * LocationQuery::MILLION + LocationQuery::MILLION,
            floor($model->price / LocationQuery::MILLION) * LocationQuery::MILLION - LocationQuery::MILLION
        ];
        list($areaMax, $areaMin) = [
            floor($model->area + 10.0),
            round($model->area - 10.0)
        ];

        $countSimilar = Location::find()
            ->where(['>=', 'price', $priceMin])
            ->andWhere(['<=', 'price', $priceMax])
            ->andWhere(['>=', 'area', $areaMin])
            ->andWhere(['<=', 'area', $areaMax])
            ->andWhere(['locationtype' => $model->locationtype])
            ->andWhere(['blocked' => false])
            ->count();

        $cookies = \Yii::$app->request->cookies;
        $locations = [];

        if (isset($cookies['locations'])) {
            $locations = $cookies['locations']->value;
            $locations = \yii\helpers\Json::decode($locations);
        }

        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => $model->room_number.'-комн. квартира, '.$model->area.' м²',
                'description' => $model->room_number.'-комн. квартира, '.$model->area.' м², ЖК '.$model->getProjectName(),
                'keywords' => 'Exclusive Qurylys, квартиры, ЖК '.$model->getProjectName().', Алматы',
            ],
            'kk-KZ' => [
                'lang' => 'kk-KZ',
                'title' => $model->room_number.' бөлмелі пәтер '.$model->area.' м²',
                'description' => $model->room_number.'бөлмелі пәтер '.$model->area.' м², ТК '.$model->getProjectName(),
                'keywords' => 'Exclusive Qurylys, ТК '.$model->getProjectName().', Алматы',
            ],
        ]);

        \Yii::$app->metaTags->registerMetaImage("https://exin.kz".$model->getPlanImagePath());

        $params = Array(
            'filter' => array('uuid'=>$model->uuid),
        );

        $ch = curl_init('https://cms.abpx.kz/api/collections/get/exin_plans_test?token=account-98a82c65cb0b513d2be55d1b56b599');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $responce = curl_exec($ch);

        curl_close($ch);

        $responce = json_decode($responce, true);

        $model->heightarea = isset($responce['entries'][0]['heightarea']) ? $responce['entries'][0]['heightarea'] : '';

        $model->arealife = isset($responce['entries'][0]['arealife']) ? $responce['entries'][0]['arealife'] : '';

        $blocks = isset($responce['entries'][0]['blocks']) ? $responce['entries'][0]['blocks'] : '';

        if (is_array($blocks)) {

          if (!preg_grep("#[^0-9\.,]#", $blocks)) {
            $block_data = array();
            $next_var = '';
            $last_var = '';

              foreach ($blocks as $key => $value) {
                $next_var = isset($blocks[$key+1]) ? $blocks[$key+1] : '';
                if (($value + 1) == $next_var) {
                  if ($last_var != '') {} else {
                    $last_var = $value . '-';
                  }
                } else {
                  if ($last_var != '') {
                    $block_data[] = $last_var . $value;
                    $last_var = '';
                  } else {
                    $block_data[] = $value;
                  }
                }
              }
          } else {
            $block_data = $blocks;
          }

        }

        $floor = isset($responce['entries'][0]['floor']) ? $responce['entries'][0]['floor'] : '';

        if (is_array($floor)) {

          $floor_data = array();
          $next_var = '';
          $last_var = '';

          foreach ($floor as $key => $value) {
            $next_var = isset($floor[$key+1]) ? $floor[$key+1] : '';
            if (($value + 1) == $next_var) {
              if ($last_var != '') {} else {
                $last_var = $value . '-';
              }
            } else {
              if ($last_var != '') {
                $floor_data[] = $last_var . $value;
                $last_var = '';
              } else {
                $floor_data[] = $value;
              }
            }
          }

        }

        if (isset($responce['entries'][0]['gallery']) && !empty($responce['entries'][0]['gallery'])) {
          $gallery = array();
          foreach ($responce['entries'][0]['gallery'] as $photo) {
            $gallery[] = 'https://cms.abpx.kz' . $photo['path'];
          }
        }

        return $this->render('view', [
            'model' => $model,
            'count_similar' => $countSimilar,
            'blocks' => isset($block_data) ? $block_data : '',
            'floor' =>  isset($floor_data) ? $floor_data : '',
            'gallery' =>  isset($gallery) ? $gallery : '',
            'price_max' => round($priceMax / LocationQuery::MILLION),
            'price_min' => floor($priceMin / LocationQuery::MILLION),
            'area_min' => $areaMin,
            'area_max' => $areaMax,
            'is_in_favorites' => in_array($model->uuid, $locations)
        ]);
    }

    /**
     * @param $id
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionToggleFavorites($id)
    {
        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $cookies = \Yii::$app->request->cookies;

            if (isset($cookies['locations'])) {
                $locations = $cookies['locations']->value;
                $locations = \yii\helpers\Json::decode($locations);
            } else {
                $locations = [];
            }

            $result = 0;

            if (in_array($id, $locations)) {
                $locationKey = array_search($id, $locations);
                unset($locations[$locationKey]);
                $result = -1;
            } else {
                $locations[] = $id;
                $result = 1;
            }

            $locations = \yii\helpers\Json::encode($locations);

            $cookies = \Yii::$app->response->cookies;
            unset($cookies['locations']);

            $cookies->add(new \yii\web\Cookie([
                'name' => 'locations',
                'value' => $locations,
                'expire' => time() + 86400 * 365,
                'sameSite' => \yii\web\Cookie::SAME_SITE_STRICT
            ]));

            return $result;
        }

        throw new NotFoundHttpException();
    }

    public function actionFavorites()
    {
        $cookies = \Yii::$app->request->cookies;

        $locationsUuids = [];
        if (isset($cookies['locations'])) {
            $locationsUuids = \yii\helpers\Json::decode($cookies['locations']->value);
        }

        $locations = [];
        if (count($locationsUuids) > 0) {
            $locations = Location::find()->where(['IN', 'uuid', $locationsUuids])->all();
        }

        $params = \Yii::$app->request->queryParams;
        $page = $params['page'] ?? 1;
        $limit = $params['limit'] ?? LocationQuery::LIMIT;
        $countLocations = count($locations);

        $tail = $countLocations % $limit > 0 ? 1 : 0;
        $paginatorLimit = floor($countLocations / $limit) + $tail;

        if ($countLocations <= $limit) {
            $paginatorLimit = 1;
        }

        $offset = $page * $limit;
        $start = $page === 1 ? 0 : $offset - $limit;
        $tail = count($locations) - $offset > 0 ? $limit : null;
        $locationsToShow = array_slice($locations, $start, $tail);

        $paginator = new Paginator($countLocations, $paginatorLimit, $page, $limit);

        return $this->render('favorites', [
            'locations' => $locationsToShow,
            'paginator' => $paginator,
            'page' => $page,
            'limit' => $limit
        ]);
    }
}
