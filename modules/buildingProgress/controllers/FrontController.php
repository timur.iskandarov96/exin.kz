<?php


namespace app\modules\buildingProgress\controllers;


use app\components\front\Helper;
use app\modules\buildingProgress\models\BuildingProgress;
use app\modules\buildingProgress\models\BuildingProgressGallery;
use app\modules\buildingProgress\models\BuildingProgressTranslation;
use app\modules\project\models\ProjectStatus;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class FrontController extends Controller
{
    /**
     * HINT: $status and $statusMobile fields will be used in future
     * @return string
     */
    public function actionIndex()
    {
        $date = \Yii::$app->request->get('date');
        $status = \Yii::$app->request->get('type');
        $statusMobile = \Yii::$app->request->get('status-mobile');

        if ($date || $status || $statusMobile) {
            $status = \Yii::$app->request->get('type') ?? \Yii::$app->request->get('status') ??
                \Yii::$app->request->get('type') ?? \Yii::$app->request->get('status-mobile') ;
            $date = \Yii::$app->request->get('date') == '' ? null : \Yii::$app->request->get('date');
            $project = \Yii::$app->request->get('project') == '' ? null : \Yii::$app->request->get('project');

            $buildingProgressList = BuildingProgress::find()->joinWith('project')->all();

            $buildingTranslation = BuildingProgressTranslation::find()
                ->where(['building_progress_id' => $project])
                ->andWhere(['lang' => \Yii::$app->language])
                ->one();

            $buildingProgressModel = BuildingProgress::findOne($project);
            $months = (array) Helper::formatDates(BuildingProgressGallery::getUniqueDates($project), \Yii::$app->language);


            return $this->render('index', [
                'building_progress_list' => $buildingProgressList,
                'projects' => $buildingProgressList,
                'months' => array_reverse($months),
                'statuses' => ArrayHelper::map(ProjectStatus::find()->all(), 'id', 'status'),
                'gallery_date' => $date,
                'gallery_project' => $project,
                'project_desc' => $buildingTranslation->description,
            ]);
        }

        $buildingProgressList = BuildingProgress::find()->joinWith('project')->all();

        $dates = Helper::formatDates(BuildingProgressGallery::getUniqueDates($buildingProgressList[0]->id), \Yii::$app->language);
        end($dates);

        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => 'Ход строительства'
            ]
        ]);

        $months = (array) Helper::formatDates(BuildingProgressGallery::getUniqueDates($buildingProgressList[0]->id), \Yii::$app->language);

        return $this->render('index', [
            'building_progress_list' => $buildingProgressList,
            'projects' => $buildingProgressList,
            'months' => array_reverse($months),
            'statuses' => ArrayHelper::map(ProjectStatus::find()->all(), 'id', 'status'),
            'gallery_date' => key($dates),
            'gallery_project' => $buildingProgressList[0]->id
        ]);

    }
}