<?php

use yii\helpers\Url;
use yii\helpers\Html;

\app\assets\AdvanceAsset::register($this);

/* @var $months array */
/* @var $projects array */
/* @var $building_progress_list \app\modules\buildingProgress\models\BuildingProgress[] */
/* @var $statuses array */
/* @var $gallery_date */
/* @var $gallery_project */
/* @var $project_desc */

$project_name = '';

?>

<main class="js-header-size">
    <div class="advance page">

        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Ход строительства'), 'url' => false]
        ]]) ?>

        <div class="container-main">
            <h1><?= \Yii::t('front', 'Ход строительства') ?></h1>
        </div>

        <div class="advance__info">

            <div class="container-main">
                <?= Html::beginForm(['/building-progress'], 'get', ['enctype' => 'multipart/form-data', 'class' => 'advance__filter']) ?>

                <div class="advance__select-block">
                    <div class="select-form advance__select" id="select-projects">
                        <div class="select-form__head">
                            <span class="select-form__title"><?= \Yii::t('front', 'Жилой комплекс') ?></span>
                        </div>
                        <?php $hiddenProject = $gallery_project === null ? '0' : $gallery_project; ?>
                        <?= Html::hiddenInput('project', $hiddenProject, ['data-select-input' => '']) ?>
                        <ul class="select-form__options">
                            <?php foreach ($projects as $project): ?>
                                <?php if (($gallery_project * 1) === $project->id): ?>
                                    <li class="is-active" data-select-value="<?= $project->id ?>"><?= $project->project->name ?></li>
                                    <?php $project_name = $project->project->name; ?>
                                <?php else: ?>
                                    <li data-select-value="<?= $project->id ?>"><?= $project->project->name ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

                <div class="advance__select-block">
                    <div class="select-form advance__select" id="select-dates">
                        <div class="select-form__head">
                            <span class="select-form__title"><?= \Yii::t('front', 'Месяц') ?></span>
                        </div>
                        <?php $hiddenDate = $gallery_date === null ? '0' : $gallery_date; ?>
                        <?= Html::hiddenInput('date', $hiddenDate, ['data-select-input' => '']) ?>
                        <ul class="select-form__options">
                            <?php foreach ($months as $key => $month): ?>
                                <?php if ($gallery_date === $key): ?>
                                    <li class="is-active" data-select-value="<?= $key ?>"><?= $month ?></li>
                                <?php else: ?>
                                    <li data-select-value="<?= $key ?>"><?= $month ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

                <div class="advance__controls">
                    <button class="btn"><?= \Yii::t('front', 'Показать') ?></button>
                </div>
                <?= Html::endForm() ?>
            </div>

            <?php if (count($building_progress_list) > 0): ?>

                <div class="advance__gallery" data-gallery="1">
                    <?php $gallery = array_values(current($building_progress_list)->getGallery($gallery_project, $gallery_date)); ?>

                    <div style="display: none" class="container-main advance__gallery-container">
                        <div class="advance__gallery-img">
                            <img src="<?= current($gallery) ?>" alt="Главное изображение галлереи">
                        </div>
                    </div>

                    <div class="container-main advance__slider-container">
                        <div class="swiper-container advance__preview-list">
                            <div class="building__steps_gallery swiper-wrapper">
                                <?php foreach ($gallery as $key => $item): ?>
                                    <div class="gallery__item swiper-slide">
                                        <a href="<?php echo 'https://exin.kz' . $item ?>" data-fslightbox="first-lightbox">
                                            <img src="<?php echo 'https://exin.kz' . $item ?>" alt="">
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="advance__preview-controls">
                            <div class="partners__btn partners__btn--prev"></div>
                            <div class="advance__pagination"></div>
                            <div class="partners__btn partners__btn--next"></div>
                        </div>
                    </div>

                </div>

                <div class="container-main" style="display: none">
                    <div class="advance__desc">
                        <div class="advance__desc-txt">
                            <h2><?= $project_name ?></h2>
                            <?php echo isset($project_desc) ? $project_desc : '' ?>
                        </div>
                        <!--div class="advance__marks">
                            <span class="advance__mark advance__mark--service">
                                <?= \Yii::t('front', 'Срок сдачи').': '.
                        current($building_progress_list)->service_date ?>
                            </span>
                        </div-->
                    </div>
                </div>

            <?php endif; ?>
        </div>
</main>

<style>
    @media (min-width: 1024px) {
        .gallery__item:not(:first-child) {height: 145px;}

        .building__steps_gallery {
            display: grid;
            grid-template-columns: repeat(6, 1fr);
            grid-gap: 20px;
        }

        .gallery__item img {
            border-radius: 6px;
            overflow: hidden;
        }

        .gallery__item:first-child {
            grid-column-start: 1;
            grid-column-end: 4;
            grid-row-start: 1;
            grid-row-end: 4;
        }

        .advance__controls {
            margin-left: 0px;
        }

        form.advance__filter {
            grid-gap: 20px;
        }

        .advance__preview-list {
            margin: 30px 0px 0;
        }

        .advance__preview-list {
            padding-bottom: 40px;
        }
        .advance__select-block {
            width: 280px;
        }


    }

    .gallery__item img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .advance__preview-list {
        padding-bottom: 0px !important;
    }

</style>

<script src="https://tamarix.kz/assets/js/fslightbox.js"></script>

<script>

    document.addEventListener('DOMContentLoaded', domIsReady())

    function domIsReady() {
        var selects = document.querySelectorAll('#select-projects li[data-select-value]')

        for (let i = 0; i < selects.length; i++) {
            let item = selects[i]
            item.addEventListener('click', function () {
                let id = item.getAttribute('data-select-value')
                getDates(id)
            })
        }

        function getDates(id) {
            const request = new XMLHttpRequest()
            request.open('GET', '/admin/buildingProgress/building-progress/get-building-progress-dates?id=' + id + '&lang=ru', true)
            request.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
            request.onload = function (e) {
                if (request.readyState == 4 && request.status == 200) {
                    let obj = new Function('return (' + request.response + ')')()
                    addNewOptionsForDate(obj.dates.reverse());
                }
            };
            request.send(null);
        }

        function addNewOptionsForDate(dates) {

            var select = document.querySelector('#select-dates .select-form__options')
            let selectTitle = document.querySelector('#select-dates .select-form__title')
            let selectInput = document.querySelector('#select-dates input[name="date"]')
            let lastDateVal = ''
            select.innerHTML = ''

            for (date in dates) {

                let option = document.createElement('li')

                option.setAttribute('data-select-value', date)
                lastDateVal = date
                selectInput.value = date

                option.textContent = dates[date]
                selectTitle.textContent = dates[date]

                option.addEventListener('click', function () {

                    selectInput.value = this.getAttribute('data-select-value')
                    selectTitle.textContent = this.textContent

                    let allOptionsInHTML = document.querySelectorAll('#select-dates li[data-select-value]')
                    for (option in allOptionsInHTML) {
                        if (allOptionsInHTML[option].classList) {
                            allOptionsInHTML[option].classList.remove('is-active')
                        }
                    }
                    this.classList.add('is-active')

                    select.classList.remove('is-active')
                    selectTitle.classList.remove('is-active')

                })

                select.appendChild(option)
            }

            let firstDate = document.querySelector('[data-select-value="' + lastDateVal + '"]')
            firstDate.classList.add('is-active')

        }

        //Set images height
        var items = document.querySelectorAll('.gallery__item')
        items.forEach((item) => {
            let propWidth = 16
            let propHeight = (window.outerWidth > 1024) ? 13 : 9
            let widthItem = item.offsetWidth
            let heightItem = propHeight * (widthItem / propWidth)
            item.style.minHeight = heightItem + 'px'
        })

    }

</script>