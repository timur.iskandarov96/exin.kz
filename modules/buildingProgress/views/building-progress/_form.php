<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\buildingProgress\models\BuildingProgress */
/* @var $form Bridge\Core\Widgets\ActiveForm */

$this->registerCss(
    <<<CSS
    .gallery-block-present {   
        border: 2px solid #ddd;
        border-radius: 15px;
        padding: 10px;
        margin-top: 2em;
        margin-left: 5px;
    }

    .gallery-remove {
        position: relative;
        top: 92%;
        left: 87%;
        font-size: 1.6em;
        border-radius: 5px;
    }
CSS

)
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'dynamicform_wrapper']]); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->field($model, 'project_id')->dropDownList(
            \app\modules\project\models\ProjectDropdowns::housingEstates(),
            ['prompt' => 'Выберите...']
        ) ?>

        <?= $form->translate($model, '@app/modules/buildingProgress/views/building-progress/_form-translation') ?>

        <div class="row">
            <div class="col-md-12">
                <?= Html::label('Срок сдачи') ?>
            </div>
        </div>

        <div class="row">
            <?= Html::activeHiddenInput($model, 'service_date', ['id' => 'service_date']); ?>

            <div class="col-md-2">
                <?php $quarterValue = $model->getServiceDate() === null ? null : $model->getServiceDate()['quarter'] ?>
                <?= Html::label('Квартал', null, ['style' => 'display: block']) ?>
                <?= Html::dropDownList('service_quarter', $quarterValue, $model::QUARTERS, ['prompt' => 'Выберите...'])?>
            </div>

            <div class="col-md-2">
                <?php $yearValue = $model->getServiceDate() === null ? null : $model->getServiceDate()['year'] ?>
                <?= Html::label('Год', null, ['style' => 'display: block']) ?>
                <?= Html::dropDownList('service_year', $yearValue,  $model::getServiceDateYears(), ['prompt' => 'Выберите...'])?>
            </div>
        </div>

        <div id="panels" style="margin-top: 20px">

            <?php if (count($model->getGalleryDates()) > 0): ?>
                <?php $nextKey = count($model->getGalleryDates()); ?>
            <?php else: ?>
                <?php $nextKey = 0; ?>
            <?php endif ?>

            <?php foreach($model->getGalleryDates() as $key => $date): ?>

                <div class="gallery-block row gallery-block-present">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">

                                <?= $form->field($model, "galleryDate[$key]")->datePicker([
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'startView'=> 'year',
                                        'minViewMode'=> 'months',
                                        'format' => 'yyyy-mm',
                                    ]
                                ]) ?>

                            </div>
                        </div>

                        <?php $this->beginContent('@app/views/layouts/admin/_panel.php', ['title' => 'Галерея']) ?>

                        <?= $form->field($model, "galleryImages[$key][]", ['template' => '{input}'])->imageUpload(['options' => [
                            'accept' => 'image/*',
                            'multiple' => true,
                        ],
                            'pluginOptions' => [
                                'showRemove' => false,
                                'showUpload' => false,
                                'initialPreviewAsData' => true,
                                'overwriteInitial' => false,
                                'browseOnZoneClick' => true,
                                'initialPreview' => array_values($model->getGallery($model->id, $date)),
                                'initialPreviewConfig' => array_map(function ($v) {
                                    return ['key' => $v];
                                }, array_keys($model->getGallery($model->id, $date))),
                                'maxFileSize' => 2800,
                                'deleteUrl' => \yii\helpers\Url::to([
                                    '/admin/buildingProgress/building-progress/delete-gallery-file',
                                    'modelName' => \app\modules\buildingProgress\models\BuildingProgressGallery::class
                                ]),
                            ],
                            'pluginEvents' => [
                                'filesorted' => new \yii\web\JsExpression('function(event, params){
                                                    $.post("' . \yii\helpers\Url::to(["/admin/catalog/catalog/sort-image",
                                        'postId' => $model->id,
                                        'modelName' => \app\modules\buildingProgress\models\BuildingProgressGallery::class
                                    ]) . '",{position: params});
                                            }'),
                            ],
                        ]) ?>

                        <?php $this->endContent() ?>

                    </div>

                    <div class="col-md-6">
                        <?= Html::button('Удалить галлерею', [
                            'class' => 'btn btn-danger delete-gallery-btn',
                            'data-gallery-id' => $model->id,
                            'data-gallery-date' => $date
                        ]) ?>
                    </div>

                </div>

            <?php endforeach; ?>

            <div class="alert alert-success alert-dismissable" style="margin-top: 20px">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i>Возможна загрузка лишь одной галереи за одну сессию. Максимальное количество изображений - 20</h4>
                <?= Yii::$app->session->getFlash('success') ?>
            </div>

            <div class="gallery-block row gallery-block-present">

                <div class="col-md-10">

                    <div class="row">
                        <div class="col-md-6">

                            <?= $form->field($model, "galleryDate[$nextKey]")->datePicker(['pluginOptions' => [
                                'autoclose' => true,
                                'startView'=> 'year',
                                'minViewMode'=> 'months',
                                'format' => 'yyyy-mm'
                            ]]) ?>

                        </div>
                    </div>

                    <?php $this->beginContent('@app/views/layouts/admin/_panel.php', ['title' => 'Галерея']) ?>

                    <?= $form->field($model, "galleryImages[$nextKey][]", ['template' => '{input}'])->imageUpload(['options' => [
                        'accept' => 'image/*',
                        'multiple' => true,
                    ],
                        'pluginOptions' => [
                            'showRemove' => false,
                            'showUpload' => false,
                            'initialPreviewAsData' => true,
                            'overwriteInitial' => false,
                            'browseOnZoneClick' => true,
                            'initialPreview' => [],
                            'initialPreviewConfig' => array_map(function ($v) {
                                return ['key' => $v];
                            }, []),
                            'maxFileSize' => 2800,
                            'maxFileCount' => 20,
                            'deleteUrl' => \yii\helpers\Url::to([
                                '/admin/buildingProgress/building-progress/delete-gallery-file',
                                'modelName' => \app\modules\buildingProgress\models\BuildingProgressGallery::class
                            ]),
                        ],
                        'pluginEvents' => [
                            'filesorted' => new \yii\web\JsExpression('function(event, params){
                                                    $.post("' . \yii\helpers\Url::to(["/admin/catalog/catalog/sort-image",
                                    'postId' => $model->id,
                                    'modelName' => \app\modules\buildingProgress\models\BuildingProgressGallery::class
                                ]) . '",{position: params});
                                            }'),
                        ],
                    ]) ?>

                    <?php $this->endContent() ?>

                </div>

                <div class="col-md-2">
                    <?= Html::button('×', ['class' => 'gallery-remove']) ?>
                </div>

            </div>

            <div class="gallery-block"></div>

        </div>

    </div>

</div>

<div class="form-group" style="margin-top: 2em">

    <div class="col-md-3">

        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>

    </div>

    <div class="col-md-3">

        <?= Html::button('Добавить галерею', [
                'id' => 'btn-add-gallery',
                'class' => 'btn btn-info',
                'style' => ['display' => 'none']
            ]
        ) ?>

    </div>

</div>

<?php ActiveForm::end(); ?>

<?php
$modelId = $model->id;

$js = <<<JS

$("body").on("click", "#btn-add-gallery", function () {
    let lastNum = $('.widget-panel').length;
   
    $.ajax({
        url: "add-file-input",
        data: {index: lastNum},
        success: function (data) {
            let galleryElemBlock = $(".gallery-block").last();
            galleryElemBlock.clone().appendTo('#panels');
            $(".gallery-block").last().html(data);
            $(".gallery-block").last().addClass('gallery-block-present');
            $(".gallery-block").last().addClass('row');
            $("#btn-add-gallery").css('display', 'none');
        },
        error: function (exception) {
            console.log('ERROR');
        }
    });
});

let serviceQuarter = '';
let serviceYear = '';

$('select[name="service_quarter"]').change(function () {
    serviceQuarter = $('select[name="service_quarter"] option:selected').text();
    console.log(serviceQuarter + ' ' + serviceYear);
    $('#service_date').val(serviceQuarter + ' ' + serviceYear);
});

$('select[name="service_year"]').change(function () {
    serviceYear = $('select[name="service_year"] option:selected').text();
    console.log(serviceQuarter + ' ' + serviceYear);
    $('#service_date').val(serviceQuarter + ' ' + serviceYear);
});

$('body').on('click', '.gallery-remove', function(e) {
    e.target.parentElement.parentElement.remove();
    $("#btn-add-gallery").css('display', 'block');
})

$('.delete-gallery-btn').click(function (e) {
        if (confirm('Вы уверен что хотите удалить галерею?')) {
        $.ajax({
            url: 'delete-gallery',
            type: "GET",
            data: {
                galleryId: e.target.dataset.galleryId,
                galleryDate: e.target.dataset.galleryDate
            },
            success: function () {
                e.target.parentNode.parentNode.remove();
            }
        });
    }
});

JS;

$this->registerJs($js);

?>
