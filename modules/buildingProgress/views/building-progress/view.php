<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\buildingProgress\models\BuildingProgress */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ход стротиельства', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'project_id',
                'value' => function($model) {
                    if ($model->project_id) {
                        return $model->project->name;
                    }
                }
            ],
            [
                'label' => 'Описание',
                'value' => function($model) {
                    return $model->getTranslation()::find()->where(['lang' => \Yii::$app->language])->one()->description;
                }
            ],
            'service_date',
            'position',
        ],
    ]) ?>
    </div>
</div>
<h2>Галлерея</h2>
<div class="row">
    <?php $galleryPaths = \app\modules\buildingProgress\models\BuildingProgressGallery::getImages($model->id); ?>
    <?php foreach($galleryPaths as $path): ?>
        <?= Html::img($path, []); ?>
    <?php endforeach; ?>
</div>