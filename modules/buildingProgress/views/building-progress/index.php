<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\buildingProgress\models\search\BuildingProgressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ход строительства';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'project_id',
            'value' => function($model) {
                if ($model->project_id) {
                    return $model->project->name;
                }
            }
        ],
        'service_date',
        [
            'class' => 'yii2tech\admin\grid\PositionColumn',
            'value' => 'position',
            'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        ],

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
