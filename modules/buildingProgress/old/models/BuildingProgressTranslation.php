<?php

namespace app\modules\buildingProgress\models;

use Yii;

/**
 * This is the model class for table "building_progress_translation".
 *
 * @property integer $id
 * @property integer $building_progress_id
 * @property string $description
 *
 * @property BuildingProgress $buildingProgress
 */
class BuildingProgressTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'building_progress_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['building_progress_id'], 'integer'],
            [['description', 'lang'], 'string'],
            [['building_progress_id'], 'exist', 'skipOnError' => true, 'targetClass' => BuildingProgress::class, 'targetAttribute' => ['building_progress_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Язык перевода',
            'building_progress_id' => 'Building Progress ID',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildingProgress()
    {
        return $this->hasOne(BuildingProgress::className(), ['id' => 'building_progress_id']);
    }

}
