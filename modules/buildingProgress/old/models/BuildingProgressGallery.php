<?php


namespace app\modules\buildingProgress\models;


use Imagine\Image\ManipulatorInterface;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;


class BuildingProgressGallery extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'building_progress_gallery';
    }

    public static function getImages($modelId, $date=null)
    {
        $gallery = static::find()
            ->where(['building_progress_id' => $modelId])
            ->orderBy(['id' => SORT_DESC])
        ;

        if ($date) {
            $gallery = $gallery->andWhere(['deadline_date' => $date]);
        }

        $gallery = $gallery->indexBy('id')
            ->select('path')
            ->orderBy('position')
            ->column();

        return array_map(function($v) use($modelId){
            //return static::getUrl($v);
            return static::get1175Image($v, $modelId);
        }, $gallery);
    }

    public static function bulkSave(int $modelId, array $data, string $deadlineDate)
    {
        $count = (int)static::find()
            ->where(['building_progress_id' => $modelId])
            ->count();

        $gallery = [];
        $path = \Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . static::tableName() . DIRECTORY_SEPARATOR . $modelId;

        FileHelper::createDirectory($path);

        foreach ($data as $index => $datum) {

            if (!$datum instanceof UploadedFile) {
                throw new \Exception('Gallery item must be instantiated of yii\web\UploadedFile');
            }

            $fileName = md5(microtime()) . '.' . $datum->getExtension();
            $isUpload = $datum->saveAs($path . DIRECTORY_SEPARATOR . $fileName);

            if (static::getThumbnailsConfig()) {
                foreach (static::getThumbnailsConfig() as $profile => $thumb) {
                    $thumbPath = $path . DIRECTORY_SEPARATOR . self::getThumbFileName($fileName, $profile);
                    self::generateImageThumb($thumb, $path . '/' . $fileName, $thumbPath);
                }
            }

            if ($isUpload) {
                $gallery[] = [
                    $modelId,
                    $fileName,
                    $index + $count,
                    $deadlineDate
                ];
            }
        }

        static::query()
            ->batchInsert(static::tableName(), [
                'building_progress_id',
                'path',
                'position',
                'deadline_date'
            ], $gallery)->execute();

        return true;
    }

    public static function getThumbnailsConfig(): array
    {
        return [
            '1175х650' => [
                'width' => 1175,
                'height' => 650,
                'quality' => 85,
                'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
            ],
            '770x550' => [
                'width' => 770,
                'height' => 550,
                'quality' => 85,
                'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND
            ],
        ];
    }

    public static function getThumbFileName($filename, $profile = 'thumb')
    {
        return $profile . '-' . $filename;
    }

    public static function generateImageThumb($config, $path, $thumbPath)
    {
        $width = ArrayHelper::getValue($config, 'width');
        $height = ArrayHelper::getValue($config, 'height');
        $quality = ArrayHelper::getValue($config, 'quality', 100);
        $mode = ArrayHelper::getValue($config, 'mode', ManipulatorInterface::THUMBNAIL_OUTBOUND);
        $bg_color = ArrayHelper::getValue($config, 'bg_color', 'FFF');

        if (!$width || !$height) {
            $image = Image::getImagine()->open($path);
            $ratio = $image->getSize()->getWidth() / $image->getSize()->getHeight();
            if ($width) {
                $height = ceil($width / $ratio);
            } else {
                $width = ceil($height * $ratio);
            }
        }

        // Fix error "PHP GD Allowed memory size exhausted".
        ini_set('memory_limit', '512M');
        Image::$thumbnailBackgroundColor = $bg_color;
        Image::thumbnail($path, $width, $height, $mode)->save($thumbPath, ['quality' => $quality]);
    }

    private static function getUrl($fileName)
    {
        return \Yii::getAlias('@web/media/' . static::tableName() . DIRECTORY_SEPARATOR . $fileName);
    }

    private static function get1175Image($fileName, $modelId)
    {
        $fileName = self::getThumbFileName($fileName, '1175х650');
        return \Yii::getAlias('@web/media/' . static::tableName() . DIRECTORY_SEPARATOR . $modelId . DIRECTORY_SEPARATOR . $fileName);
    }

    public static function query($sql = null)
    {
        $connection = \Yii::$app->db;
        return $connection->createCommand($sql);
    }

    public static function getDir()
    {
        return \Yii::getAlias('@webroot/media/' . static::tableName());
    }

    public static function getUniqueDates() : array
    {
        $fromDb = self::find()
            ->select('deadline_date')
            ->distinct()
            ->asArray()
            ->all();

        $galleryDates = array_map(function($item) {
            return $item['deadline_date'];
        }, $fromDb);

        // sort dates from past to future
        usort($galleryDates, function($date1, $date2) {
            $date1 = (new \DateTimeImmutable($date1))->getTimestamp();
            $date2 = (new \DateTimeImmutable($date2))->getTimestamp();

            if ($date1 === $date2) {
                return 0;
            }

            return ($date1 < $date2) ? -1 : 1;
        });

        return $galleryDates;
    }
}
