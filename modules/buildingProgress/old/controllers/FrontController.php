<?php


namespace app\modules\buildingProgress\controllers;


use app\components\front\Helper;
use app\modules\buildingProgress\models\BuildingProgress;
use app\modules\buildingProgress\models\BuildingProgressGallery;
use app\modules\project\models\ProjectStatus;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class FrontController extends Controller
{
    /**
     * HINT: $status and $statusMobile fields will be used in future
     * @return string
     */
    public function actionIndex()
    {
        $date = \Yii::$app->request->get('date');
        $status = \Yii::$app->request->get('type');
        $statusMobile = \Yii::$app->request->get('status-mobile');
        if ($date || $status || $statusMobile) {
            $status = \Yii::$app->request->get('type') ?? \Yii::$app->request->get('status') ??
                \Yii::$app->request->get('type') ?? \Yii::$app->request->get('status-mobile') ;
            $date = \Yii::$app->request->get('date') == '' ? null : \Yii::$app->request->get('date')  ;

            $buildingProgressList = BuildingProgress::getByStatusAndDeadline((int) $status, $date);

            return $this->render('index', [
                'building_progress_list' => $buildingProgressList,
                'months' => Helper::formatDates(BuildingProgressGallery::getUniqueDates(), \Yii::$app->language),
                'statuses' => ArrayHelper::map(ProjectStatus::find()->all(), 'id', 'status'),
                'gallery_date' => $date
            ]);
        }

        $buildingProgressList = BuildingProgress::find()->joinWith('project')->all();

        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => 'Ход строительства'
            ]
        ]);

        return $this->render('index', [
            'building_progress_list' => $buildingProgressList,
            'months' => Helper::formatDates(BuildingProgressGallery::getUniqueDates(), \Yii::$app->language),
            'statuses' => ArrayHelper::map(ProjectStatus::find()->all(), 'id', 'status'),
            'gallery_date' => null
        ]);
    }
}