<?php

namespace app\modules\buildingProgress\controllers;

use app\components\front\Helper;
use app\modules\buildingProgress\models\BuildingProgress;
use app\modules\buildingProgress\models\BuildingProgressGallery;
use app\modules\buildingProgress\models\BuildingProgressTranslation;
use Bridge\Core\Controllers\BaseAdminController;
use Bridge\Core\Widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;


/**
 * BuildingProgressController implements the CRUD actions for [[app\modules\buildingProgress\models\BuildingProgress]] model.
 * @see app\modules\buildingProgress\models\BuildingProgress
 */
class BuildingProgressController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\buildingProgress\models\BuildingProgress';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\buildingProgress\models\search\BuildingProgressSearch';

    /**
     * @inheritdoc
     */
    public function actions() : array
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\buildingProgress\models\BuildingProgress',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'actions' => [
                        'index',
                        'view',
                        'update',
                        'delete',
                        'create',
                        'toggle',
                        'position',
                        'delete-gallery-file',
                        'add-datepicker',
                        'add-file-input',
                        'get-gallery-dates',
                        'delete-gallery'
                    ],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
                [
                    'actions' => ['get-building-progress'],
                    'allow' => true,
                ],
            ],
        ];
        return $behaviors;
    }

    /**
     * @param string $modelName
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionDeleteGalleryFile(string $modelName)
    {
        $id = \Yii::$app->request->post('key');
        $model = urldecode($modelName)::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        $path = $model::getDir() . DIRECTORY_SEPARATOR . $model->building_progress_id . DIRECTORY_SEPARATOR . $model->path;

        if($model->delete() && unlink($path)) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @param $serviceDate
     * @param $lang
     * @return array|\yii\web\Response
     * @throws Exception
     */
    public function actionGetBuildingProgress($id, $serviceDate, $lang)
    {
        if (\Yii::$app->request->isAjax) {

            $lang = $lang === 'ru' ? 'ru-RU' : 'kk-KZ';

            $model = BuildingProgress::findOne($id);
            $modelTranslation = BuildingProgressTranslation::find()
                ->where(['building_progress_id' => $id])
                ->andWhere(['lang' => $lang])
                ->one();

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (!$model) {
                throw new Exception('Model not found');
            }

            return [
                'description' => $modelTranslation->description,
                'serviceDate' => $model->service_date,
                'gallery' => $model->getGallery($serviceDate)
            ];
        }

        return $this->redirect('/');
    }

    /**
     * @deprecated
     * @param $index
     * @return string
     */
    public function actionAddFileInput($index)
    {
        $form = new ActiveForm();
        $model = new BuildingProgress();

        return $this->renderAjax('_fileinput', [
            'form' => $form,
            'model' => $model,
            'index' => $index
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetGalleryDates($id)
    {
        $gallery = BuildingProgressGallery::find()
            ->where(['building_progress_id' => $id])
            ->andWhere(['not', ['deadline_date' => null]])
            ->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return array_values(array_unique(array_map(function ($e) {
            return \Yii::$app->formatter->asDate($e->deadline_date, 'yyyy-MM');
        }, $gallery)));
    }

    /**
     * @param $galleryId
     * @param $galleryDate
     * @return bool
     */
    public function actionDeleteGallery($galleryId, $galleryDate)
    {
        if (\Yii::$app->request->isAjax) {
            try {
                \Yii::$app->db->createCommand()->delete(BuildingProgressGallery::tableName(), [
                'building_progress_id' => $galleryId,
                'deadline_date' => $galleryDate
            ])->execute();
            } catch (\Exception $e) {
                \Yii::error('Could not remove gallery');
            }

            return true;
        }

        return false;
    }
}
