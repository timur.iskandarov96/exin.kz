<?php

use yii\helpers\Url;
use yii\helpers\Html;

\app\assets\AdvanceAsset::register($this);

/* @var $months array */
/* @var $building_progress_list \app\modules\buildingProgress\models\BuildingProgress[] */
/* @var $statuses array */
/* @var $gallery_date */

?>

<main class="js-header-size">
    <div class="advance page">
        <?= \app\widgets\Breadcrumbs::widget(['elements' => [
            ['title' => \Yii::t('front', 'Главная'), 'url' => Url::home()],
            ['title' => \Yii::t('front', 'Ход строительства'), 'url' => false]
        ]]) ?>
        <div class="container-main">
            <h1><?= \Yii::t('front', 'Ход строительства') ?></h1>
            <?= Html::beginForm(['/building-progress'], 'get', ['enctype' => 'multipart/form-data', 'class' => 'advance__filter']) ?>
                <div class="advance__select-block">
                    <div class="select-form advance__select" id="select-dates">
                        <div class="select-form__head">
                            <span class="select-form__title"><?= \Yii::t('front', 'Месяц') ?></span>
                        </div>
                        <?php $hiddenDate = $gallery_date === null ? '0' : $gallery_date; ?>
                        <?= Html::hiddenInput('date', $hiddenDate, ['data-select-input' => '']) ?>
                        <ul class="select-form__options">
                            <?php foreach ($months as $key => $month): ?>
                                <?php if ($gallery_date === $key): ?>
                                    <li class="is-active" data-select-value="<?= $key ?>"><?= $month ?></li>
                                <?php else: ?>
                                    <li data-select-value="<?= $key ?>"><?= $month ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="advance__controls">
                    <button class="btn"><?= \Yii::t('front', 'Применить') ?></button>
                    <a href="<?= Url::to('/building-progress') ?>" class="btn discharge"><?= \Yii::t('front', 'Сбросить') ?></a>
                </div>
            <?= Html::endForm() ?>
        </div>
        <div class="container-main">
            <div class="advance__results-block">
                <div class="swiper-container advance__results-list">
                    <div class="swiper-wrapper">
                        <?php foreach ($building_progress_list as $key => $elem): ?>
                            <?php $active = $key === 0 ? ' is-active' : '' ?>
                            <div class="<?= 'swiper-slide advance__result'.$active ?>" data-id="<?= $elem->id ?>">
                                <div class="advance__result-img">
                                    <img src="<?= $elem->project->getThumbUploadUrl('image', 'thumb') ?>"
                                         alt="<?= $elem->project->name ?>"
                                         width="770"
                                         height="550"
                                    >
                                </div>
                                <div class="advance__result-info">
                                    <p class="advance__result-title"><?= $elem->project->name ?></p>
                                    <p class="apart__place"><?= $elem->project->translation->address ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="advance__info">
            <?php if (count($building_progress_list) > 0): ?>
                <div class="container-main">
                    <div class="advance__desc">
                        <div class="advance__desc-txt">
                            <?= current($building_progress_list)->translation->description ?>
                        </div>
                        <div class="advance__marks">
                            <span class="advance__mark advance__mark--service">
                                <?= \Yii::t('front', 'Срок сдачи').': '.
                                current($building_progress_list)->service_date ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="advance__gallery" data-gallery="1">
                    <?php $gallery = array_values(current($building_progress_list)->getGallery($gallery_date)); ?>
                    <div class="container-main advance__gallery-container">
                        <div class="advance__gallery-img">
                            <img src="<?= current($gallery) ?>" alt="Главное изображение галлереи">
                        </div>
                    </div>
                    <div class="container-main  advance__slider-container">
                        <div class="swiper-container advance__preview-list">
                            <div class="swiper-wrapper">
                                <?php foreach ($gallery as $key => $item): ?>
                                    <?php $hidden = $key > 4 ? ' hidden' : ''; ?>
                                    <?php $active = $key === 0 ? ' active' : ''; ?>
                                    <div class="<?= 'swiper-slide advance__preview-item'.$active.$hidden ?>" data-gallery="1">
                                        <img src="<?= $item ?>" alt="">
                                    </div>
                                <?php endforeach; ?>
                                <?php if (count($gallery) > 5): ?>
                                    <div class="swiper-slide advance__preview-item advance__preview-more" data-gallery="1">
                                        <p><?= \Yii::t('front', 'еще') ?> <?= count($gallery) - 5 ?></p>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="advance__preview-controls">
                            <div class="partners__btn partners__btn--prev"></div>
                            <div class="advance__pagination"></div>
                            <div class="partners__btn partners__btn--next"></div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
</main>
