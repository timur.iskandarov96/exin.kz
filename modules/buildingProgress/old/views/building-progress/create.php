<?php

/* @var $this yii\web\View */
/* @var $model app\modules\buildingProgress\models\BuildingProgress */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Building Progresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

