<?php

/* @var $this yii\web\View */
/* @var $model app\modules\buildingProgress\models\BuildingProgress */

$this->title = 'Редактировать запись: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ход строительства', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

<?php
$modelId = $model->id;

$js = <<<JS
$(document).ready(function () {
    
    console.log('UPDATE');
    $.ajax({
        url: "get-gallery-dates",
        data: {id: $modelId},
        success: function (data) {
            console.log(data);
            for (let i = 0; i < data.length; i++) {
                console.log(data[i]);
                $("#buildingprogress-gallerydate-"+i).val(data[i]);
            }
        },
        error: function (exception) {
            console.log('ERROR');
        }
    });
});
JS;


$this->registerJs($js);

?>
