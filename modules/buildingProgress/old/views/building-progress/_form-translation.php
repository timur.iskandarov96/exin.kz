<?php
/**
 * @var $languageCode string
 * @var $model \app\modules\buildingProgress\models\BuildingProgress
 * @var $form \naffiq\bridge\widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '[' . $languageCode . ']building_progress_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']lang')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '[' . $languageCode . ']description')->richTextArea(['options' => ['rows' => 6]]) ?>
