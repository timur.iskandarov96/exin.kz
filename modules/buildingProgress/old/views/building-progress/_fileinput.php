<?php

/* @deprecated */

use kartik\widgets\FileInput;
use yii\helpers\Html;

/* @var $model \app\modules\buildingProgress\models\BuildingProgress */
/* @var $i index */

?>

    <div class="col-md-10">

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, "galleryDate[$index]")->datePicker(['pluginOptions' => [
                    'autoclose' => true,
                    'startView'=> 'year',
                    'minViewMode'=> 'months',
                    'format' => 'yyyy-mm'
                ]]) ?>
            </div>
        </div>

        <?php $this->beginContent('@app/views/layouts/admin/_panel.php', ['title' => 'Галерея']) ?>

        <?= $form->field($model, "galleryImages[$index][]", ['template' => '{input}'])->imageUpload(['options' => [
            'accept' => 'image/*',
            'multiple' => true,
        ],
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'initialPreviewAsData' => true,
                'overwriteInitial' => false,
                'browseOnZoneClick' => true,
                'initialPreview' => array_values($model->getGallery()),
                'initialPreviewConfig' => array_map(function ($v) {
                    return ['key' => $v];
                }, array_keys($model->getGallery())),
                'maxFileSize' => 2800,
                'deleteUrl' => \yii\helpers\Url::to([
                    '/admin/buildingProgress/building-progress/delete-gallery-file',
                    'modelName' => \app\modules\buildingProgress\models\BuildingProgressGallery::class
                ]),
            ],
            'pluginEvents' => [
                'filesorted' => new \yii\web\JsExpression('function(event, params){
                                                    $.post("' . \yii\helpers\Url::to(["/admin/catalog/catalog/sort-image",
                        'postId' => $model->id,
                        'modelName' => \app\modules\buildingProgress\models\BuildingProgressGallery::class
                    ]) . '",{position: params});
                                            }'),
            ],
        ]) ?>

        <?php $this->endContent() ?>

    </div>

    <div class="col-md-2">
        <?= Html::button('X', ['class' => 'gallery-remove']) ?>
    </div>

