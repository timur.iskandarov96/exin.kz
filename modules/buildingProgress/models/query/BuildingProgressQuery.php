<?php

namespace app\modules\buildingProgress\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\buildingProgress\models\BuildingProgress]].
 *
 * @see \app\modules\buildingProgress\models\BuildingProgress
 */
class BuildingProgressQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\buildingProgress\models\BuildingProgress[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\buildingProgress\models\BuildingProgress|array|null
     */
    public function one($db = null)
    {
    return parent::one($db);
    }
}
