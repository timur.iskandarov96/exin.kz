<?php

namespace app\modules\buildingProgress\models;

use app\modules\aboutCompany\models\AboutCompanyTranslation;
use app\modules\gallery\models\RequestsAfterGallery;
use Yii;
use app\modules\project\models\Project;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "building_progress".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $deadline_date
 * @property string $service_date
 * @property integer $position
 *
 * @property Project $project
 * @property BuildingProgressTranslation[] $buildingProgressTranslations
 *
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition($position) Moves owner record to the specific position.
 * @method bool getIsFirst() Checks whether this record is the first in the list.
 * @method bool getIsLast() Checks whether this record is the the last in the list.
 * @method \BaseActiveRecord|static|null findPrev() Finds record previous to this one.
 * @method \BaseActiveRecord|static|null findNext() Finds record next to this one.
 * @method \BaseActiveRecord|static|null findFirst() Finds the first record in the list.
 * @method \BaseActiveRecord|static|null findLast() Finds the last record in the list.
 * @method mixed beforeInsert($event) Handles owner 'beforeInsert' owner event, preparing its positioning.
 * @method mixed beforeUpdate($event) Handles owner 'beforeInsert' owner event, preparing its possible re-positioning.
 */
class BuildingProgress extends \yii\db\ActiveRecord
{
    const QUARTERS = ['I', 'II', 'III', 'IV'];

    /**
     * @var array
     */
    public $galleryImages;

    /**
     * @var array
     */
    public $galleryDate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'building_progress';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'galleryDate'], 'required'],
            [['project_id', 'position'], 'integer'],
            [['service_date', 'galleryDate', 'galleryImages'], 'safe'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'ЖК',
            'service_date' => 'Срок эксплуатации',
            'position' => 'Позиция',
            'galleryDate' => 'Дата загрузки галереи'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildingProgressTranslations()
    {
        return $this->hasMany(BuildingProgressTranslation::class, ['building_progress_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\modules\buildingProgress\models\query\BuildingProgressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\buildingProgress\models\query\BuildingProgressQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'translation' => [
                'class' => '\Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' =>BuildingProgressTranslation::class,
                'translationModelRelationColumn' => 'building_progress_id'
            ]
        ];
    }

    /**
     * @return array|bool[]|string[]
     */
    public function getGallery($project=null, $date=null)
    {
        if (isset($project) && !empty($project)) {
            return BuildingProgressGallery::getImages($project, $date);
        } else {
            return BuildingProgressGallery::getImages($this->id, $date);
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if (count($this->galleryImages) > 0) {
            if (count($this->galleryImages) !== count($this->galleryDate)) {
                throw new Exception('Number of elements in gallery MUST match number of dates');
            }

            for ($i = 0; $i < count($this->galleryImages); $i++) {
                $fileData = UploadedFile::getInstances($this, "galleryImages[$i]");
                $galleryDate = $this->galleryDate[$i].'-01';

                BuildingProgressGallery::bulkSave(
                    $this->id,
                    $fileData,
                    $galleryDate
                );
            }
        }

        $deadlineDatesInDb = (new \yii\db\Query())
            ->select(['deadline_date'])
            ->from(BuildingProgressGallery::tableName())
            ->where(['building_progress_id' => $this->id])
            ->all();

        if (count($deadlineDatesInDb) > 0) {
            $deadlineDatesInDb = array_values(array_unique(array_map(function($e) {
                return $e['deadline_date'];
            }, $deadlineDatesInDb)));

            $formattedRequestDates = array_map(function($e) {
                return $e.'-01';
            }, $this->galleryDate);

            $changedDates = array_diff_assoc($formattedRequestDates, $deadlineDatesInDb);

            try {
                foreach ($changedDates as $key => $date) {
                    $needToBeUpdated = BuildingProgressGallery::find()
                        ->where(['building_progress_id' => $this->id])
                        ->andWhere(['deadline_date' => $deadlineDatesInDb[$key]])
                        ->all();
                    foreach ($needToBeUpdated as $item) {
                        $item->deadline_date = $date;
                        $item->update(false);
                    }
                }
            } catch (\yii\db\Exception $e) {
                Yii::error('AFTER_SAVE BuildingProgressGallery error ' . $e->getMessage());
            }
        }
    }

    /**
     * @param int|null $status
     * @param string|null $date
     * @return array
     */
    public static function getByStatusAndDeadline(int $status = null, string $date = null) : array
    {
        $all = self::find()->all();

        return array_filter($all, function ($item) use($status, $date) {
            if ($status && $date) {
                return ($item->project->status->id === $status && in_array($date,$item->getGalleryDates()));
            } elseif ($status) {
                return $item->project->status->id === $status;
            } elseif($date) {
                return in_array($date, $item->getGalleryDates());
            }

            return true;
        });
    }

    /**
     * @return array
     */
    public function getGalleryDates() : array
    {
        $gallery = BuildingProgressGallery::find()
            ->where(['building_progress_id' => $this->id])
            ->andWhere(['not', ['deadline_date' => null]])
            ->all();

        return array_values(array_unique(ArrayHelper::map($gallery, 'id', 'deadline_date')));
    }

    /**
     * @return array
     */
    public function getServiceDate() : ?array
    {
        if (!$this->service_date) {
            return null;
        }

        $serviceDateAsArr = explode(' ', $this->service_date);

        return [
            'quarter' => array_search($serviceDateAsArr[0], self::QUARTERS),
            'year' => array_search($serviceDateAsArr[1], self::getServiceDateYears())
        ];
    }

    /**
     * @return array
     */
    public static function getServiceDateYears() : array
    {
        $currentYear = (int) (new \DateTimeImmutable())->format('Y');

        return range($currentYear, $currentYear + 4);
    }
}
