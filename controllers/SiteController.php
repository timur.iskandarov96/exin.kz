<?php

namespace app\controllers;

use app\components\amocrm\AmoCrmService;
use app\components\sendpulse\EmailSender;
use app\modules\collection\models\query\DistrictQuery;
use app\modules\promoBanner\models\PromoBanner;
use app\modules\news\models\News;
use app\modules\request\models\HousingEstate;
use app\modules\request\models\Request;
use app\modules\slider\models\Slider;
use app\modules\stat\models\StatisticsMain;
use Da\User\Validator\AjaxRequestModelValidator;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        $formTokenName = \Yii::$app->params['form_token_param'] ?? '';

        if ($formTokenValue = \Yii::$app->request->post($formTokenName)) {
            $sessionTokenValue = \Yii::$app->session->get($formTokenName);

            if ($formTokenValue != $sessionTokenValue ) {
                throw new \yii\web\HttpException(400, 'The form token could not be verified.');
            }

            \Yii::$app->session->remove($formTokenName);
        }

        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        \Yii::$app->metaTags->registerAction([
            'ru-RU' => [
                'lang' => 'ru-RU',
                'title' => 'Exclusive Qurylys',
                'description' => 'Квартиры в ЖК Алматы. Цены на квартиры в лучших новостройках города. Чтобы купить квартиру в жилом комплексе мечты — оставьте заявку на сайте.',
                'keywords' => 'Exclusive Qurylys, квартиры, ЖК, Алматы',
            ],
        ]);

        return $this->render('index', [
            'contactForm' => new ContactForm(),
            'housingEstates' => HousingEstate::getAll(),
            'banner' => PromoBanner::find()->active()->one(),
            'news' => News::find()->active()->limit(5)->offset(0)->orderBy(['id' => SORT_DESC])->all(),
            'stats' => StatisticsMain::find()->active()->all(),
            'districtsCollection' => DistrictQuery::getCollection(),
            'slides' => Slider::find()->active()->orderBy(['position' => SORT_ASC])->all()
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSaveRequest()
    {
        $model = new ContactForm();
        $req = new Request();

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            try {
                $model->save($req);
                return true;
            } catch (\Exception $e) {
                \Yii::error($e->getMessage());
                throw new BadRequestHttpException();
            }
        }
    }

    public function actionFavorites()
    {
        return $this->render('favorites');
    }

    public function actionPolicy()
    {
        $this->layout = false;
        return $this->render('politics');
    }
}
