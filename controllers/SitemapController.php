<?php


namespace app\controllers;


use app\modules\crew\models\Crew;
use app\modules\exclusiveSocial\models\ExclusiveSocial;
use app\modules\news\models\News;
use app\modules\vacancy\models\Vacancy;
use yii\web\Controller;
use yii\web\Response;
use Yii;


class SitemapController extends Controller
{
    public function actionIndex()
    {
        Yii::$app->language = 'ru-RU';
        $items = [];

        $items = array_merge($items, $this->news());
        $items = array_merge($items, $this->exclusiveSocial());
        $items = array_merge($items, $this->vacancies());

        $xmlSitemap = $this->renderPartial('index', [ // записываем view на переменную для последующего кэширования
            'host' => Yii::$app->request->hostInfo,         // текущий домен сайта
            'urls' => $items,                               // сгенерированные ссылки для sitemap
        ]);

        Yii::$app->cache->set('sitemap', $xmlSitemap, 60 * 60 * 12); // кэшируем результат на 12 ч

        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        return $xmlSitemap;
    }

    private function news() : array
    {
        $news = News::find()->active()->all();

        $items = [];
        foreach ($news as $post) {
            $timeStamp = $post->updated_at ?? $post->created_at;
            $items[] = [
                'loc' => $post->url,
                'lastmod' => date(DATE_W3C, strtotime($timeStamp)),
                'changefreq' => 'daily',
                'priority' => 1.0,
            ];
        }

        return $items;
    }

    private function exclusiveSocial() : array
    {
        $articles = ExclusiveSocial::find()->active()->all();

        $items = [];
        foreach ($articles as $post) {
            $timeStamp = $post->updated_at ?? $post->created_at;
            $items[] = [
                'loc' => $post->url,
                'lastmod' => date(DATE_W3C, strtotime($timeStamp)),
                'changefreq' => 'daily',
                'priority' => 1.0,
            ];
        }

        return $items;
    }

    private function vacancies()
    {
        $vacancies = Vacancy::find()->active()->all();

        $items = [];
        foreach ($vacancies as $vacancy) {
            $timeStamp = $vacancy->updated_at ?? $vacancy->created_at;
            $items[] = [
                'loc' => $vacancy->url,
                'lastmod' => date(DATE_W3C, strtotime($timeStamp)),
                'changefreq' => 'daily',
                'priority' => 1.0,
            ];
        }

        return $items;
    }
}