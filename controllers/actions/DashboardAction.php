<?php

namespace app\controllers\actions;

use yii\base\Action;

class DashboardAction extends Action
{
    public function runWithParams($params)
    {
        return $this->controller->redirect('/admin/request/request/index');
    }
}