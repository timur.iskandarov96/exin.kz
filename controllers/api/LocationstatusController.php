<?php


namespace app\controllers\api;


use app\modules\collection\models\BatchOperator;
use app\modules\collection\models\BatchOperatorValidation;
use app\modules\collection\models\LocationStatus;
use yii\db\Exception;
use yii\web\ServerErrorHttpException;


/**
 * Class LocationstatusController
 * @package app\controllers\api
 */
class LocationstatusController extends BaseRestController
{
    /**
     * @var string
     */
    public $modelClass = LocationStatus::class;

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchInsert()
    {
        $locationStatuses = \Yii::$app->request->post('locationstatuses');
        $attributes = ['uuid', 'name', 'blocked'];
        $validation = BatchOperatorValidation::validateInsert($locationStatuses, $attributes);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchInsert($locationStatuses, LocationStatus::tableName(), $attributes);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Locationstatus batch insert');
        }

        return true;
    }

    /**
     * @return bool|mixed
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchUpdate()
    {
        $locationStatuses = \Yii::$app->request->post('locationstatuses');
        $validation = BatchOperatorValidation::validateUpdate($locationStatuses);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchUpdate($locationStatuses, LocationStatus::tableName());
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Locationstatus batch update');
        }

        return true;
    }

    /**
     * @param string $id
     * @return LocationStatus|array|null
     */
    public function actionView(string $id)
    {
        return LocationStatus::find()->where(['uuid' => $id])->one();
    }
}
