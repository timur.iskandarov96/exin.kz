<?php


namespace app\controllers\api;


use app\modules\collection\models\BatchOperator;
use app\modules\collection\models\BatchOperatorValidation;
use app\modules\collection\models\LocationType;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;


/**
 * Class LocationtypeController
 * @package app\controllers\api
 */
class LocationtypeController extends BaseRestController
{
    /**
     * @var string
     */
    public $modelClass = LocationType::class;

    /**
     * @return array|void
     */
    public function actions() : ?array
    {
        return array_merge(parent::actions(), [
            'get-translation'
        ]);
    }

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchInsert()
    {
        $locationTypes = \Yii::$app->request->post('locationtypes');
        $attributes = ['uuid', 'name', 'blocked'];
        $validation = BatchOperatorValidation::validateInsert($locationTypes, $attributes);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchInsert($locationTypes, LocationType::tableName(), $attributes);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Locationtype batch insert');
        }

        return true;
    }

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchUpdate()
    {
        $locationTypes = \Yii::$app->request->post('locationtypes');
        $validation = BatchOperatorValidation::validateUpdate($locationTypes);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchUpdate($locationTypes, LocationType::tableName());
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Locationtype batch update');
        }

        return true;
    }

    /**
     * @param string $id
     * @return LocationType|array|null
     */
    public function actionView(string $id)
    {
        return LocationType::find()->where(['uuid' => $id])->one();
    }

    /**
     * @param string $lang
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetTranslation(string $lang) : array
    {
        if (!in_array($lang, ['ru-RU', 'kk-KZ'])) {
            throw new BadRequestHttpException();
        }

        $locationTypes = LocationType::find()->where(['blocked' => false])->all();

        $result = [];
        foreach ($locationTypes as $type) {
            $result[] = [
                'uuid' => $type->uuid,
                'name' => \Yii::t('front', $type->name, [], $lang)
            ];
        }

        return $result;
    }
}
