<?php


namespace app\controllers\api;


use app\modules\collection\models\BatchOperator;
use app\modules\collection\models\BatchOperatorValidation;
use app\modules\collection\models\Project;
use yii\db\Exception;
use yii\web\ServerErrorHttpException;


/**
 * Class ProjectController
 * @package app\controllers\api
 */
class ProjectController extends BaseRestController
{
    /**
     * @var string
     */
    public $modelClass = Project::class;

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchInsert()
    {
        $projects = \Yii::$app->request->post('projects');
        $attributes = ['uuid', 'name', 'city', 'district', 'blocked'];
        $validation = BatchOperatorValidation::validateInsert($projects, $attributes);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchInsert($projects, Project::tableName(), $attributes);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Project batch insert');
        }

        return true;
    }

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchUpdate()
    {
        $projects = \Yii::$app->request->post('projects');
        $validation = BatchOperatorValidation::validateUpdate($projects);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchUpdate($projects, Project::tableName());
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Project batch update');
        }

        return true;
    }

    /**
     * @param string $id
     * @return Project|array|null
     */
    public function actionView(string $id)
    {
        return Project::find()->where(['uuid' => $id])->one();
    }
}