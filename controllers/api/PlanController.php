<?php


namespace app\controllers\api;


use app\modules\collection\models\BatchOperator;
use app\modules\collection\models\BatchOperatorValidation;
use app\modules\collection\models\Plan;
use yii\db\Exception;
use yii\web\ServerErrorHttpException;


/**
 * Class PlanController
 * @package app\controllers\api
 */
class PlanController extends BaseRestController
{
    /**
     * @var string
     */
    public $modelClass = Plan::class;

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchInsert()
    {
        $plans = \Yii::$app->request->post('plans');
        $attributes = ['uuid', 'text', 'ru_image', 'kk_image', 'blocked'];
        $validation = BatchOperatorValidation::validateInsert($plans, $attributes);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchInsert($plans, Plan::tableName(), $attributes);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Plan batch insert');
        }

        return true;
    }

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchUpdate()
    {
        $plans = \Yii::$app->request->post('plans');
        $validation = BatchOperatorValidation::validateUpdate($plans);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchUpdate($plans, Plan::tableName());
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Plan batch update');
        }

        return true;
    }

    /**
     * @param string $id
     * @return Plan|array|null
     */
    public function actionView(string $id)
    {
        return Plan::find()->where(['uuid' => $id])->one();
    }
}