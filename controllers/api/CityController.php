<?php


namespace app\controllers\api;

use app\modules\collection\models\BatchOperator;
use app\modules\collection\models\BatchOperatorValidation;
use app\modules\collection\models\City;
use yii\db\Exception;
use yii\web\ServerErrorHttpException;


/**
 * Class CityController
 * @package app\controllers\api
 */
class CityController extends BaseRestController
{
    /**
     * @var string
     */
    public $modelClass = City::class;

    /**
     * @return bool|mixed
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchInsert()
    {
        $cities = \Yii::$app->request->post('cities');
        $attributes = ['uuid', 'name', 'blocked'];
        $validation = BatchOperatorValidation::validateInsert($cities, $attributes);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchInsert($cities, City::tableName(), $attributes);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process City batch insert');
        }

        return true;
    }

    /**
     * @return bool|mixed
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchUpdate()
    {
        $cities = \Yii::$app->request->post('cities');
        $validation = BatchOperatorValidation::validateUpdate($cities);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchUpdate($cities, City::tableName());
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process City batch update');
        }

        return true;
    }

    /**
     * @param string $id
     * @return City|array|null
     */
    public function actionView(string $id)
    {
        return City::find()->where(['uuid' => $id])->one();
    }
}