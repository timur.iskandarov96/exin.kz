<?php


namespace app\controllers\api;


use app\modules\collection\models\BatchOperator;
use app\modules\collection\models\BatchOperatorValidation;
use app\modules\collection\models\Location;
use app\modules\collection\models\query\LocationQuery;
use yii\db\Exception;
use yii\web\ServerErrorHttpException;


/**
 * Class LocationController
 * @package app\controllers\api
 */
class LocationController extends BaseRestController
{
    /**
     * @var string
     */
    public $modelClass = Location::class;

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchInsert()
    {
        $locations = \Yii::$app->request->post('locations');
        $attributes = [
            'uuid',
            'project',
            'area',
            'arealife',
            'locationtype',
            'room_number',
            'locationstatus',
            'price',
            'priceformetre',
            'finish',
            'heightarea',
            'file',
            'blocked',
            'sold'
        ];

        $validation = BatchOperatorValidation::validateInsert($locations, $attributes);

        if (!$validation['result']) {

            date_default_timezone_set('Asia/Dacca');
            $mess = "Дата: ".date("d M Y H:i:s")."\n Сообщение: \n".$validation['message'];
            $token='575889929:AAHx0Um6lHbpu52dgnP0Mpwf-4qGnno_HKQ';//Наш token
            $query = [
                'chat_id' => 420047281,//id чата -248040821
                'parse_mode' => 'HTML',
                'text' => $mess
            ];


            file_get_contents(sprintf(
                'https://api.telegram.org/bot%s/sendMessage?%s',
                $token, http_build_query($query)
            ));

            return $validation['message'];

        }

        try {
            BatchOperator::batchInsert($locations, Location::tableName(), $attributes);
            BatchOperator::sendToCmsData($locations);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Location batch insert');
        }

        return true;
    }

    public function actionBatchInsertAndClear()
    {
        $locations = \Yii::$app->request->post('locations');
        $attributes = [
            'uuid',
            'project',
            'area',
            'arealife',
            'locationtype',
            'room_number',
            'locationstatus',
            'price',
            'priceformetre',
            'finish',
            'heightarea',
            'file',
            'blocked',
            'sold'
        ];

        $validation = BatchOperatorValidation::validateInsert($locations, $attributes);

        if (!$validation['result']) {

            return $validation['message'];

        }

        try {
            BatchOperator::clearTable(Location::tableName());
            BatchOperator::sendToCmsData($locations);
            BatchOperator::batchInsert($locations, Location::tableName(), $attributes);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Location batch insert');
        }

        return true;
    }

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchUpdate()
    {
        $locations = \Yii::$app->request->post('locations');
        $validation = BatchOperatorValidation::validateUpdate($locations);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchUpdate($locations, Location::tableName());
            BatchOperator::sendToCmsData($locations);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process Location batch update');
        }

        return true;
    }

    /**
     * @param string $id
     * @return Location|array|null
     */
    public function actionView(string $id) : ?Location
    {
        return Location::find()->where(['uuid' => $id])->one();
    }

    /**
     * @return array
     */
    public function actionFilterData() : array
    {
        return [
            'min_area' => floor(Location::find()->min('area')),
            'max_area' => round(Location::find()->max('area')),
            'min_price' => floor(Location::find()->min('price') / LocationQuery::MILLION),
            'max_price' => round(Location::find()->max('price') / LocationQuery::MILLION),
            'finish_types_ru' => Location::FINISH_TYPES_RU,
            'finish_types_kz' => Location::FINISH_TYPES_KZ
        ];
    }

    /**
     * @return array|void
     */
    public function actions() : ?array
    {
        return array_merge(parent::actions(), [
            'filter-data'
        ]);
    }
}
