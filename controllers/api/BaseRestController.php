<?php

namespace app\controllers\api;


use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;


/**
 * Class BaseRestController
 * @package app\controllers\api
 */
class BaseRestController extends ActiveController
{
    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        $behaviours = parent::behaviors();

        $behaviours['authenticator'] = [
            'class' => HttpBearerAuth::class,
        ];

        return $behaviours;
    }

    /**
     * @return array|\string[][]
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'batch-insert' => ['POST'],
            'batch-insert-and-clear' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    /**
     * @return array|void
     */
    public function actions()
    {
        $actions = array_merge(parent::actions(), [
            'batch-insert',
            'batch-insert-and-clear',
            'batch-update'
        ]);

        unset($actions['view']);

        return $actions;
    }
}
