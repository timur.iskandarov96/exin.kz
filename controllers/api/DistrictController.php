<?php


namespace app\controllers\api;


use app\modules\collection\models\BatchOperator;
use app\modules\collection\models\BatchOperatorValidation;
use app\modules\collection\models\District;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;


/**
 * Class DistrictController
 * @package app\controllers\api
 */
class DistrictController extends BaseRestController
{
    /**
     * @var string
     */
    public $modelClass = District::class;

    /**
     * @return array|void
     */
    public function actions() : ?array
    {
        return array_merge(parent::actions(), [
            'get-translation'
        ]);
    }

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchInsert()
    {
        $districts = \Yii::$app->request->post('districts');
        $attributes = ['uuid', 'name', 'city', 'blocked'];
        $validation = BatchOperatorValidation::validateInsert($districts, $attributes);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchInsert($districts, District::tableName(), $attributes);
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process District batch insert');
        }

        return true;
    }

    /**
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionBatchUpdate()
    {
        $districts = \Yii::$app->request->post('districts');
        $validation = BatchOperatorValidation::validateUpdate($districts);

        if (!$validation['result']) {
            return $validation['message'];
        }

        try {
            BatchOperator::batchUpdate($districts, District::tableName());
        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Could not process District batch update');
        }

        return true;
    }

    /**
     * @param string $id
     * @return District|array|null
     */
    public function actionView(string $id)
    {
        return District::find()->where(['uuid' => $id])->one();
    }

    /**
     * @param string $lang
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetTranslation(string $lang) : array
    {
        if (!in_array($lang, ['ru-RU', 'kk-KZ'])) {
            throw new BadRequestHttpException();
        }

        $districts =  District::find()->where(['blocked' => false])->all();

        $result = [];
        foreach ($districts as $district) {
            $result[] = [
                'uuid' => $district->uuid,
                'name' => \Yii::t('front', $district->name, [], $lang)
            ];
        }

        return $result;
    }
}
